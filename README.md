# Phiscal
The Business Information System

## Description
Repo defines a RESTful API to [Phiscal](https://www.phiscal.site), an online Business Information System, facilitating functionality of different induries, by means of sub-components, dubbed **"Modules"**.
Application is licensed under [the GNU Public License version 3](https://www.gnu.org/licenses/gpl-3.0.txt).

## Dependencies
* Apache 2
* Maria DB
* PHP 7.4
* Phalcon 4

## Installation
Setup database.schema

### GNU System
Execute shell script in dir **usr/sql/** .

```bash
bash usr/sql/db-gnu.sh
```

### Windows
Execute batch file  **usr/sql/db-win32.bat** .
