<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Model
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Model;

use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Di\Injectable;
/**
 * BaseModel class
 *
 * @category  PHP
 * @package   Nishe\Model
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @link      127.0.0.1
 */
class Base extends Injectable
{
    /**
     * @var PDOStatement
     */
    private $stmt;

    /**
     * @var array
     */
    private $index;

    /**
     * @var string
     */
    protected $source;

    /**
     * Constructor
     *
     * @param array $param = []	: Selected fields
     */
    public function __construct(array $param = [])
    {
        foreach ($param as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Returns table name mapped in the model
     *
     * @param string $source	: Database Table/View
     *
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * Set Model Database Table
     *
     * @param string $table
     * *
     * @return self
     */
    public function setSource(string $target): self
    {
        $this->source = $target;
        return $this;
    }

    /**
     * Returns Primary Key query or parameter
     *
     * @param bool $value	: Determines what is returned
     *
     * @return string|array
     */
    public function getId(bool $value = false)
    {
        if ($value) {
            return $this->index;
        }

        $query = '';
        $i = 0;
        foreach ($this->index as $key => $value) {
            $query = $i === 0 ? "$key = :$key" : "$query AND $key = :$key";
            $i++;
        }
        return $query;
    }

    /**
     * Assign Primary Key
     *
     * @param array $id
     *
     * @return self
     */
    public function setId(array $id): self
    {
        $this->index = $id;
        return $this;
    }

    /**
     * Generic Query
     *
     * @param string $sql   Query
     * @param array  $param aurguments
     *
     * @return bool
     */
    public function sql(string $sql, array $param = null): bool
    {
        try {
            $this->stmt = $this->db->prepare($sql);
            return $this->stmt->execute($param);
        } catch (\PDOException $e) {
            $aug = '';
            foreach ($param as $key => $value) {
                $aug .= "{$key}:{$value};";
            }
            $this->session->set('XET', (object) ['messages' => [$e->getMessage(), $sql, $aug], 'code' => intval($e->getCode())]);
            return false;
        }
    }

    /**
     * Generate Stored Function script
     *
     * @param string $name  Function
     * @param array  $param Arguments
     *
     * @return bool
     */
    public function sf(string $name, array $param = []): bool
    {
        $args = '';
        $i = 0;
        foreach ($param as $key => $value) {
            $args = $i === 0 ? ":$key" : "$args,:$key";
            $i++;
        }
        return $this->sql("SELECT $name($args)", $param);
    }

    /**
     * Generate Stored Procedure script
     *
     * @param string $name  Procedure
     * @param array  $param Arguments
     *
     * @return bool
     */
    public function sp(string $name, array $param = []): bool
    {
        $call = "call $name(";
        foreach ($param as $key => $value) {
            $call = "$call:$key, ";
        }
        return $this->sql("$call@code, @info)", $param);
    }

    /**
     * Select Single Record in table
     *
     * @param string	$fields	: Table Fields returned from query
     * @param string	$class	: Object Type returned from query
     *
     * @return object|bool
     */
    public function select(string $fields = '*', $class = self::class)
    {
        $query = "SELECT $fields FROM {$this->getSource()} WHERE {$this->getId()}";
        if (!$this->sql($query, $this->getId(true))) {
            return false;
        }
        $obj = $this->stmt->fetchObject($class);
        if (!$obj) {
            return false;
        }
        return $obj->setId($this->getId(true))->setSource($this->getSource());
    }

    /**
     * Insert new Record
     *
     * @param array $param	: Fields
     *
     * @return mixed
     */
    public function insert(array $param)
    {
        $fields = '';
        $values = '';
        $i = 0;
        foreach ($param as $key => $value) {
            switch ($i) {
                case 0:
                    $fields = $key;
                    $values = ":$key";
                    break;

                default:
                    $fields = "$fields, $key";
                    $values = "$values, :$key";
                    break;
            }
            $i++;
        }
        if (!$this->sql("INSERT INTO {$this->getSource()} ({$fields}) VALUES({$values})", $param)) {
            return false;
        }
        $this->stmt->closeCursor();
        return $this->getPdo();
    }

    /**
     * Generate Update script
     *
     * @param array  $param Arguments
     * @param string $override	: Target Table
     *
     * @return bool
     */
    public function update(array $param): bool
    {
        $args = '';
        $i = 0;
        foreach ($param as $key => $value) {
            $args = $i === 0 ? "$key = :$key" : "$args, $key = :$key";
            $i++;
        }
        $param += $this->getId(true);
        return $this->sql("UPDATE {$this->getSource()} SET $args WHERE {$this->getId()}", $param);
    }

    /**
     * Delete Record
     *
     * @return mixed
     */
    public function delete()
    {
        $result = $this->sql("DELETE FROM {$this->getSource()} WHERE {$this->getId()}", $this->getId(true));
        if ($result === false) {
            return false;
        }
        $this->stmt->closeCursor();
        return $this->getPdo();
    }

    //
    // ToolSet
    //

    /**
     * Query Results object
     *
     * @return PDOStatement
     */
    public function getStatement(): \PDOStatement
    {
        return $this->stmt;
    }

    /**
     * PDO Object
     *
     * @return PDO
     */
    public function getPdo(): Mysql
    {
        return $this->db;
    }

    /**
     * Filter out empty fields
     *
     * @return : array
     */
    public function filter(): array
    {
        return array_filter((array) $this);
    }

    //
    // Stored Routine Section
    //

    /**
     * Verify Stored Procedure Results
     *
     * @return object|bool
     */
    public function verifySP()
    {
        try {
            $this->stmt->closeCursor();
            $result = $this->db->query('SELECT @code code, @info content')->fetch(\PDO::FETCH_OBJ);

            if ($result->code != 0) {
                $this->session->set('XET', (object) ['messages' => [$result->content], 'code' => intval($result->code)]);
                return false;
            }
            return $result->content;
        } catch (\PDOException $e) {
            $this->session->set('XET', (object) ['messages' => [$e->getMessage()], 'code' => intval($e->getCode())]);
            return false;
        }
    }

    //
    // Static Section
    //

    /**
     * Select user by ID
     *
     * @param string $id	: Primary Key
     *
     * @return false|User
     */
    public static function getData(array $id, string $fields = '*', $override = null)
    {
        $source = get_called_class();
        $entity = (new $source())->setId($id);
        $entity->setSource($override ?? $entity->getsource());
        return $entity->select($fields, $source);
    }

    /**
     * Check Record existence in database
     *
     * @param array $param
     *
     * @return bool
     */
    public static function check(array $param, array $id, $override = null): bool
    {
        $source = get_called_class();
        $target = $override ?? (new $source())->getSource();
        $model = (new self())->setId($id);
        $script =
            "SELECT COUNT(*) FROM $target WHERE " .
            array_keys($param)[0] .
            ' = :' .
            array_keys($param)[0] .
            '  AND NOT (' .
            $model->getId() .
            ')';

        if (!$model->sql($script, $param + $id)) {
            return false;
        }
        $result = $model->getStatement()->fetchColumn();
        $model->getStatement()->closeCursor();
        return boolval($result);
    }

    /**
     * Select Entity by ID
     *
     * @param string $child	: Calling class
     * @param string $script	: SQL
     * @param array  $param	: Arguments
     *
     * @return object|null
     */
    protected static function record(string $srcipt, array $param, $class = self::class)
    {
        $model = new $class();
        $model->sql($srcipt, $param);
        return $model->getStatement()->fetchObject($class);
    }

    /**
     * Direct Call to Stored Function
     *
     * @param string	$name		: Name of Stored Function
     * @param array	$param		: Stored Function Parameters
     *
     * @return mixed
     */
    protected static function storedfunction(string $name, array $param)
    {
        $model = new self();
        if ($model->sf($name, $param) === false) {
            return false;
        }
        $result = $model->getStatement()->fetchColumn();
        $model->getStatement()->closeCursor();
        return $result;
    }

    /**
     * Direct Call to Stored Procedure
     *
     * @param string	$name		: Name of Stored Procedure
     * @param array	$param		: Stored Function Parameters
     *
     * @return false|self
     */
    protected static function storedProcedure(string $name, array $param = [])
    {
        $model = new self();
        if ($model->sp($name, $param) === false) {
            return false;
        }
        return $model;
    }

    /**
     * Select Entity by ID
     *
     * @param string $relation
     * @param string $field
     *
     * @return array
     */
    protected static function enum(string $relation, string $field): array
    {
        $model = new self();
        $model->sql("SHOW COLUMNS FROM $relation WHERE FIELD = :enum", ['enum' => $field]);
        $enum = $model->getStatement()->fetch(\PDO::FETCH_ASSOC)['Type'];
        $type = substr($enum, 6, strlen($enum) - 8);
        return explode("','", $type);
    }

    /**
     * List Records
     *
     * @return array
     */
    protected static function dql(string $script, array $param = null, int $mode = \PDO::FETCH_CLASS, $context = self::class): array
    {
        $model = new self();
        $model->sql($script, $param);
        return $model->getStatement()->fetchAll($mode, $context);
    }
}
?>
