<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Entity
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Model\Entity;

use Phalcon\Db\Adapter\Pdo\Mysql;
use Phiscal\Model\Base;
use Phiscal\Plugin\Identity\Acl;

/**
 * Index class
 *
 * @category  PHP
 * @package   Phiscal\Model\Entity
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @link      127.0.0.1
 */
class Index extends Base
{
    /**
     * Great Reset function
     *
     * @return bool
     */
    public static function greatreset(): bool
    {
        return false !== self::storedProcedure('greatreset')->verifySP();
    }

    /**
     * Notification System
     *
     * @param string $user	: Recepient
     * @param string $type	: Classification
     * @param string $icon	: Symbol
     * @param string $message	: Notification Content
     * @param string $module	: Source Module
     * @param string $delim	: Split Demiliter
     *
     * @return false|string
     */
    public static function notify(string $uId, string $type, string $icon, string $message, $module = null, string $delim = ';')
    {
        $param = [
            'uId' => $uId,
            'module' => $module,
            'type' => $type,
            'icon' => $icon,
            'message' => $message,
            'delim' => $delim,
        ];
        $result = self::storedProcedure('notify', $param);
        return $result === false ? false : $result->verifySP();
    }

    /**
     * Inquiries function
     *
     * @return bool
     */
    public function makeInquiry(): bool
    {
        $param = [
            'fname' => $this->firstname,
            'lname' => $this->lastname,
            'phone' => $this->phone,
            'email' => $this->email,
            'subject' => $this->subject,
            'content' => $this->content,
        ];
        $script =
            'INSERT INTO inquiry' .
            '(FIRST_NAME,LAST_NAME,PHONE,EMAIL,SUBJECT,CONTENT) ' .
            'VALUES(:fname,:lname,:phone,:email,:subject,:content)';
        return $this->sql($script, $param);
    }

    /**
     * List Inquiries
     *
     * @return array
     */
    public static function getInquiries(): array
    {
        return self::dql('SELECT QUEST_ID, CONCAT(FIRST_NAME, \' \', LAST_NAME) INQUIRER, SUBJECT, `DATE` FROM inquiry');
    }

    /**
     * All Controllers
     *
     * @return array
     */
    public static function getResources(): array
    {
        return self::dql('SELECT name, handler FROM resource');
    }

    /**
     * All Countries
     *
     * @return array
     */
    public static function getCountries(): array
    {
        return self::dql('SELECT alpha2Code id, name FROM earth.country');
    }

    /**
     * All Routes
     *
     * @return array
     */
    public static function getRoutes(): array
    {
        return self::dql('SELECT resource, method, route, action, preflight FROM v_resourceAction');
    }

    /**
     * Access Control List
     *
     * @param Acl &$acl
     *
     * @return array
     */
    public static function acl(Acl &$acl): array
    {
        $list = parent::dql('SELECT resource, description, route action FROM v_resourceAction WHERE private');
        $ls = [];
        foreach ($list as $item) {
            $acl->resourceDesc[$item->resource] = $item->description;
            $ls[$item->resource][] = explode('/', $item->action)[0];
        }
        return $ls;
    }
}
?>
