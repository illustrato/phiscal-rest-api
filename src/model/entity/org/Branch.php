<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Entity\Org
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2022 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Model\Entity\Org;

use Phiscal\Model\Base;

/**
 * Branch class
 *
 * @category  PHP
 * @package   Phiscal\Model\Entity\Org
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2022 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Branch extends Base
{
    /**
     * @param array $param = []
     */
    public function __construct(array $param = [])
    {
        parent::__construct($param);
        $this->source = 'branch';
    }

    /**
     * Return targe Record
     *
     * @return false|self
     */
    public static function getRecord(int $tenantId, int $id)
    {
        $query = 'SELECT * FROM branch WHERE ORG_ID = tenant(:tenant_id) AND branch_no = :id';
        $param = ['id' => $id, 'tenant_id' => $tenantId];
        $result = self::record($query, $param, self::class);
        if ($result === false) {
            return false;
        }
        return $result->setId(['ORG_ID' => $result->ORG_ID, 'BRANCH_NO' => $result->BRANCH_NO]);
    }

    /**
     * Return Branch List Related to an Organization
     * @param int $orgId	: Target organization ID
     *
     * @return array
     */
    public static function ls(int $tenantId): array
    {
        $query =
            'SELECT BRANCH_NO branch, main, photo, addressline1, addressline2, city, country, gps, telephone, description ' .
            'FROM branch WHERE ORG_ID = tenant(:tenantId)';
        return self::dql($query, ['tenantId' => $tenantId], \PDO::FETCH_FUNC, 'Phiscal\Model\Entity\Org\Branch::list');
    }

    /**
     * Properly formats list from database
     * @param *
     *
     * @return array
     */
    public static function list($branch, $main, $photo, $addr1, $addr2, $city, $country, $gps, $tel, $desc): array
    {
        return [
            'branch' => intval($branch),
            'main' => boolval($main),
            'photo' => $photo,
            'addressline1' => $addr1,
            'addressline2' => $addr2,
            'city' => $city,
            'country' => $country,
            'gps' => $gps,
            'telephone' => $tel,
            'description' => $desc,
        ];
    }
}
