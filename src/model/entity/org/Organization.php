<?php
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Entity\Org
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Model\Entity\Org;

use Phiscal\Model\Base;

/**
 * Tenant class
 *
 * @category  PHP
 * @package   Phiscal\Model\Entity\Org
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Organization extends Base
{
    /**
     * @param array $param = []
     * @param string $table = 'tenant'
     */
    public function __construct(array $param = [], string $table = 'organization')
    {
        parent::__construct($param);
        $this->source = $table;
    }

    /**
     * Returns Tenant Related Lists
     *
     * @return array
     */
    public static function ls(string $target, $org): array
    {
        $pieces = explode(':', $target);
        switch ($pieces[0]) {
            case 'structures':
                $sql = "SELECT STRUCT_ID id, IF(SUFFIX IS \N, NAME, CONCAT(NAME,' :: ', SUFFIX, '.')) name, description FROM structure";
                if (count($pieces) === 1) {
                    return parent::dql("$sql ORDER BY name DESC");
                }
                switch ($pieces[1]) {
                    case 'mnp':
                        return self::dql("{$sql} WHERE STRUCT_ID = :i1 OR STRUCT_ID = :i2", ['i1' => 'PTN', 'i2' => 'SOL']);
                    case 'ltd':
                        return self::dql("{$sql} WHERE STRUCT_ID <> :i1 AND STRUCT_ID <> :i2 ORDER BY NAME", [
                            'i1' => 'PTN',
                            'i2' => 'SOL',
                        ]);
                    default:
                        return [];
                }

            case 'registra':
                return self::dql("SELECT name FROM {$target}");
            default:
                return [];
        }
    }

    /**
     * Register New Organization
     *
     * @return bool|int
     */
    public function crud()
    {
        $param = [
            'tenantId' => $this->request->getTenant(),
            'name' => $this->name ?? null,
            'struct' => $this->struct_id ?? null,
            'regno' => $this->regno ?? null,
            'registra' => $this->registra ?? null,
            'web' => $this->website ?? null,
            'dba' => $this->dba ?? null,
            'desc' => $this->description ?? null,
            'motto' => $this->motto ?? null,
            'logo' => $this->logo ?? null,
            'module' => $this->module ?? null,
            'uid' => $this->session->get('UID'),
        ];
        $model = self::storedProcedure('crud_org', $param);
        if ($model === false) {
            return false;
        }
        return $model->verifySP();
    }

    /**
     * Delete Tenant
     *
     * @return void
     */
    public function rm()
    {
        $param = [
            'tenantId' => $this->request->getTenant(),
            'confirmation' => $this->confirmation,
            'uid' => $this->session->get('UID'),
        ];
        $result = self::storedProcedure('rm_org', $param);
        if ($result === false) {
            return false;
        }
        return $result->verifySP();
    }

    /**
     * Switch Headquarters
     *
     * @param int $id
     *
     * @return false|array
     */
    public function mainswitch(int $id)
    {
        $param = [
            'tenant' => $this->request->getTenant(),
            'target' => $this->getSource(),
            'id' => $id,
            'action' => $this->positive ? 1 : 0,
            'user' => $this->session->get('UID'),
        ];
        $model = self::storedProcedure('mainswitch', $param);
        if ($model === false) {
            return false;
        }
        $results = [];
        switch ($this->getSource()) {
            case 'branch':
                $results = $model
                    ->getStatement()
                    ->fetchAll(\PDO::FETCH_FUNC, function ($branch, $main, $photo, $addr1, $addr2, $city, $country, $gps, $tel, $desc) {
                        return [
                            'branch' => intval($branch),
                            'main' => boolval($main),
                            'photo' => $photo,
                            'addressline1' => $addr1,
                            'addressline2' => $addr2,
                            'city' => $city,
                            'country' => $country,
                            'gps' => $gps,
                            'telephone' => $tel,
                            'description' => $desc,
                        ];
                    });
                break;
            case 'logo':
                $results = $model->getStatement()->fetchAll(\PDO::FETCH_FUNC, function ($id, $file, $size, $main, $desc, $x, $y, $ext) {
                    return [
                        'id' => intval($id),
                        'file' => $file,
                        'size' => intval($size),
                        'main' => boolval($main),
                        'description' => $desc,
                        'width' => intval($x),
                        'height' => intval($y),
                        'ext' => $ext,
                    ];
                });
                break;
        }
        return $model->verifySP() === false ? false : $results;
    }

    /**
     * Get Tenant Information
     *
     * @param int $id
     *
     * @return Tenant
     */
    public static function publicInfo(int $id): self
    {
        $target = (new self())->setId(['org_id' => $id]);
        $target->logos = [];
        $target->branches = [];
        $target->staff = [];

        $records = parent::dql('SELECT * FROM v_org_depth WHERE org_id = tenant(:id)', [
            'id' => $id,
        ]);
        foreach ($records as $record) {
            $target->name = $record->name;
            $target->struct_id = $record->struct_id;
            $target->structure = $record->structure;
            $target->regno = $record->regno;
            $target->dba = $record->dba;
            $target->registra = $record->registra;
            $target->website = $record->website;
            $target->description = $record->description;
            $target->motto = $record->motto;
            $target->gatewayname = '???';
            $target->client_id = '???';
            $target->access_key = '???';
            $target->logos["{$record->logo}"] = [
                'file' => "{$record->logo}.{$record->ext}",
                'id' => intval($record->logo),
                'width' => intval($record->width),
                'height' => intval($record->height),
                'main' => boolval($record->mainLogo),
                'size' => intval($record->size),
                'ext' => $record->ext,
                'description' => $record->logoDesc,
            ];
            $target->branches["{$record->branch}"] = [
                'photo' => $record->photo,
                'branch' => intval($record->branch),
                'main' => boolval($record->mainBranch),
                'addressline1' => $record->addressline1,
                'addressline2' => $record->addressline2,
                'city' => $record->city,
                'region' => $record->region,
                'country' => $record->country,
                'gps' => $record->gps,
                'telephone' => $record->telephone,
                'description' => $record->branchDesc,
                'staff' => intval($record->staff),
            ];
            $target->staff[$record->user_id] = [
                'user_id' => $record->user_id,
                'fullname' => $record->fullname,
                'firstname' => $record->firstname,
                'lastname' => $record->lastname,
                'dob' => $record->dob,
                'age' => intval($record->age),
                'idnumber' => $record->idnumber,
                'email' => $record->email,
                'phone' => $record->phone,
                'nationality' => $record->nationality,
                'avatar' => $record->avatar,
                'active' => $record->active,
                'state' => $record->state,
                'report' => $record->report,
                'workEmail' => $record->workEmail,
                'telephone' => $record->workTelephone,
                'empBranch' => intval($record->empBranch),
                'branchAddress' => $record->branchAddress,
                'branchCity' => $record->branchCity,
                'branchRegion' => $record->branchRegion,
                'branchCountry' => $record->branchCountry,
            ];
        }
        return $target;
    }

    /**
     * Get Tenant Information
     *
     * @param int $id
     *
     * @return Tenant
     */
    public static function adminInfo(int $id, string $uid): self
    {
        $target = (new self())->setId(['org_id' => $id]);
        $target->logos = [];
        $target->branches = [];
        $target->staff = [];

        $records = parent::dql('SELECT * FROM v_org_depth WHERE org_id = tenant(:id) AND admin = :uid', [
            'id' => $id,
            'uid' => $uid,
        ]);
        foreach ($records as $record) {
            $target->name = $record->name;
            $target->struct_id = $record->struct_id;
            $target->structure = $record->structure;
            $target->regno = $record->regno;
            $target->dba = $record->dba;
            $target->registra = $record->registra;
            $target->website = $record->website;
            $target->description = $record->description;
            $target->motto = $record->motto;
            $target->gatewayname = $record->gatewayName;
            $target->provider_id = $record->gatewayProvider;
            $target->client_id = $record->gatewayClient;
            $target->access_key = $record->gatewayKey;
            $target->logos["{$record->logo}"] = [
                'file' => "{$record->logo}.{$record->ext}",
                'id' => intval($record->logo),
                'width' => intval($record->width),
                'height' => intval($record->height),
                'main' => boolval($record->mainLogo),
                'size' => intval($record->size),
                'ext' => $record->ext,
                'description' => $record->logoDesc,
            ];
            $target->branches["{$record->branch}"] = [
                'photo' => $record->photo,
                'branch' => intval($record->branch),
                'main' => boolval($record->mainBranch),
                'addressline1' => $record->addressline1,
                'addressline2' => $record->addressline2,
                'city' => $record->city,
                'region' => $record->region,
                'country' => $record->country,
                'gps' => $record->gps,
                'telephone' => $record->telephone,
                'description' => $record->branchDesc,
                'staff' => intval($record->staff),
            ];
            $target->staff[$record->user_id] = [
                'user_id' => $record->user_id,
                'fullname' => $record->fullname,
                'firstname' => $record->firstname,
                'lastname' => $record->lastname,
                'dob' => $record->dob,
                'age' => intval($record->age),
                'idnumber' => $record->idnumber,
                'email' => $record->email,
                'phone' => $record->phone,
                'nationality' => $record->nationality,
                'avatar' => $record->avatar,
                'active' => $record->active,
                'state' => $record->state,
                'report' => $record->report,
                'workEmail' => $record->workEmail,
                'telephone' => $record->workTelephone,
                'empBranch' => intval($record->empBranch),
                'branchAddress' => $record->branchAddress,
                'branchCity' => $record->branchCity,
                'branchRegion' => $record->branchRegion,
                'branchCountry' => $record->branchCountry,
            ];
        }
        return $target;
    }
}
?>
