<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Entity\Org
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Model\Entity\Org;

use Phiscal\Model\Base;

/**
 * Employee class
 *
 * @category  PHP
 * @package   Phiscal\Model\Entity\Org
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Employee extends Base
{
    /**
     * @param array $param = []
     * @param string $table = 'tenant'
     */
    public function __construct(array $param = [], string $table = 'employee')
    {
        parent::__construct($param);
        $this->source = $table;
    }

    /**
     * Create or Update Employee
     *
     * @param string $uId	: Target User ID if this is an update
     *
     * @return false|self
     */
    public function crud(string $uId)
    {
        $param = [
            'uId' => $uId === 'new' ? null : $uId,
            'fname' => $this->firstname ?? null,
            'lname' => $this->lastname ?? null,
            'nation' => $this->nationality ?? null,
            'idno' => $this->idnumber ?? null,
            'dob' => $this->dob ?? date('Y-m-d'),
            'umail' => $this->email ?? null,
            'wmail' => $this->workEmail ?? null,
            'telephone' => $this->telephone ?? null,
            'branch' => $this->branch ?? 0,
            'org' => $this->request->getTenant(),
            'photo' => $this->photo ?? null,
            'admin' => $this->session->get('UID'),
            'skipmail' => $this->config->app->useMail ? 0 : 1,
        ];
        $model = self::storedProcedure('crud_emp', $param);
        if ($model === false) {
            return false;
        }
        $employee = $model->getStatement()->fetchObject(self::class);
        $result = $model->verifySP();
        if ($result === false) {
            return false;
        }
        $employee->setId(['id' => $employee->user_id]);
        $employee->cert = json_decode($result);
        return $employee;
    }

    /**
     * Return Logo List Related to an Organization
     * @param int $orgId	: Target organization ID
     *
     * @return array
     */
    public static function ls(int $tenantId): array
    {
        $query = 'SELECT * FROM v_staff WHERE organization = tenant(:tenantId)';
        return self::dql($query, ['tenantId' => $tenantId], \PDO::FETCH_FUNC, 'Phiscal\Model\Entity\Org\Employee::list');
    }

    /**
     * Photo Update
     *
     * @return false|string
     */
    public function Photo()
    {
        $param = [
            'target' => $this->getId(true)['user_id'],
            'tenantId' => $this->request->getTenant(),
            'photo' => $this->photo ?? null,
            'uid' => $this->session->get('UID'),
        ];
        $model = self::storedProcedure('crud_avatar', $param);
        if ($result === false) {
            return false;
        }
        $staff = $model->getStatement()->fetchAll(\PDO::FETCH_FUNC, 'Phiscal\Model\Entity\Org\Employee::list');
        $result = $model->verifySP();
        if ($result === false) {
            return false;
        }
        return $staff;
    }

    /**
     * Properly formats list from database
     * @param *
     *
     * @return array
     */
    public static function list(
        $id,
        $name,
        $fname,
        $lname,
        $age,
        $dob,
        $idno,
        $email,
        $phone,
        $nation,
        $pic,
        $active,
        $org,
        $workEmail,
        $workTel,
        $branchNo,
        $address,
        $city,
        $region,
        $country,
        $state,
        $report
    ): array {
        return [
            'user_id' => $id,
            'fullname' => $name,
            'firstname' => $fname,
            'lastname' => $lname,
            'age' => intval($age),
            'dob' => $dob,
            'idnumber' => $idno,
            'email' => $email,
            'workEmail' => $workEmail,
            'phone' => $phone,
            'telephone' => $workTel,
            'nationality' => $nation,
            'avatar' => $pic,
            'active' => intval($active),
            'organization' => $org,
            'empBranch' => intval($branchNo),
            'branchAddress' => $address,
            'branchCity' => $city,
            'branchRegion' => $region,
            'branchCountry' => $country,
            'state' => $state,
            'report' => $report,
        ];
    }
}
?>
