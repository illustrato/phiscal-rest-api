<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Modules\Entity\User
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Model\Entity\User;

use Phiscal\Model\Base;

/**
 * Token class
 *
 * @category  PHP
 * @package   Phiscal\Modules\Entity\User
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Token extends Base
{
    /**
     * @param array $param = []
     * @param string $table = 'tenant'
     */
    public function __construct(array $param = [], string $table = 'token')
    {
        parent::__construct($param);
        $this->source = $table;
    }

    /**
     * New Token
     *
     * @return : bool
     */
    public function add(): bool
    {
        $param = [
            'id' => $this->token_id,
            'token' => $this->token,
            'type' => $this->type,
            'uid' => $this->user_id,
            'agent' => $this->userAgent ?? null,
        ];
        return false !== self::storedProcedure('newtoken', $param)->verifySP();
    }

    /**
     * Delete Authentication Tokens
     *
     * @param string $uid User Id
     *
     * @return bool
     */
    public static function rm(string $uid): bool
    {
        return (new parent())->sql('DELETE FROM token WHERE USER_ID = :uid', ['uid' => $uid]);
    }

    /**
     * Token object by Token string
     *
     * @return Token|false
     */
    public static function fetch(string $token)
    {
        return parent::record('SELECT * FROM token WHERE `TOKEN` = :token', ['token' => $token], self::class);
    }
}
?>
