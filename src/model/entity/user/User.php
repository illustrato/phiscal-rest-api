<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Modules\Entity\User
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Model\Entity\User;

use Phalcon\Security\JWT\Signer\Hmac;
use Phalcon\Security\JWT\Builder;
use Phalcon\Security\JWT\Token\Token;
use Phiscal\Model\Base;
use Phiscal\Traiting\Token as TokenTrait;
/**
 * All the users registered in the application
 *
 * @category  PHP
 * @package   Phiscal\Modules\Entity\User
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @link      127.0.0.1
 */
class User extends Base
{
    use TokenTrait;

    /**
     * @param array $param = []
     * @param string $table = 'tenant'
     */
    public function __construct(array $param = [], string $target = 'user')
    {
        parent::__construct($param);
        $this->source = $target;
    }

    /**
     * User List function
     *
     * @return array
     */
    public static function ls(): array
    {
        return self::dql('SELECT * FROM v_activeUser', null, \PDO::FETCH_CLASS, self::class);
    }

    /**
     * Signup function
     *
     * @return bool|User
     */
    public function signup()
    {
        $param = [
            'fname' => $this->firstname,
            'lname' => $this->lastname,
            'idno' => $this->idnumber,
            'nation' => $this->nationality,
            'email' => $this->email,
            'passwd' => $this->security->hash($this->password),
            'agent' => $this->module ?? null,
            'skipmail' => $this->config->app->useMail ? 0 : 1,
        ];
        $model = self::storedProcedure('signup', $param);
        if ($model === false) {
            return false;
        }
        $user = $model->getStatement()->fetchObject(self::class);
        $result = $model->verifySP();
        if ($result === false) {
            return false;
        }
        $account = (new self(json_decode($user->user, true)))->setId(['USER_ID' => $user->USER_ID]);
        $account->token = $result;
        return $account;
    }

    /**
     *	Email Verification
     *
     * @param string $token
     *
     * @return bool
     */
    public function emailVerification()
    {
        $param = ['src' => $this->target, 'id' => $this->user_id, 'utoken' => $this->token];
        $result = self::storedProcedure('emailverification', $param);
        return $result === false ? false : $result->verifySP();
    }

    /**
     *	Email Update
     *
     * @param string $token
     *
     * @return bool|string
     */
    public function updateEmail(string $token)
    {
        $param = ['id' => $this->user_id, 'utoken' => $token];
        $result = self::storedProcedure('emailreset', $param);
        return $result === false ? false : $result->verifySP();
    }

    /**
     * Avatar Update
     *
     * @return false|string
     */
    public function avatar()
    {
        $param = [
            'target' => 'self',
            'tenantId' => $this->request->getTenant(),
            'avatar' => $this->avatar ?? null,
            'uid' => $this->getId(true)['user_id'],
        ];
        $result = self::storedProcedure('crud_avatar', $param);
        return $result === false ? false : $result->verifySP();
    }

    /**
     *	Email Verification
     *
     * @param string $token
     *
     * @return bool
     */
    public function crudFreelance()
    {
        $param = [
            'alias' => $this->alias ?? null,
            'ext' => $this->module ?? null,
            'provider' => $this->provider_id ?? null,
            'client' => $this->client_id ?? null,
            'access' => $this->access_key ?? null,
            'uid' => $this->session->get('UID'),
        ];
        $result = self::storedProcedure('crud_freelance', $param);
        return $result === false ? false : $result->verifySP();
    }

    /**
     * Register login attempt
     *
     * @param string $RMT	: Remember Me Token
     *
     * @return bool
     */
    public function signin(string $RMT = null): bool
    {
        $param = [
            'id' => $this->user_id,
            'ip' => $this->request->getClientAddress(),
            'agent' => $this->request->getUserAgent(),
            'remember' => isset($RMT) ? 1 : 0,
            'RMT' => $RMT,
        ];
        $result = self::storedProcedure('auth', $param);
        return $result === false ? false : false !== $result->verifySP();
    }

    /**
     * undocumented function
     *
     * @return false|self
     */
    public static function getTenant(string $uid)
    {
        $query =
            'SELECT ALIAS, p.* FROM tenant ' .
            'LEFT JOIN paymentgateway p USING (`TENANT_ID`) ' .
            'INNER JOIN user USING(`USER_ID`) ' .
            'WHERE USER_ID = :uid';
        return self::record($query, ['uid' => $uid], self::class);
    }

    /**
     * Returns Titles to use in an Input Form
     *
     * @return object
     */
    public static function getTitles()
    {
        return self::enum('user', 'TITLE');
    }

    /**
     * Delete User Record
     *
     * @param string $email Id
     *
     * @return PDO
     */
    public static function rm(string $email)
    {
        $model = (new self())->setId(['email' => $email, 'active' => false]);
        $pdo = $model->delete();
        return $pdo->query('SELECT @AVATAR avatar')->fetch(\PDO::FETCH_OBJ)->avatar;
    }

    /**
     * Register failed login attempt
     *
     * @param array $param
     *
     * @return int
     * @throws Exception
     */
    public static function loginFail(array $param)
    {
        $model = new self();
        return intval(self::storedfunction('signinfail', $param));
    }

    /**
     * Read Notifications
     *
     * @return false|object
     */
    public function readFeeds()
    {
        $param = ['user' => $this->session->get('UID')];
        $result = self::storedfunction('readfeeds', $param);
        return $result === false ? false : json_decode($result);
    }

    /**
     * Account State
     *
     * @return string
     */
    public function flag()
    {
        return $this->record('SELECT HINT FROM account_state WHERE ACCST_ID = :state', ['state' => $this->active], self::class)->HINT;
    }

    /**
     * Create Remember Token
     *
     * @return string|bool
     */
    public function rememberToken()
    {
        $param = ['id' => $this->id, 'agent' => $this->request->getUserAgent()];
        return self::storedfunction('remembertoken', $param);
    }

    /**
     * Reset password request
     *
     * @return User|bool
     */
    public function forgotPasswd()
    {
        $param = ['email' => $this->email, 'agent' => $this->request->getUserAgent()];
        $model = self::storedProcedure('forgotpasswd', $param);
        if ($model === false) {
            return false;
        }
        $user = $model->getStatement()->fetchObject(self::class);
        $result = $model->verifySP();
        if ($result === false) {
            return false;
        }
        $user->token = $result;
        return $user;
    }

    /**
     * Reset password function
     *
     * @param bool $authentic	: User provided rge correct password or not
     *
     * @return string|bool
     */
    public function passwdReset(bool $authentic)
    {
        $param = [
            'uid' => $this->session->get('UID'),
            'authentic' => $authentic ? 1 : 0,
            'tkn' => $this->request->getHeader('PSD'),
            'password' => $this->password,
            'ip' => $this->request->getClientAddress(),
            'agent' => $this->request->getUserAgent(),
        ];
        $result = self::storedProcedure('passwdreset', $param);
        return $result === false ? false : $result->verifySP();
    }

    /**
     * User Session by User Id
     *
     * @param string $uId
     * @param int $tenant
     *
     * @return bool|array
     */
    public static function getSession(string $uId, int $tenant)
    {
        $user = self::record('SELECT verify(:uId,:tenantId) session', ['uId' => $uId, 'tenantId' => $tenant]);
        if ($user === false) {
            return $user;
        }
        $session = json_decode($user->session, true);
        $session['user']['tenants'] = json_decode($session['user']['tenants'] ?? '[]');
        return boolval($session['user'])
            ? [
                'user' => new User($session['user']),
                'tenant' => boolval($session['tenant']) ? $session['tenant'] : false,
                'rank' => $session['rank'],
                'modules' => $session['modules'],
                'feeds' => $session['feed'],
            ]
            : false;
    }

    /**
     * Authentication JWT
     *
     * @return Token
     */
    public function getToken(): Token
    {
        $signer = new Hmac($this->getTokenAlgorithm());
        $builder = new Builder($signer);
        $now = $this->getTokenTimeIssuedAt();

        return $builder
            ->setAudience($this->getTokenAudience()) // aud
            ->setContentType('application/json') // cty - header
            ->setExpirationTime($this->getTokenTimeExpiration()) // exp
            ->setId("{$this->user_id}$now") // jti
            ->setIssuedAt($now) // iat
            ->setIssuer($this->username ?? 'Anonymous') // iss
            ->setNotBefore($this->getTokenTimeNotBefore()) // nbf
            ->setPassphrase('QcMpZ&' . sha1("{$this->user_id}$now"))
            ->setSubject($this->user_id) // sub
            ->getToken();
    }
}
?>
