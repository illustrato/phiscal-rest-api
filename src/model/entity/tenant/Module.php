<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Modules\Entity\Tenant
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Model\Entity\Tenant;

use Phalcon\Db\Adapter\Pdo\Mysql;
use Phiscal\Model\Base;

/**
 * Module class
 *
 * @category  PHP
 * @package   Phiscal\Modules\Entity\Tenant
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @link      127.0.0.1
 */
class Module extends Base
{
    /**
     * @param array $param = []
     * @param string $table = 'tenant'
     */
    public function __construct(array $param = [], string $table = 'module')
    {
        parent::__construct($param);
        $this->source = $table;
    }

    /**
     * User List function
     *
     * @return array
     */
    public static function ls($list = 'all', $tenant = null): array
    {
        $items = [];
        $fields = 'mod_id, name, description, webclient, logo';
        switch ($list) {
            case 'all':
                $items = self::dql('SELECT * FROM module ORDER BY NAME', null, \PDO::FETCH_CLASS, self::class);
                break;
            case 'tenant':
                $sql = "SELECT $fields, days FROM v_module WHERE tenant_id = :target";
                $items = self::dql($sql, ['target' => $tenant], \PDO::FETCH_CLASS, self::class);
                break;
            case 'active':
                $sql = "SELECT $fields FROM v_module WHERE tenant_id = :target AND days <= 30";
                $items = self::dql($sql, ['target' => $tenant], \PDO::FETCH_CLASS, self::class);
                break;
            case 'idling':
                $sql = "SELECT $fields FROM v_module WHERE tenant_id = :target AND days > 30";
                $sql = "$sql AND mod_id NOT IN (SELECT mod_id FROM v_module WHERE tenant_id = :target AND days <= 30)";
                $sql = "$sql GROUP BY mod_id";
                $items = self::dql($sql, ['target' => $tenant], \PDO::FETCH_CLASS, self::class);
                break;
        }
        return $items;
    }

    /**
     * Subscribe Curent Entity to Module
     *
     * @return bool
     */
    public function subscribe(): bool
    {
        $param = [
            'target' => $this->request->getTenant(),
            'provider' => $this->mod_id,
            'uid' => $this->session->get('UID'),
        ];
        $model = self::storedProcedure('subscribe', $param);
        if ($model === false) {
            return false;
        }
        return false !== $model->verifySP();
    }
}
?>
