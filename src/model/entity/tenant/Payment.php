<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Modules\Entity\Tenant
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Model\Entity\Tenant;

use Phiscal\Model\Base;

/**
 * Payment class
 *
 * @category  PHP
 * @package   Phiscal\Modules\Entity\Tenant
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Payment extends Base
{
    /**
     * @param array $param = []
     * @param string $table = 'tenant'
     */
    public function __construct(array $param = [], string $table = 'paymentgateway')
    {
        parent::__construct($param);
        $this->source = $table;
    }

    /**
     * Returns Payment Related Lists
     *
     * @return array
     */
    public static function ls(string $proxy): array
    {
        $target = ['provider' => 'paygateprovider'];
        return self::dql("SELECT * FROM {$target[$proxy]}");
    }

    /**
     * Add/Update ayment Gateway
     *
     * @param int $entityId:	TENANT_ID or USER_ID, in that order
     *
     * @return bool
     */
    public function paygate(int $entityId): bool
    {
        $param = [
            'target' => $entityId,
            'provider' => $this->provider_id,
            'client' => $this->client_id,
            'access' => $this->access_key,
            'uid' => $this->session->get('UID'),
        ];
        $result = self::storedProcedure('paygate', $param);
        return $result === false ? false : $result->verifySP() !== false;
    }

    /**
     * Get Payment Details
     *
     * @param int 	$entity	: Owner
     *
     * @return false|Payment
     */
    public function getDetails($subject)
    {
        $sql = 'SELECT g.NAME provider, g.provider_id, client_id, access_key FROM paymentgateway p';
        $sql = "$sql INNER JOIN paygateprovider g USING (PROVIDER_ID) WHERE `TENANT_ID` = :subject";
        return $this->record($sql, ['subject' => $subject], self::class);
    }
}
?>
