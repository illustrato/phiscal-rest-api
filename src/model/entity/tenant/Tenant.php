<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Entity\Tenant
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2022 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Model\Entity\Tenant;

use Phiscal\Model\Base;

/**
 * Tenant class
 *
 * @category  PHP
 * @package   Phiscal\Model\Entity\Tenant
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2022 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Tenant extends Base
{
    /**
     * @param array $param = []
     * @param string $table = 'tenant'
     */
    public function __construct(array $param = [], string $table = 'tenant')
    {
        parent::__construct($param);
        $this->source = $table;
    }

    /**
     * Get list of Tenants belonging to an Admin
     *
     * @param $uid	: Admin User Id
     *
     * @return array
     */
    public static function adminList(string $uid): array
    {
        $script =
            'SELECT rank, business, modules FROM v_user_tenant a ' .
            'INNER JOIN v_tenant USING(tenant_id) ' .
            'WHERE USER_ID = :uid ORDER BY type DESC';
        $tenants = self::dql($script, ['uid' => $uid], \PDO::FETCH_FUNC, function ($rank, $business, $modules) {
            return ['rank' => $rank, 'tenant' => json_decode($business), 'modules' => json_decode($modules)];
        });
        return $tenants;
    }
}
