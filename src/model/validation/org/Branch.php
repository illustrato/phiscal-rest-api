<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Validation\Org
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Model\Validation\Org;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Callback;
use Phiscal\Model\Entity\Org\Branch as Model;

/**
 * Branch class
 *
 * @category  PHP
 * @package   Phiscal\Model\Validation\Org
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Branch extends Validation
{
    /**
     * @var array $id
     */
    private $id;

    /**
     * @var bool $new
     */
    private $new;

    /**
     * @param bool $new = true
     */
    public function __construct(bool $new = true)
    {
        $this->new = $new;
        parent::__construct();
    }

    /**
     * Initialize components
     *
     * @return void
     */
    public function initialize()
    {
        $this->setLabels([
            'addressline1' => 'Address Line 1',
            'addressline2' => 'Address Line 2',
            'city' => 'City',
            'country' => 'Country',
            'description' => 'Description',
            'gps' => 'GPS Coordinates',
            'photo' => 'Photograph',
            'telephone' => 'Telephone',
        ]);

        if ($this->new) {
            $this->add(
                ['addressline1', 'gps', 'country'],
                new PresenceOf([
                    'message' => ':field Required',
                    'cancelOnFail' => true,
                ])
            );
        }

        $this->add(
            'addressline1',
            new StringLength([
                'max' => 45,
                'min' => 9,
                'allowEmpty' => true,
                'messageMaximum' => ':field should be at most 45 characters',
                'messageMinimum' => ':field should be at least 9 characters',
            ])
        );
        $this->add(
            'description',
            new StringLength([
                'max' => 45,
                'min' => 3,
                'allowEmpty' => true,
                'messageMaximum' => ':field should be at most 45 characters',
                'messageMinimum' => ':field should be at least 9 characters',
            ])
        );

        $this->add(
            'country',
            new Regex([
                'pattern' => '/^[a-zA-Z]{2}$/',
                'message' => 'Use ISO 3166-1 2-letter for the :field',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            'telephone',
            new Regex([
                'pattern' => '/^\+(?:[0-9] ?){6,14}[0-9]$/',
                'message' => 'Invalid :field',
                'allowEmpty' => true,
            ])
        );
        $this->add(
            'telephone',
            new Callback([
                'callback' => function ($data) {
                    if (!Model::check(['TELEPHONE' => $data['telephone']], $this->id, 'branch')) {
                        return true;
                    }
                    return false;
                },
                'message' => 'Matching :field found in Database',
                'allowEmpty' => true,
            ])
        );
    }

    /**
     * Initialize Id Field
     *
     * @param array $param
     *
     * @return Branch
     */
    public function setId(array $param): self
    {
        $this->id = $param;
        return $this;
    }
}
?>
