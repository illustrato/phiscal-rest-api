<?php
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Validation\Org
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Model\Validation\Org;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Url;
use Phalcon\Validation\Validator\StringLength;

/**
 * Tenant class
 *
 * @category  PHP
 * @package   Phiscal\Model\Validation\Org
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Organization extends Validation
{
    /**
     * Initialize components
     *
     * @return void
     */
    public function initialize()
    {
        $this->setLabels([
            'name' => 'Company Name',
            'struct_id' => 'Business Structure',
            'regno' => 'Registration Number',
            'registra' => 'Registrar Commission',
            'website' => 'Website',
            'motto' => 'Slogan',
            'dba' => 'Doing Business As',
            'description' => 'Description',
        ]);

        if (!$this->request->inTenant()) {
            $this->add(
                ['name', 'struct_id'],
                new PresenceOf([
                    'message' => ':field Required',
                    'cancelOnFail' => true,
                ])
            );
        }

        $this->add(
            'website',
            new Url([
                'message' => ':field must be a url',
                'cancelOnFail' => true,
                'allowEmpty' => true,
            ])
        );

        $this->add(
            'motto',
            new StringLength([
                'max' => 95,
                'min' => 5,
                'messageMaximum' => ':field too long',
                'messageMinimum' => ':field too short',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            'description',
            new StringLength([
                'max' => 255,
                'min' => 10,
                'messageMaximum' => ':field should be at most 255 characters',
                'messageMinimum' => ':field should be at least 10 characters',
                'allowEmpty' => true,
            ])
        );
    }
}
?>
