<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Validation\Org
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2022 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Model\Validation\Org;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\Callback;
use Phiscal\Model\Entity\Org\Employee as Model;
use Phiscal\Model\Validator\IdNo\ZA as IdZA;
use Phiscal\Model\Validator\Date as DateISO;

/**
 * Employee class
 *
 * @category  PHP
 * @package   Phiscal\Model\Validation\Org
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2022 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Employee extends Validation
{
    /**
     * @var array $key
     */
    private $key;

    /**
     * @param array $key
     */
    public function __construct(array $key)
    {
        $this->key = $key;
        parent::__construct();
    }

    /**
     * Initialize components
     *
     * @return void
     */
    public function initialize()
    {
        $this->setLabels([
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'email' => 'Personal Email Address',
            'workEmail' => 'Work Email Address',
            'telephone' => 'Work Telephone',
            'idnumber' => 'Id Number',
            'dob' => 'Date of Birth',
            'nationality' => 'Nationality',
        ]);

        if ($this->key['USER_ID'] === 'new') {
            $this->add(
                ['firstname', 'lastname', 'nationality', 'email'],
                new PresenceOf([
                    'message' => ':field Required',
                    'cancelOnFail' => true,
                ])
            );
        }

        $this->add(
            ['firstname', 'lastname'],
            new StringLength([
                'max' => 20,
                'min' => 2,
                'allowEmpty' => true,
                'messageMaximum' => ':field too long',
                'messageMinimum' => ':field too short',
            ])
        );

        $this->add(
            ['firstname', 'lastname'],
            new Regex([
                'pattern' =>
                    '/^[A-Za-z\x{00C0}-\x{00FF}][A-Za-z\x{00C0}-\x{00FF}\'\-]+([\ A-Za-z\x{00C0}-\x{00FF}][A-Za-z\x{00C0}-\x{00FF}\'\-]+)*/u',
                'message' => 'Invalid :field',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            ['email', 'workEmail'],
            new Email([
                'message' => 'Invalid :field',
                'allowEmpty' => true,
            ])
        );
        $this->add(
            'email',
            new Callback([
                'callback' => function ($data) {
                    if (!Model::check(['EMAIL' => $data['email']], $this->key, 'user')) {
                        return true;
                    }
                    return false;
                },
                'message' => 'Matching :field found in Database',
                'allowEmpty' => true,
            ])
        );
        $this->add(
            'workEmail',
            new Callback([
                'callback' => function ($data) {
                    if (!Model::check(['EMAIL' => $data['workEmail']], $this->key)) {
                        return true;
                    }
                    return false;
                },
                'message' => 'Matching :field found in Database',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            'nationality',
            new Regex([
                'pattern' => '/^[a-zA-Z]{2}$/',
                'message' => 'Use ISO 3166-1 2-letter for the :field',
                'allowEmpty' => true,
            ])
        );
        $this->add(
            'nationality',
            new Callback([
                'callback' => function ($data) {
                    switch ($data['nationality']) {
                        case 'ZA':
                            if (empty(trim($data['idnumber']))) {
                                return false;
                            }
                            return true;
                            break;
                        default:
                            return true;
                            break;
                    }
                },
                'message' => 'Id Number is Required',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            'idnumber',
            new Callback([
                'callback' => function ($data) {
                    switch ($data['nationality']) {
                        case 'ZA':
                            if (empty(trim($data['idnumber']))) {
                                $this->appendMessage(new Message('Id Number Required'));
                                return false;
                            }
                            return new IdZA(['message' => 'Invalid South African ID Number']);
                            break;
                        default:
                            return true;
                            break;
                    }
                },
                'cancelOnFail' => true,
                'allowEmpty' => true,
            ])
        );

        $this->add('dob', new DateISO(['message' => 'Invalid Date of Birth', 'cancelOnFail' => true, 'allowEmpty' => true]));
        $this->add(
            'dob',
            new Callback([
                'callback' => function ($data) {
                    switch ($data['nationality']) {
                        case 'ZA':
                            if (empty(trim($data['idnumber']))) {
                                return true;
                            }
                            $date = \DateTime::createFromFormat('ymd', substr($data['idnumber'], 0, 6));
                            return $data['dob'] == $date->format('Y-m-d');
                            break;
                        default:
                            return true;
                            break;
                    }
                },
                'message' => 'Date of Birth incosistent with ID Number',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            'telephone',
            new Regex([
                'pattern' => '/^\+(?:[0-9] ?){6,14}[0-9]$/',
                'message' => 'Invalid :field',
                'allowEmpty' => true,
            ])
        );
        $this->add(
            'telephone',
            new Callback([
                'callback' => function ($data) {
                    if (!Model::check(['TELEPHONE' => $data['telephone']], $this->key)) {
                        return true;
                    }
                    return false;
                },
                'message' => 'Matching :field found in Database',
                'allowEmpty' => true,
            ])
        );
    }
}
