<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Validation\Etc
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Model\Validation\Etc;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;
/**
 * Inquiry class
 *
 * @category  PHP
 * @package   Phiscal\Model\Validation\Etc
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Inquiry extends Validation
{
    /**
     * Initialize components
     *
     * @return void
     */
    public function initialize()
    {
        $this->setLabels([
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'email' => 'Email Address',
            'phone' => 'Phone Number',
            'subject' => 'Inquiry Subject',
            'content' => 'Inquiry Message',
        ]);

        $this->add(
            ['firstname', 'lastname', 'email', 'subject', 'content'],
            new PresenceOf([
                'message' => ':field Required',
                'cancelOnFail' => true,
            ])
        );

        $this->add(
            ['firstname', 'lastname'],
            new StringLength([
                'max' => 20,
                'min' => 2,
                'messageMaximum' => ':field too long',
                'messageMinimum' => ':field too short',
            ])
        );

        $this->add(
            'email',
            new Email([
                'message' => 'Invalid :field',
            ])
        );

        $this->add(
            'phone',
            new Regex([
                'message' => 'Invalid :field',
                'pattern' => '/^\+(?:[0-9] ?){6,14}[0-9]$/',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            ['subject', 'content'],
            new StringLength([
                'min' => ['subject' => 5, 'content' => 20],
                'max' => ['subject' => 90, 'content' => 255],
                'messageMinimum' => ':field Too Short',
                'messageMaximum' => ':field Too Long',
            ])
        );
    }
}
?>
