<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Validation\User
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Model\Validation\User;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Messages\Message;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\Callback;
use Phiscal\Model\Entity\User\User as Model;
use Phiscal\Model\Validator\Date as DateISO;
use Phiscal\Model\Validator\Password;
use Phiscal\Model\Validator\IdNo\ZA as IdZA;

/**
 * User class
 *
 * @category  PHP
 * @package   Phiscal\Model\Validation\User
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class User extends Validation
{
    /**
     * @var bool $new
     */
    private $new;

    /**
     * @param string $target = ''
     */
    public function __construct(bool $new = true)
    {
        $this->new = $new;
        parent::__construct();
    }

    /**
     * Initialize components
     *
     * @return void
     */
    public function initialize()
    {
        $this->setLabels([
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'email' => 'Email Address',
            'idnumber' => 'Id Number',
            'dob' => 'Date of Birth',
            'nationality' => 'Nationality',
            'newPassword' => 'New Password',
            'phone' => 'Phone Number',
            'username' => 'User Name',
            'addressline1' => 'Address Line 1',
            'addressline2' => 'Address Line 2',
            'city' => 'City Name',
            'country' => 'Country',
            'gps' => 'GPS Coordinates',
        ]);

        if ($this->new) {
            $this->add(
                ['firstname', 'lastname', 'nationality', 'email', 'password'],
                new PresenceOf([
                    'message' => ':field Required',
                    'cancelOnFail' => true,
                ])
            );
        }

        $this->add(
            ['firstname', 'lastname', 'username'],
            new StringLength([
                'max' => 20,
                'min' => 2,
                'allowEmpty' => true,
                'messageMaximum' => ':field too long',
                'messageMinimum' => ':field too short',
            ])
        );

        $this->add(
            ['firstname', 'lastname', 'city'],
            new Regex([
                'pattern' =>
                    '/^[A-Za-z\x{00C0}-\x{00FF}][A-Za-z\x{00C0}-\x{00FF}\'\-]+([\ A-Za-z\x{00C0}-\x{00FF}][A-Za-z\x{00C0}-\x{00FF}\'\-]+)*/u',
                'message' => 'Invalid :field',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            'username',
            new Regex([
                'pattern' => '/^[a-zA-Z][a-zA-Z0-9]+$/',
                'message' => ':field must start with an alphabet, then follow with alphanumerics',
                'allowEmpty' => true,
            ])
        );
        $this->add(
            'username',
            new Callback([
                'callback' => function ($data) {
                    if (!Model::check(['username' => $data['username']], ['user_id' => $this->session->get('UID')])) {
                        return true;
                    }
                    return false;
                },
                'message' => 'Matching :field found in Database',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            'email',
            new Email([
                'message' => 'Invalid :field',
                'allowEmpty' => true,
            ])
        );
        $this->add(
            'email',
            new Callback([
                'callback' => function ($data) {
                    if (!Model::check(['email' => $data['email']], ['user_id' => $this->session->get('UID')])) {
                        return true;
                    }
                    return false;
                },
                'message' => 'Matching :field found in Database',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            'phone',
            new Regex([
                'pattern' => '/^\+(?:[0-9] ?){6,14}[0-9]$/',
                'message' => 'Invalid :field',
                'allowEmpty' => true,
            ])
        );
        $this->add(
            'phone',
            new Callback([
                'callback' => function ($data) {
                    if (!Model::check(['PHONE' => $data['phone']], ['user_id' => $this->session->get('UID')])) {
                        return true;
                    }
                    return false;
                },
                'message' => 'Matching :field found in Database',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            ['addressline1', 'addressline2'],
            new StringLength([
                'max' => 45,
                'allowEmpty' => true,
                'messageMaximum' => ':field too long',
                'messageMinimum' => ':field too short',
            ])
        );

        $this->add(
            ['nationality', 'country'],
            new Regex([
                'pattern' => '/^[a-zA-Z]{2}$/',
                'message' => 'Use ISO 3166-1 2-letter for the :field',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            'nationality',
            new Callback([
                'callback' => function ($data) {
                    switch ($data['nationality']) {
                        case 'ZA':
                            if (empty(trim($data['idnumber']))) {
                                return false;
                            }
                            return true;
                            break;
                        default:
                            return true;
                            break;
                    }
                },
                'message' => 'Id Number is Required',
                'allowEmpty' => true,
            ])
        );

        $this->add(
            'idnumber',
            new Callback([
                'callback' => function ($data) {
                    switch ($data['nationality']) {
                        case 'ZA':
                            if (empty(trim($data['idnumber']))) {
                                $this->appendMessage(new Message('Id Number Required'));
                                return false;
                            }
                            return new IdZA(['message' => 'Invalid South African ID Number']);
                            break;
                        default:
                            return true;
                            break;
                    }
                },
                'cancelOnFail' => true,
                'allowEmpty' => true,
            ])
        );

        $this->add('dob', new DateISO(['message' => 'Invalid Date of Birth', 'cancelOnFail' => true, 'allowEmpty' => true]));
        $this->add(
            'dob',
            new Callback([
                'callback' => function ($data) {
                    switch ($data['nationality']) {
                        case 'ZA':
                            if (empty(trim($data['idnumber']))) {
                                return true;
                            }
                            $date = \DateTime::createFromFormat('ymd', substr($data['idnumber'], 0, 6));
                            return $data['dob'] == $date->format('Y-m-d');
                            break;
                        default:
                            return true;
                            break;
                    }
                },
                'message' => 'Date of Birth incosistent with ID Number',
                'allowEmpty' => true,
            ])
        );

        $this->add('newPassword', new Password(['allowEmpty' => true]));
    }
}
?>
