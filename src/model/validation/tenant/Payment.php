<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Validation\Tenant
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Model\Validation\Tenant;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Callback;
use Phiscal\Model\Entity\Tenant\Payment as Model;
/**
 * Payment class
 *
 * @category  PHP
 * @package   Phiscal\Model\Validation\Tenant
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Payment extends Validation
{
    /**
     * @var bool $new
     */
    private $new;

    /**
     * @param string $target = ''
     */
    public function __construct(bool $new = null)
    {
        $this->new = $new ?? Model::check(['tenant_id' => $this->request->getTenant()], ['provider_id' => '--']);
        parent::__construct();
    }

    /**
     * Initialize components
     *
     * @return void
     */
    public function initialize()
    {
        $this->setLabels([
            'provider_id' => 'Payment Gateway Provider',
            'client_id' => 'Client ID',
            'access_key' => 'Access Key',
        ]);

        if ($this->new) {
            $this->add(
                ['provider_id', 'access_key'],
                new PresenceOf([
                    'message' => ':field Required',
                    'cancelOnFail' => true,
                ])
            );
        }

        $this->add(
            'provider_id',
            new Callback([
                'callback' => function ($data) {
                    switch ($data['provider_id']) {
                        case 'SP':
                            return true;
                    }
                    return !empty($data['client_id']);
                },
                'message' => 'Client ID Required',
                'allowEmpty' => true,
            ])
        );
    }
}
?>
