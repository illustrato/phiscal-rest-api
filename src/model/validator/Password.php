<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Validator
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Model\Validator;

use Phalcon\Messages\Message;
use Phalcon\Validation;
use Phalcon\Validation\AbstractValidator;

/**
 * Password class
 *
 * @category  PHP
 * @package   Phiscal\Model\Validator
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Password extends AbstractValidator
{
    /**
     * Executes the validation
     *
     * @param Validation $validator
     * @param string     $attribute
     *
     * @return bool
     */
    public function validate(Validation $validator, $attribute): bool
    {
        $value = $validator->getValue($attribute);

        if (strlen($value) < 8) {
            $validator->appendMessage(new Message('Password Length below minimum (8 charactors)', $attribute, self::class));
            return false;
        }

        $conditions = ['Uppercase' => false, 'Lowercase' => false, 'Numeric' => false, 'Symbol' => false];
        $chars = str_split($value);

        foreach ($chars as $char) {
            if (
                (ord($char) > 32 && ord($char) < 48) ||
                (ord($char) > 57 && ord($char) < 65) ||
                (ord($char) > 90 && ord($char) < 97) ||
                ord($char) > 122
            ) {
                $conditions['Symbol'] = true;
                break;
            }
        }

        foreach ($chars as $char) {
            if (ord($char) >= 65 && ord($char) <= 90) {
                $conditions['Uppercase'] = true;
                break;
            }
        }

        foreach ($chars as $char) {
            if (ord($char) >= 97 && ord($char) <= 122) {
                $conditions['Lowercase'] = true;
                break;
            }
        }

        foreach ($chars as $char) {
            if (ord($char) >= 48 && ord($char) <= 57) {
                $conditions['Numeric'] = true;
                break;
            }
        }

        foreach ($conditions as $condition => $found) {
            if (!$found) {
                $validator->appendMessage(new Message("$condition not found in Password", $attribute, self::class));
                return false;
            }
        }

        return true;
    }
}
?>
