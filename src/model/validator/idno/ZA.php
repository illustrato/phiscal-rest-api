<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Model\Validator;
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Model\Validator\IdNo;

use Phalcon\Messages\Message;
use Phalcon\Validation;
use Phalcon\Validation\AbstractValidator;

/**
 * ZAIdNo class
 *
 * @category  PHP
 * @package   Phiscal\Model\Validator;
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class ZA extends AbstractValidator
{
    /**
     * Executes the validation
     *
     * @param Validation $validator
     * @param string     $attribute
     *
     * @return bool
     */
    public function validate(Validation $validator, $attribute): bool
    {
        $value = $validator->getValue($attribute);
        $message = $this->getOption('message');
        if (!$message) {
            $message = 'Id Number isn\'t valid';
        }
        $valid = false;

        if (is_numeric($value) && strlen($value) === 13) {
            $errors = false;

            $num_array = str_split($value);

            // Validate the day and month
            $month = $num_array[2] . $num_array[3];
            $day = $num_array[4] . $num_array[5];

            if ($month < 1 || $month > 12) {
                $errors = true;
            }
            if ($day < 1 || $day > 31) {
                $errors = true;
            }

            // Validate citizenship

            // citizenship as per id number
            $nationality = $num_array[10];

            // citizenship as per submission
            if ($nationality < 0 || $nationality > 1) {
                $errors = true;
            }

            /**********************************
                 Check Digit Verification
             **********************************/

            // Declare the arrays
            $even_digits = [];
            $odd_digits = [];

            // Loop through modified $num_array, storing the keys and their values in the above arrays
            foreach ($num_array as $index => $digit) {
                if ($index === 0 || $index % 2 === 0) {
                    $odd_digits[] = $digit;
                } else {
                    $even_digits[] = $digit;
                }
            }
            // use array pop to remove the last digit from $odd_digits and store it in $check_digit
            $check_digit = array_pop($odd_digits);
            //All digits in odd positions (excluding the check digit) must be added together.
            $added_odds = array_sum($odd_digits);
            //All digits in even positions must be concatenated to form a 6 digit number.
            $concatenated_evens = implode('', $even_digits);

            //This 6 digit number must then be multiplied by 2.
            $evensx2 = $concatenated_evens * 2;
            // Add all the numbers produced from the even numbers x 2
            $added_evens = array_sum(str_split("$evensx2"));

            $sum = $added_odds + $added_evens;
            // get the last digit of the $sum
            $last_digit = substr("$sum", -1);

            /* 10 - $last_digit
             * $verify_check_digit = 10 - (int)$last_digit; (Will break if $last_digit = 0)
             * Edit suggested by Ruan Luies
             * verify check digit is the resulting remainder of
             * 10 minus the last digit divided by 10
             */
            $verify_check_digit = (10 - (int) $last_digit) % 10;

            // test expected last digit against the last digit in $id_number submitted
            if ((int) $verify_check_digit !== (int) $check_digit) {
                $errors = true;
            }

            // if errors haven't been set to true by any one of the checks, we can change verified to true;
            if (!$errors) {
                $valid = true;
            }
        }
        if (!$valid) {
            $validator->appendMessage(new Message($message, $attribute, self::class));
        }

        return $valid;
    }
}
?>
