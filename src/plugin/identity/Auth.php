<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Plugins\Identity
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Plugin\Identity;

use Phalcon\Di\Injectable;
use Phalcon\Security\JWT\Signer\Hmac;
use Phalcon\Security\JWT\Validator;
use Phalcon\Security\JWT\Exceptions\ValidatorException;
use Phiscal\Model\Entity\User\Profile;
use Phiscal\Model\Entity\User\Token;
use Phiscal\Model\Entity\User\User;
use Phiscal\Traiting\Token as TokenTrait;

class Auth extends Injectable
{
    use TokenTrait;
    /**
     * Checks the user credentials
     *
     * @param object $credentials
     *
     * @return void
     * @throws Exception
     */
    public function check(object $credentials): void
    {
        // Check if the user exist
        $user = User::getData(['email' => $credentials->email], 'user_id, username, password');
        if (!$user) {
            $this->registerUserThrottling(0);
            throw new \Exception(json_encode(['Wrong email/password Combination']), $this->response::UNAUTHORIZED);
        }

        // Check the password
        if (!empty($user->password) && !$this->security->checkHash($credentials->password ?? '', $user->password)) {
            $this->registerUserThrottling($user->user_id);
            throw new \Exception(json_encode(['Wrong email/password Combination']), $this->response::UNAUTHORIZED);
        }

        if (!$user->signin()) {
            $error = $this->session->get('XET');
            throw new \Exception(json_encode($error->messages), $error->code);
        }

        // Check if the remember me was selected
        if ($credentials->remember) {
            $this->createRememberEnvironment($user);
        }

        // generate JWT
        $jwt = $user->getToken();
        $model = new Token([
            'token_id' => $jwt->getClaims()->get('jti'),
            'token' => $jwt->getToken(),
            'user_id' => $user->user_id,
            'type' => 'JWT',
            'userAgent' => $this->request->getUserAgent(),
        ]);
        // save JWT to database
        if (!$model->add()) {
            $error = $this->session->get('XET');
            throw new \Exception(json_encode($error->messages), $error->code);
        }
        // cache result to flash message session
        $this->session->set('JWT', $jwt->getToken());
    }

    /**
     * Implements login throttling
     * Reduces the effectiveness of brute force attacks
     *
     * @param int $userId
     *
     * @throws Exception
     */
    public function registerUserThrottling($id)
    {
        $param = ['id' => $id, 'ip' => $this->request->getClientAddress(), 'agent' => $this->request->getUserAgent()];
        if (($attemps = User::loginFail($param)) === false) {
            $error = $this->session->get('XET');
            throw new \Exception(json_encode($error->messages), $error->code);
        }
        switch ($attemps) {
            case 1:
            case 2:
                // no delay
                break;
            case 3:
            case 4:
                sleep(2);
            default:
                sleep(4);
                break;
        }
    }

    /**
     * Creates the remember me environment settings the related cookies and
     * generating tokens
     *
     * @param User $user
     */
    public function createRememberEnvironment(User $user)
    {
        $token = $user->rememberToken();
        if ($token != false) {
            $expire = time() + 86400 * 8;
            $this->cookies->set('RMU', $user->id, $expire, '/', null, "www.{$this->config->app->publicUrl}", null, [
                'samesite' => 'Strict',
            ]);
            $this->cookies->set('RMT', $token, $expire, '/', null, "www.{$this->config->app->publicUrl}", null, ['samesite' => 'Strict']);
        }
    }

    /**
     * Check if the session has a remember me cookie
     *
     * @return boolean
     */
    public function hasRememberMe(): bool
    {
        return $this->cookies->has('RMU');
    }

    /**
     * Logs on using the information in the cookies
     *
     * @return void
     * @throws Exception
     */
    public function loginWithRememberMe(): void
    {
        // information from cookies
        $userId = $this->cookies->get('RMU')->getValue();
        $cookieToken = $this->cookies->get('RMT')->getValue();

        if (empty($userId) || empty($cookieToken)) {
            throw new \Exception(json_encode('Data not found in Coockie'), 404);
        }

        // get record from database
        $user = User::byId($userId);

        if (!$user) {
            $this->cookies->get('RMU')->delete();
            $this->cookies->get('RMT')->delete();
            throw new \Exception(json_encode("User with ID '$userId' not found"), 404);
        }

        if (!$user->signin($cookieToken)) {
            $this->cookies->get('RMU')->delete();
            $this->cookies->get('RMT')->delete();
            $error = $this->session->get('XET');
            throw new \Exception(json_encode($error->messages), $error->code);
        }

        // JWT
        $jwt = $user->getToken();
        $model = new Token([
            'token_id' => $jwt->getClaims()->get('jti'),
            'token' => $jwt->getToken(),
            'userId' => $userId,
            'type' => 'JWT',
            'userAgent' => $this->request->getUserAgent(),
        ]);

        // save JWT to database
        if (!$model->add()) {
            $this->cookies->get('RMU')->delete();
            $this->cookies->get('RMT')->delete();
            $error = $this->session->get('XET');
            throw new \Exception(json_encode($error->messages), $error->code);
        }
        // cache result to flash message session
        $this->session->set('JWT', $jwt->getToken());
    }

    /**
     * Checks if the user is banned/inactive/suspended/deactivated
     *
     * @param Users $user
     *
     * @throws Exception
     */
    public function checkUserFlags(User $user)
    {
        if (intval($user->active) < 1) {
            throw new \Exception(json_encode([$user->flag()]), 403);
        }
    }

    /**
     * Removes the user identity information from session
     *
     * @return void
     */
    public function remove(): void
    {
        if ($this->cookies->has('RMU')) {
            $this->cookies->get('RMU')->delete();
        }
        if ($this->cookies->has('RMT')) {
            $token = $this->cookies->get('RMT')->getValue();
            $token = Token::fetch($token);
            if ($token) {
                $this->deleteToken($token->USER_ID);
            }
            $this->cookies->get('RMT')->delete();
        }
    }

    /**
     * Auths the user by their id
     *
     * @param string $uId
     *
     * @return array
     * @throws Exception
     */
    public function authUserById(string $uId): array
    {
        $user = User::getData(['user_id' => $uId]);
        if (!$user) {
            throw new \Exception(json_encode(['The user does not exist']), 403);
        }

        $this->checkUserFlags($user);

        // generate JWT
        $jwt = $user->getToken();
        $model = new Token([
            'token_id' => $jwt->getClaims()->get('jti'),
            'token' => $jwt->getToken(),
            'userId' => $id,
            'type' => 'JWT',
            'userAgent' => $this->request->getUserAgent(),
        ]);
        // save JWT to database
        if (!$model->add()) {
            $error = $this->session->get('XET');
            throw new \Exception(json_encode($error->messages), $error->code);
        }
        // cache result to flash message session
        return $jwt->getToken();
    }

    /**
     * Get the entity related to user in the active JWT
     *
     * @return User
     * @throws Exception
     */
    public function getSession(string $uId): array
    {
        // Verify Token
        if (($session = $this->userByToken($this->request->getBearerToken())) === false) {
            $error = $this->session->get('XET');
            throw new \Exception(json_encode($error->messages), $error->code);
        }
        $this->checkUserFlags($session['user']);
        return $session;
    }

    /**
     * Delete the current user token in session
     *
     * @return PDO
     */
    public function deleteToken()
    {
        $model = (new Token())->setId(['user_id' => $this->session->get('UID'), 'tokentype_id' => 'JWT']);
        return $model->delete();
    }
    /**
     * Returns the current token user
     *
     * @param string $token
     *
     * @return false | User
     */
    public function uidByToken(string $token)
    {
        $uId = Profile::verifyToken($token);
        if ($uId === false) {
            $this->session->set('XET', (object) ['messages' => ['JWT not found in database'], 'code' => 404]);
            return false;
        }
        try {
            // Parse the token received
            $tokenObject = $this->getToken($token);
            // Create the validator
            $validator = new Validator($tokenObject, 100); // allow for a time shift of 100

            $expires = $issued = time();
            $claims = $tokenObject->getClaims()->getPayload();
            $signer = new Hmac($this->getTokenAlgorithm());

            foreach ($claims['aud'] as $audience) {
                $validator->validateAudience($audience);
            }
            $validator
                ->validateExpiration($expires)
                ->validateId("{$claims['sub']}{$claims['iat']}")
                ->validateIssuedAt($issued)
                ->validateIssuer($claims['iss'])
                ->validateNotBefore($this->getTokenTimeNotBefore())
                ->validateSignature($signer, 'QcMpZ&' . sha1("{$claims['sub']}{$claims['iat']}"));
            return $uId;
        } catch (\Exception $e) {
            $this->session->set('XET', (object) ['messages' => [$e->getMessage()], 'code' => $e->getCode()]);
            return false;
        }
        return $check;
    }

    /**
     * Get Profiles associated with User ID
     *
     * @param string $uid
     *
     * @return array
     */
    public function getProfiles(string $uid): array
    {
        return Profile::getList($uid);
    }

    /**
     * User Profile
     *
     * @param string $uId
     * @param int $tenantId
     *
     * @return false|object
     */
    public function getProfile(string $uId, int $tenantId)
    {
        $model = new Profile(['user_id' => $uId]);
        return $model->onTenant($tenantId);
    }
}
?>
