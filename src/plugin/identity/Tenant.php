<?php
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal/Plugin/Identity
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Plugin\Identity;

use Phalcon\Di\Injectable;
use Phiscal\Model\Entity\Tenant as Model;

/**
 * Tenant class
 *
 * @category  PHP
 * @package   Phiscal\Plugin\Identity
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Tenant extends Injectable
{
    /**
     * Get the entity related to orgization in the active Tenant
     *
     * @return false|Model\Tenant
     */
    public function getCurrent()
    {
        $fields = 'tenant';
        return Model::getData(
            ['TENANT_ID' => $this->request->getTenant(), "JSON_EXTRACT(tenant, '$.admin')" => $this->session->get('UID')],
            $fields,
            'v_tenant_compact'
        );
    }
}
?>
