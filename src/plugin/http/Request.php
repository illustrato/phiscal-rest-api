<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Plugins\Http
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Plugin\Http;
use Phalcon\Http\Request as PhRequest;
/**
 * Request class
 *
 * @category  PHP
 * @package   Phiscal\Plugins\Http
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Request extends PhRequest
{
    private const TENANT = 'Tenant';
    /**
     * Bearer Token from Header
     *
     * @return string
     */
    public function getBearerToken(): string
    {
        $headers = apache_request_headers();
        $key = 'Authorization';
        return array_key_exists($key, $headers) ? str_replace('Bearer ', '', $headers[$key]) : '';
    }

    /**
     * Tenant Header
     *
     * @return int
     */
    public function getTenant(): int
    {
        return intval($this->getHeader(self::TENANT));
    }

    /**
     * Check if requested from Tenant
     *
     * @return bool
     */
    public function inTenant(): bool
    {
        return $this->hasHeader(self::TENANT) && $this->getTenant() > 0;
    }

    /**
     * Check Headers for the Bearer Token
     *
     * @return bool
     */
    public function isEmptyBearerToken(): bool
    {
        return true === empty($this->getBearerToken());
    }
}
?>
