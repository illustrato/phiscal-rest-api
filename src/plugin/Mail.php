<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Plugin
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Plugin;

use Phalcon\Di\Injectable;
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use function Phiscal\Core\envValue;

class Mail extends Injectable
{
    /**
     * Sends e-mails based on predefined templates
     *
     * @param array  $to
     * @param string $subject
     * @param string $message
     *
     * @return bool
     */
    public function send(array $to, string $subject, string $message, &$error): bool
    {
        $config = $this->config->mail;
        //Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);

        try {
            /* $mail->SMTPDebug = SMTP::DEBUG_SERVER; //Enable verbose debug output */
            $mail->isSMTP(); //Send using SMTP
            $mail->Host = envValue('STRATO_SMTP_HOST', $config->smtp['host']); //Set the SMTP server to send through
            $mail->SMTPAuth = true; //Enable SMTP authentication
            $mail->CharSet = 'UTF-8';
            $mail->Username = envValue('STRATO_SMTP_USER', $config->smtp['address']); //SMTP username
            $mail->Password = envValue('STRATO_SMTP_PASSWD', $config->smtp['passwd']); //SMTP password
            $mail->addReplyTo('flames@illustrato.org', 'Information');
            $mail->SMTPSecure = $this->secure(envValue('STRATO_SMTP_SECURE', $config->smtp['secure'])); //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port = envValue('STRATO_SMTP_PORT', $config->smtp['port']); //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
            $mail->setFrom(envValue('STRATO_SMTP_USER', $config->smtp['address']), 'Phiscal');
            $mail->isHTML(true);
            $mail->addAddress($to['email'], "{$to['recepient']}");
            $mail->Subject = $subject;
            $mail->Body = $message;
            $mail->send();
        } catch (Exception $e) {
            $error = "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            return false;
        }
        return true;
    }

    /**
     * Securirity function
     *
     * @param string $param
     *
     * @return string
     */
    private function secure(string $param): string
    {
        return $param === 'STARTTLS ' ? PHPMailer::ENCRYPTION_STARTTLS : $param;
    }

    /**
     * Read Verification Email
     *
     * @param string $uid	: User Id
     * @param string $token	: Token
     *
     * @return string
     */
    public function verifyemail(string $uid, string $token): string
    {
        $activate = "https://www.{$this->config->app->publicUrl}/#/redirect/a/$uid/$token";
        $revoke = "https://www.{$this->config->app->publicUrl}/#/redirect/r/$uid/$token";
        $message = file_get_contents('usr/email/verification.php');
        $message = str_replace('btnlink1', $activate, $message);
        $message = str_replace('btnlink2', $revoke, $message);
        return $message;
    }

    /**
     * Read Password Reset Email
     *
     * @param string $uid	: User Id
     * @param string $token	: Token
     *
     * @return string
     */
    public function resetPassword(string $uid, string $token): string
    {
        $reset = "https://www.{$this->config->app->publicUrl}/#/redirect/p/$uid/$token";
        $message = file_get_contents('usr/email/passwdreset.php');
        $message = str_replace('btnlink1', $reset, $message);
        return $message;
    }

    /**
     * Read Email Reset Email
     *
     * @param string $uid	: User Id
     * @param string $token	: Token
     *
     * @return string
     */
    public function resetEmail(string $uid, string $token): string
    {
        $reset = "https://www.{$this->config->app->publicUrl}/#/redirect/e/$uid/$token";
        $message = file_get_contents('usr/email/emailreset.php');
        $message = str_replace('btnlink1', $reset, $message);
        return $message;
    }

    /**
     * Verify Email Address of Staff Member
     *
     * @param string $admin	: The full name and email address of admin who added employee
     * @param string $org	: The name of business that employs staff member
     * @param string $uid	: User Id
     * @param string $token	: Token
     *
     * @return void
     */
    public function verifyStaff(string $uid, object $cert): string
    {
        $activate = "https://www.{$this->config->app->publicUrl}/#/redirect/a/$uid/{$cert->token}";
        $reject = "https://www.{$this->config->app->publicUrl}/#/redirect/r/$uid/{$cert->token}";
        $account = "https://www.{$this->config->app->publicUrl}/#/redirect/t/$uid/{$cert->token}";
        $message = file_get_contents('usr/email/verifystaff.php');
        $message = str_replace('btnlink1', $activate, $message);
        $message = str_replace('btnlink2', $reject, $message);
        $message = str_replace('btnlink3', $account, $message);
        $message = str_replace('target_admin', "{$cert->admin_name} ({$cert->admin_mail})", $message);
        $message = str_replace('admin_mail', $cert->admin_mail, $message);
        $message = str_replace('target_business', $cert->business, $message);
        return $message;
    }
}
?>
