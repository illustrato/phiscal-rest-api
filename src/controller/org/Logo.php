<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Controller
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2022 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Controller\Org;

use Phiscal\Controller\Base;
use Phiscal\Model\Entity\Index;
use Phiscal\Model\Entity\Org\Logo as Model;

/**
 * Logo class
 *
 * @category  PHP
 * @package   Phiscal\Controller
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2022 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Logo extends Base
{
    /**
     * CRUD operations
     * PRIVATE
     * DB Requests :4
     *
     * @param int $logoId
     *
     * @method PUT
     */
    public function crud(int $logoId)
    {
        $this->checkRequestMethod('Put');
        $param = $this->request->getJsonRawBody();
        $model = $logoId === 0 ? new Model([]) : Model::getRecord($this->request->getTenant(), $logoId);
        $this->checkRecord($model);

        /**
         * rm action
         */

        if (empty($param->base64)) {
            $file = "{$this->config->app->files}img/logo/{$model->ORG_ID}/{$logoId}.{$param->ext}";
            // rm action
            $model = $model->setId(['ORG_ID' => $model->ORG_ID, 'LOGO_NO' => $model->LOGO_NO]);
            $model->delete();
            $this->checkDbRequest();
            if (!unlink($file)) {
                Index::notify($this->session->get('UID'), 'log', 'fa fa-trash text-danger', null, "Couldn't delete file '$file'");
            }
            $this->intercept(Model::ls($this->request->getTenant()), $this->response::OK);
        }

        /**
         * mk action
         */

        // create database record
        $result = $model->insert([
            'ORG_ID' => $this->request->getTenant(),
            'EXT' => $param->ext,
            'DESCRIPTION' => $param->description,
            'WIDTH' => $param->width,
            'HEIGHT' => $param->height,
        ]);
        $this->checkDbRequest();
        $id = $result->query('SELECT @LOGO_NO logo, @ORG_ID org')->fetch(\PDO::FETCH_OBJ);
        $model->setId(['ORG_ID' => $id->org, 'LOGO_NO' => $id->logo]);

        // attempt creating file
        $bytes = $this->touch("img/logo/{$id->org}", $id->logo, (array) $param, $model);
        // update database record
        $model->update(['SIZE' => $bytes]);

        $this->response->setPayloadSuccess(Model::ls($this->request->getTenant()))->send();
    }
}
