<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Controller\Org
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2022 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Controller\Org;

use Phiscal\Controller\Base;
use Phiscal\Model\Entity\Index;
use Phiscal\Model\Entity\Org\Branch as Model;
use Phiscal\Model\Validation\Org\Branch as BranchValidator;

/**
 * Branch class
 *
 * @category  PHP
 * @package   Phiscal\Controller\Org
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2022 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Branch extends Base
{
    /**
     * Create or Update branch record
     * PRIVATE
     * DB Requests :3
     *
     * @param int $logoId
     *
     * @method PUT
     */
    public function crud(int $branchId): void
    {
        // verify request method
        $this->checkRequestMethod('Put');
        $payload = (array) $this->request->getJsonRawBody();
        $newRecord = $branchId === 0;

        // Create Model object to use in creating or updating Branch Record
        $model = $newRecord ? new Model([]) : Model::getRecord($this->request->getTenant(), $branchId);
        $this->checkRecord($model);

        // Create index to identify record, if it exist
        $index = ['ORG_ID' => $newRecord ? 0 : intval($model->ORG_ID), 'BRANCH_NO' => $branchId];

        // Validate User Input
        $this->validateInput($payload, (new BranchValidator($newRecord))->setId($index));

        // Check to see if payload contains the branch photograph
        $photo = ['up' => false];
        if (!empty($payload['picture']->base64)) {
            $photo = ['up' => true, 'ext' => $payload['picture']->ext, 'base64' => $payload['picture']->base64];
            $payload['photo'] = $payload['picture']->ext;
        }
        unset($payload['picture']);

        // Create or Update record, depending on condition
        $result = $newRecord
            ? $model->insert($payload + ['ORG_ID' => $this->request->getTenant()])
            : $model->setId($index)->update($payload);
        $this->checkDbRequest();

        // Get New Branch ID
        if ($newRecord) {
            $record = $result->query('SELECT @BRANCH_NO branch, @ORG_ID org')->fetch(\PDO::FETCH_OBJ);
            $branchId = intval($record->branch);
            $index = ['ORG_ID' => intval($record->org), 'BRANCH_NO' => $branchId];
        }

        // Create branch photo image file
        if ($photo['up']) {
            $this->touch("img/branch/{$index['ORG_ID']}", "$branchId", $photo);
        }
        // Make HTTP Response
        $this->response->setPayloadSuccess(Model::ls($this->request->getTenant()))->send();
    }

    /**
     * Update/Delete Branch Photograph
     * PRIVATE
     * DB Requests :3
     *
     * @param int $branchId
     *
     * @method PUT
     */
    public function updatePhoto(int $branchId): void
    {
        // verify request method
        $this->checkRequestMethod('Put');
        $model = Model::getRecord($this->request->getTenant(), $branchId);
        $this->checkRecord($model);
        $payload = $this->request->getJsonRawBody();

        if (empty($payload->base64)) {
            $file = "{$this->config->app->files}img/branch/{$model->ORG_ID}/{$branchId}.{$payload->ext}";
            // rm action
            $model = $model->setId(['ORG_ID' => $model->ORG_ID, 'BRANCH_NO' => $model->BRANCH_NO]);
            $model->update(['photo' => '?']);
            $this->checkDbRequest();
            if (!unlink($file)) {
                Index::notify($this->session->get('UID'), 'log', 'fa fa-trash text-danger', "Couldn't delete file '$file'");
            }
            $this->intercept(Model::ls($this->request->getTenant()), $this->response::OK);
        }

        // mk action
        $model = $model->setId(['ORG_ID' => $model->ORG_ID, 'BRANCH_NO' => $model->BRANCH_NO]);
        $model->update(['photo' => $payload->ext]);
        $this->checkDbRequest();
        $this->touch("img/branch/{$model->ORG_ID}", "$branchId", (array) $payload);

        // Make HTTP Response
        $this->response->setPayloadSuccess(Model::ls($this->request->getTenant()))->send();
    }

    /**
     * Delete Branch Record
     * PRIVATE
     * DB Requests :3
     *
     * @param int $branchId
     *
     * @method DELETE
     */
    public function delete(int $branchId): void
    {
        // verify request method
        $this->checkRequestMethod('Delete');
        $model = Model::getRecord($this->request->getTenant(), $branchId);
        $this->checkRecord($model);
        if (!$model->delete()) {
            $this->intercept("Couldn't delete Branch '$branchId'", $this->response::INTERNAL_SERVER_ERROR);
        }
        $file = "{$this->config->app->files}img/branch/{$model->ORG_ID}/{$model->BRANCH_NO}.{$model->PHOTO}";
        if (file_exists($file)) {
            unlink($file);
        }
        // Make HTTP Response
        $this->response->setPayloadSuccess(Model::ls($this->request->getTenant()))->send();
    }
}
