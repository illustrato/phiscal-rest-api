<?php
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Controller\Org
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Controller\Org;

use Phiscal\Controller\Base;
use Phiscal\Model\Entity\Index as Index;
use Phiscal\Model\Entity\Org\Organization as Model;
use Phiscal\Model\Validation\Org\Organization as OrgValidator;
use Phiscal\Model\Validation\Org\Branch as BranchValidator;
use function Phiscal\Core\rmR;
/**
 * Tenant class
 *
 * @category  PHP
 * @package   Phiscal\Controller
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Organization extends Base
{
    /**
     * Generic List
     * PUBLIC
     * DB Requests :1
     *
     * @param string $target
     *
     * @method GET
     */
    public function getList(string $target): void
    {
        // verify request method
        $this->checkRequestMethod('Get');
        // make HTTP response
        $this->response->setPayloadSuccess(Model::ls($target, $this->request->getTenant()))->send();
    }

    /**
     * Organization CRUD Operations
     * PRIVATE
     * DB Requests :2
     *
     * @method POST
     */
    public function crud(): void
    {
        // verify request method
        $this->checkRequestMethod('Post');

        // validate user input
        $payload = (array) $this->request->getJsonRawBody();
        $this->validateInput($payload, new OrgValidator());

        // Check for Business Logo
        $logo = ['up' => false];
        if (!empty($payload['picture']->base64)) {
            $logo = array_merge(['up' => true], (array) $payload['picture']);
            $payload['logo'] = $payload['picture']->ext;
        }
        unset($payload['picture']);

        // create Tenant record in database
        $model = new Model($payload);
        $organization = $model->crud();
        $this->checkDbRequest();
        $this->checkRecord($organization, 'Registration Failed', $this->response::INTERNAL_SERVER_ERROR);
        $organization = json_decode($organization);

        // create organization logo in physical file system
        if ($logo['up']) {
            $bytes = $this->touch("img/logo/{$organization->id}", '1', $logo);
            $img = (new Model([], 'logo'))->setId(['ORG_ID' => $organization->id, 'LOGO_NO' => 1]);
            $img->update([
                'SIZE' => $bytes,
                'EXT' => $logo['ext'],
                'WIDTH' => $logo['width'],
                'HEIGHT' => $logo['height'],
                'DESCRIPTION' => $logo['description'] ?? null,
            ]);
        }

        // make HTTP response
        $this->response->setPayloadSuccess($organization)->send();
    }

    /**
     * Switch Main Logo/Branch
     * PRIVATE
     * DB Requests :1
     *
     * @param string $target	: Request targets Logo or Branch
     * @param int $id		: Primary Key of Target
     *
     * @method PUT
     */
    public function mainSwitch(string $target, int $id): void
    {
        // verify request method
        $this->checkRequestMethod('Put');
        $model = new Model((array) $this->request->getJsonRawBody(), $target);
        // make HTTP response
        $this->response->setPayloadSuccess($model->mainswitch($id))->send();
    }

    /**
     * Get Organizational Data
     * PUBLIC
     * DB Requests :1
     *
     * @method GET
     */
    public function getPublicInfo(): void
    {
        // verify request method
        $this->checkRequestMethod('Get');
        // make HTTP response
        $this->response->setPayloadSuccess(Model::publicInfo($this->request->getTenant()))->send();
    }

    /**
     * Get Organizational Data
     * PRIVATE
     * DB Requests :1
     *
     * @method GET
     */
    public function getAdminInfo(): void
    {
        // verify request method
        $this->checkRequestMethod('Get');
        // make HTTP response
        $this->response->setPayloadSuccess(Model::adminInfo($this->request->getTenant(), $this->session->get('UID')))->send();
    }

    /**
     * Delete Record
     * PRIVATE
     * DB Requests :1
     *
     * @param string $confirmation	: String to confirmation User Intent
     *
     * @method DELETE
     */
    public function delete(string $confirmation)
    {
        // verify request method
        $this->checkRequestMethod('Delete');
        // perform database request
        $model = new Model(['confirmation' => $confirmation]);
        $orgId = $model->rm();
        $this->checkDbRequest();
        // delete associate files in physical file system
        rmR("{$this->config->app->files}img/branch/{$orgId}/");
        rmR("{$this->config->app->files}img/logo/{$orgId}/");

        // make HTTP response
        $this->response->setPayloadSuccess(intval($orgId))->send();
    }
}
?>
