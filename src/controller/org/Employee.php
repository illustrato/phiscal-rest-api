<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Controller\Org``
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Controller\Org;

use Phiscal\Controller\Base;
use Phiscal\Model\Entity\Index;
use Phiscal\Model\Entity\Org\Employee as Model;
use Phiscal\Model\Entity\User\User;
use Phiscal\Model\Entity\User\Token;
use Phiscal\Model\Validation\Org\Employee as EmpValidator;

/**
 * Employee class
 *
 * @category  PHP
 * @package   Phiscal\Controller\Org
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Employee extends Base
{
    /**
     * Create or Update Employee Record
     * PRIVATE
     * DB Requests :2
     *
     * @param string 	$uId
     *
     * @method PUT
     */
    public function crud(string $uId): void
    {
        // verify request method
        $this->checkRequestMethod('Put');

        // validate user input
        $payload = (array) $this->request->getJsonRawBody();
        $this->validateInput($payload, new EmpValidator(['USER_ID' => $uId]));

        // check for user photograph
        $photo = ['up' => false];
        if (!empty($payload['picture']->base64)) {
            $photo = ['up' => true, 'ext' => $payload['picture']->ext, 'base64' => $payload['picture']->base64];
            $payload['photo'] = $payload['picture']->ext;
        }
        unset($payload['picture']);

        // create user record in database
        $model = new Model($payload);
        $employee = $model->crud($uId);
        $this->checkDbRequest();
        $this->checkRecord($employee, 'Process Failed', $this->response::INTERNAL_SERVER_ERROR);

        // create avatar image in physical file system
        if ($photo['up']) {
            $this->touch('img/avatar', $employee->user_id, $photo);
        }

        // request list of employees from database
        $list = Model::ls($this->request->getTenant());

        if ($uId === 'new') {
            // send emails only is config value is set to true
            $this->checkEmailConfig($list);

            $email = [
                'to' => ['email' => $employee->email, 'recepient' => "{$employee->firstname} {$employee->lastname}"],
                'subject' => 'Verify Account',
                'message' => $this->mail->verifyStaff($employee->user_id, $employee->cert),
            ];
            // verify user email address
            $this->sendMail($email, $employee);
        }

        // make HTTP response
        $this->response
            ->setPayloadSuccess($list)
            ->setStatusCode($this->response::CREATED)
            ->send();
    }

    /**
     * Delete Employee
     * PRIVATE
     * DB Requests :2
     *
     * @param string $id
     *
     * @method DELETE
     */
    public function rm(string $id): void
    {
        // verify request method
        $this->checkRequestMethod('Delete');
        // get user record from database
        $employee = Model::getData(['USER_ID' => $id]);
        $employee->delete();
        // make HTTP response
        $this->response->setPayloadSuccess(Model::ls($this->request->getTenant()))->send();
    }

    /**
     * Update Employee Photo
     * PRIVATE
     * DB Requests :2
     *
     * @param string $uid	: Id of target user
     *
     * @method PUT
     */
    public function crudPhoto(string $uId): void
    {
        // verify request method
        $this->checkRequestMethod('Put');
        // get request data
        $payload = (array) $this->request->getJsonRawBody();
        $file = "{$this->config->app->files}img/avatar/{$uId}.{$payload['picture']->ext}";

        // check for the picture base64 blob in the request payload
        $photo = ['up' => false];
        if (!empty($payload['picture']->base64)) {
            $photo = array_merge(['up' => true], (array) $payload['picture']);
            $payload['photo'] = $payload['picture']->ext;
        }
        unset($payload['picture']);

        // update employee record in database
        $model = (new Model($payload))->setId(['user_id' => $uId]);
        $staff = $model->photo();
        $this->checkDbRequest();

        // delete file in physical file system
        if ($photo['up'] === false) {
            if (!unlink($file)) {
                $model->update(['avatar' => $oldFile]);
                $this->intercept("Couldn't delete file '$file'", $this->response::FORBIDDEN);
            }
            $this->intercept($staff, $this->response::OK);
        }

        // upolad file in physical file system
        if (file_exists($file) && !unlink($file)) {
            $this->intercept("Couldn't delete old photograph File '$file'", $this->response::FORBIDDEN);
        }
        if ($this->touch('img/avatar', $uId, $photo) === false) {
            $model->update(['avatar' => $oldFile]);
        }
        // make HTTP response
        $this->response->setPayloadSuccess($staff)->send();
    }
}
?>
