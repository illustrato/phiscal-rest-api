<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Controller\Tenant
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Controller\Tenant;

use Phiscal\Controller\Base;
use Phiscal\Model\Entity\Tenant\Payment as Model;
use Phiscal\Model\Validation\Tenant\Payment as PaymentValidator;
/**
 * Payment class
 *
 * @category  PHP
 * @package   Phiscal\Controller\Tenant
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Payment extends Base
{
    /**
     * Generic List
     * PUBLIC
     * DB Requests :1
     *
     * @param string $proxy
     *
     * @method GET
     */
    public function getList(string $proxy): void
    {
        // verify request method
        $this->checkRequestMethod('Get');
        // make HTTP response
        $this->response->setPayloadSuccess(Model::ls($proxy))->send();
    }

    /**
     * Add Payment Gateway
     * PRIVATE
     * DB Requests :1
     *
     * @method	POST
     */
    public function addGateway(): void
    {
        // verify request method
        $this->checkRequestMethod('Post');

        // validate user input
        $payload = (array) $this->request->getJsonRawBody();
        $this->validateInput($payload, new PaymentValidator());

        // make HTTP response
        $model = new Model($payload);
        $this->response->setPayloadSuccess($model->paygate($this->request->getTenant()))->send();
    }

    /**
     * Get Payment Details
     * PRIVATE
     * DB Requests :1
     *
     * @param string $target
     *
     * @method GET
     */
    public function getDetails(string $target): void
    {
        // verify request method
        $this->checkRequestMethod('Get');
        // make HTTP response
        $model = new Model();
        $this->response->setPayloadSuccess($model->getDetails($target === 'self' ? $this->request->getTenant() : $target))->send();
    }
}
