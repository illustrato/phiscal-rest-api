<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Controller\Tenant
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Controller\Tenant;

use Phiscal\Controller\Base;
use Phiscal\Model\Entity\Tenant\Module as Model;

/**
 * ModuleController class
 *
 * @category  PHP
 * @package   Phiscal\Controller\Tenant
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @link      127.0.0.1
 */
class Module extends Base
{
    /**
     * List All Modules
     * PUBLIC
     * DB Requests :1
     *
     * @method GET
     */
    public function index(): void
    {
        // verify request method
        $this->checkRequestMethod('Get');
        // make HTTP response
        $this->response->setPayloadSuccess((new Model())->ls())->send();
    }

    /**
     * Get Descriptive information about a certain module
     * PRIVATE
     * DB Requests :0
     *
     * @param string $name
     *
     * @method GET
     */
    public function elaborate(string $name): void
    {
        // verify request method
        $this->checkRequestMethod('Get');
        if (file_exists($file = 'usr/notes/module/' . urldecode($name) . '.md') && ($handle = fopen($file, 'r'))) {
            $information = filesize($file) > 0 ? fread($handle, filesize($file)) : '';
            fclose($handle);
            $this->response->setPayloadSuccess($information)->send();
            return;
        }
        // make HTTP response
        $this->response
            ->reportErrors(["File '$file' Not Found"], 404)
            ->setStatusCode($this->response::NOT_FOUND)
            ->send();
    }

    /**
     * Get Module Data
     * PUBLIC
     * DB Requests :1
     *
     * @method GET
     */
    public function getData(string $id)
    {
        // verify request method
        $this->checkRequestMethod('Get');
        // make HTTP response
        $fields = 'name, ' . 'description, ' . 'logo, ' . 'micro, ' . 'webclient, ' . 'active';
        $this->response->setPayloadSuccess(Model::getData(['mod_id' => $id], $fields))->send();
    }

    /**
     * Subscribe Curent Entity to Module
     * PRIVATE
     * DB Requests :1
     *
     * @method POST
     */
    public function subscribe(): void
    {
        // verify request method
        $this->checkRequestMethod('Post');
        // get request data
        $model = new Model((array) $this->request->getJsonRawBody());
        // make HTTP response
        $this->response->setPayloadSuccess($model->subscribe())->send();
    }

    /**
     * Get Subscription List of Entity
     * PRIVATE
     * DB Requests :1
     *
     * @method GET
     */
    public function getList(string $target): void
    {
        // verify request method
        $this->checkRequestMethod('Get');
        // make HTTP response
        $this->response->setPayloadSuccess(Model::ls($target, $this->request->getTenant()))->send();
    }
}
?>
