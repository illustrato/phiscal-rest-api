<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Controller
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Controller;

use Phalcon\Http\Response\Cookies;
use Phalcon\Http\ResponseInterface;
use Phiscal\Model\Entity\Index as Model;
use Phiscal\Model\Entity\Tenant\Payment;
use Phiscal\Model\Validation\Etc\Inquiry as InquiryValidator;
use function Phiscal\Core\envValue;
use function Phiscal\Core\rmR;

/**
 * IndexController class
 *
 * @category  PHP
 * @package   Phiscal\Controller
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @link      127.0.0.1
 */
class Index extends Base
{
    /**
     * GreatReset function
     * PRIVATE
     * DB Requests :1
     *
     * @method DELETE
     */
    public function greatreset(): void
    {
        // verify HTTP request method
        $this->checkRequestMethod('Delete');

        // make database request
        Model::greatreset();
        $this->checkDbRequest();

        // clean server of associated files
        rmR("{$this->config->app->files}img/avatar", false);
        rmR("{$this->config->app->files}img/banner", false);
        rmR("{$this->config->app->files}img/branch", false);
        rmR("{$this->config->app->files}img/logo", false);

        // make HTTP response
        $this->response->setStatusCode($this->response::ACCEPTED)->send();
    }

    /**
     * Terms of Service function
     * PUBLIC
     * DB Requests :0
     *
     * @method GET
     */
    public function getTnC(): void
    {
        // verify HTTP request method
        $this->checkRequestMethod('Get');

        // find and read file containing the terms of service
        if (file_exists($file = 'usr/notes/tnc.md') && ($handle = fopen($file, 'r'))) {
            $termsofservice = filesize($file) > 0 ? fread($handle, filesize($file)) : '';
            fclose($handle);
            $this->response->setPayloadSuccess($termsofservice)->send();
            return;
        }

        // make HTTP response
        $this->response
            ->setPayloadError('Ts&Cs file not found')
            ->setStatusCode($this->response::NOT_FOUND)
            ->send();
    }

    /**
     * Make inquiry function
     * PUBLIC
     * DB Requests :1
     *
     * @method POST
     */
    public function makeInquiry(): void
    {
        // verify HTTP request method
        $this->checkRequestMethod('Post');

        // validate user input
        $payload = (array) $this->request->getJsonRawBody();
        $this->validateInput($payload, new InquiryValidator());

        $model = new Model($payload);

        // make HTTP response
        $this->response
            ->setPayloadSuccess($model->makeInquiry())
            ->setStatusCode($this->response::CREATED)
            ->send();
    }

    /**
     * List Countries
     * PUBLIC
     * DB Requests :1
     *
     * @method GET
     */
    public function getCountries(): void
    {
        // verify HTTP request method
        $this->checkRequestMethod('Get');
        // make HTTP response
        $this->response->setPayloadSuccess(Model::getCountries())->send();
    }

    /**
     * List Inquiries
     * PUBLIC
     * DB Requests :1
     *
     * @method GET
     */
    public function getInquiries(): void
    {
        // verify HTTP request method
        $this->checkRequestMethod('Get');
        // make HTTP response
        $this->response->setPayloadSuccess(Model::getInquiries())->send();
    }

    /**
     * Test function
     * PUBLIC
     *
     * @param string $param
     *
     * @method mixed
     */
    public function test(string $param): void
    {
        exit(var_dump(unlink('/var/www/phiscal/var/cache/session/state-tmp')));
        $payload = (array) $this->request->getJsonRawBody();
        $model = new Model($payload);
        $param = ['first' => $model->first ?? 'NC', 'second' => $model->second ?? 'NC', 'third' => $model->third ?? 'NC'];
        $check = Payment::check(['tenant_id' => $this->request->getTenant()], ['provider_id' => '--']);
        // Make HTTP Response
        $this->response->setPayloadSuccess($check)->send();
    }
}
?>
