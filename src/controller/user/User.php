<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Controller\User
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Controller\User;

use Phiscal\Controller\Base;
use Phiscal\Model\Entity\User\User as Model;
use Phiscal\Model\Entity\User\Profile;
use Phiscal\Model\Entity\User\Token;
use Phiscal\Model\Entity\Org\Employee;
use Phiscal\Model\Entity\Tenant\Tenant;
use Phiscal\Model\Validation\User\User as UserValidator;

/**
 * UserController class
 *
 * @category  PHP
 * @package   Phiscal\Controller\User
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @link      127.0.0.1
 */
class User extends Base
{
    /**
     * Index function
     * PRIVATE
     * DB Requests :1
     *
     * @method GET
     */
    public function index(): void
    {
        // Verify HTTP Request Method
        $this->checkRequestMethod('Get');
        // Make HTTP Response
        $this->response->setPayloadSuccess(Model::ls())->send();
    }

    /**
     * Get User Data
     * PRIVATE
     * DB Requests :1
     *
     * @param string $target
     *
     * @method GET
     */
    public function getData(string $target)
    {
        // Verify HTTP Request Method
        $this->checkRequestMethod('Get');
        // Make HTTP Response
        switch ($target) {
            case 'tenant':
                $this->response->setPayloadSuccess(Model::getTenant($this->session->get('UID')))->send();
                break;
            default:
                $fields =
                    'firstname, ' .
                    'lastname, ' .
                    'idnumber, ' .
                    'nationality, ' .
                    'title, ' .
                    'dob, ' .
                    'addressline1, ' .
                    'addressline2, ' .
                    'city, ' .
                    'region, ' .
                    'country, ' .
                    'username, ' .
                    'alias, ' .
                    'email, ' .
                    'phone, ' .
                    'alias, ' .
                    'avatar';
                $this->response->setPayloadSuccess(Model::getData(['user_id' => $this->session->get('UID')], $fields))->send();
                break;
        }
    }

    /**
     * User Profiles
     * PRIVATE
     * DB Requests :1
     *
     * @method GET
     */
    public function getProfiles(): void
    {
        // Verify HTTP Request Method
        $this->checkRequestMethod('Get');
        // Make HTTP Response
        $this->response->setPayloadSuccess(Profile::ls())->send();
    }

    /**
     * List All Available Title
     * PUBLIC
     * DB Requests :1
     *
     * @method GET
     */
    public function getTitles(): void
    {
        // Verify HTTP Request Method
        $this->checkRequestMethod('Get');
        // Make HTTP Response
        $this->response->setPayloadSuccess(Model::getTitles())->send();
    }

    /**
     * Check User Record in Database
     * PUBLIC
     * DB Requests :1
     *
     * @param string $target
     * @param string $value
     *
     * @method GET
     */
    public function check(string $target, string $value): void
    {
        // Verify HTTP Request Method
        $this->checkRequestMethod('Get');
        // Make Database Request
        $pieces = explode(':', $value);
        $result = Model::check([$target => $pieces[0]], ['user_id' => $pieces[1] ?? '']);
        // Make HTTP Response
        $this->response->setPayloadSuccess($result)->send();
    }

    /**
     * Update User Data
     * PRIVATE
     * DB Requests :2
     *
     * @method POST
     */
    public function update(): void
    {
        // Verify HTTP Request Method
        $this->checkRequestMethod('Post');
        // Retrieve Database Record
        $user = Model::getData(
            ['USER_ID' => $this->session->get('UID')],
            "JSON_VALUE(user, '$.fullname') fullname, JSON_VALUE(user, '$.email') email, JSON_VALUE(user,'$.employee') employee",
            'v_session'
        );
        $this->checkRecord($user);
        $user->setSource('user');

        // validate user input
        $payload = (array) $this->request->getJsonRawBody();
        if (boolval($user->employee)) {
            $this->restrictedFields($payload);
        }
        $this->validateInput($payload, new UserValidator(false));

        // Send Verification Email if email update is requested
        $this->emailUpdate($user, $payload);
        // Update Database Record
        if (count($payload) > 0) {
            $user->update($payload);
        }

        // Make HTTP Response
        $this->response->setPayloadSuccess(true)->send();
    }

    /**
     * Freelance Agent CRUD Operations
     * PRIVATE
     * DB Requests :1
     *
     * @method POST
     */
    public function freelanceAgent(): void
    {
        // Verify HTTP Request Method
        $this->checkRequestMethod('Post');

        // Make Database Request
        $model = new Model((array) $this->request->getJsonRawBody());
        $result = $model->crudFreelance();
        $this->checkDbRequest();

        // Make HTTP Response
        $this->response->setPayloadSuccess(json_decode($result))->send();
    }

    /**
     * Get list of Tenants belonging to a User
     * PRIVATE
     * DB Requests :1
     *
     * @method GET
     */
    public function tenantList(): void
    {
        // Verify HTTP Request Method
        $this->checkRequestMethod('Get');
        // Make HTTP Response
        $this->response->setPayloadSuccess(Tenant::adminList($this->session->get('UID')))->send();
    }

    /**
     * Update User Avatar
     * PRIVATE
     * DB Requests :2
     *
     * @method PUT
     */
    public function crudAvatar(): void
    {
        // Verify HTTP Request Method
        $this->checkRequestMethod('Post');

        // User Data
        $uId = $this->session->get('UID');
        $payload = (array) $this->request->getJsonRawBody();
        $file = "{$this->config->app->files}img/avatar/{$uId}.{$payload['picture']->ext}";

        // Check for the picture base64 blob in the request payload
        $avatar = ['up' => false];
        if (!empty($payload['picture']->base64)) {
            $avatar = array_merge(['up' => true], (array) $payload['picture']);
            $payload['avatar'] = $payload['picture']->ext;
        }
        unset($payload['picture']);

        // Update user record in database
        $model = (new Model($payload))->setId(['user_id' => $uId]);
        $oldFile = $model->avatar();
        $this->checkDbRequest();

        // Delete file in Physical File System
        if ($avatar['up'] === false) {
            if (!unlink($file)) {
                $model->update(['avatar' => $oldFile]);
                $this->intercept("Couldn't delete file '$file'", $this->response::FORBIDDEN);
            }
            $this->intercept(true, $this->response::OK);
        }

        // Upolad file in Physical File System
        if (file_exists($file) && !unlink($file)) {
            $this->intercept("Couldn't delete old photograph File '$file'", $this->response::FORBIDDEN);
        }
        if ($this->touch('img/avatar', $uId, $avatar) === false) {
            $model->update(['avatar' => $oldFile]);
        }

        // Make HTTP Response
        $this->response->setPayloadSuccess(true)->send();
    }

    /**
     * Action to change users password
     * PRIVATE
     * DB Requests :2
     *
     * @method PUT
     */
    public function resetPasswd(): void
    {
        // Verify HTTP Request Method
        $this->checkRequestMethod('Put');

        // Retrieve Database Record
        $user = Model::getData(['user_id' => $this->session->get('UID')], 'password'); // DB Request
        $this->checkRecord($user);

        // Validate New Password
        $payload = $this->request->getJsonRawBody();
        $this->validateInput((array) $payload, new UserValidator(false));

        // Password Switch
        $authentic = isset($payload->oldPassword) ? $this->security->checkHash($payload->oldPassword, $user->password) : false;
        $user->password = $this->security->hash($payload->newPassword);

        // Make HTTP Response
        $this->response->setPayloadSuccess($user->passwdReset($authentic))->send(); // DB Request
    }

    /**
     * Delete function
     * PUBLIC
     * DB Requests :1
     *
     * @param string $email Criteria
     *
     * @method DELETE
     */
    public function rm($email): void
    {
        // Verify HTTP Request Method
        $this->checkRequestMethod('Delete');

        // Make Database Request
        $avatar = Model::rm($email);
        if (!empty($avatar)) {
            $file = "{$this->config->app->files}img/avatar/$avatar";
            unlink($file);
        }

        // make HTTP response
        $this->response->setPayloadSuccess('gone!')->send();
    }

    /**
     * Email Update function
     *
     * @param Model\User	$user	: User table Model
     * @param Array 		$input	: User input
     *
     * @return void
     */
    private function emailUpdate(Model $user, array &$input): void
    {
        if (!isset($input['email'])) {
            return;
        }
        $newMail = strtolower(trim($input['email']));
        if ($newMail === $user->email) {
            return;
        }
        unset($input['email']);

        $uid = $user->getID(true)['user_id'];
        $tid = $uid . time();
        $token = md5($tid . $newMail);
        $model = new Token([
            'token_id' => $tid,
            'token' => $token,
            'user_id' => $uid,
            'type' => 'EML',
            'userAgent' => $newMail,
        ]);

        $model->add();
        $this->checkDbRequest();

        $email = [
            'to' => ['email' => $newMail, 'recepient' => $user->fullname],
            'subject' => 'Reset Email Address',
            'message' => $this->mail->resetEmail($uid, $token),
        ];

        $this->sendMail($email, $model);
    }

    /**
     * Ensures Employees don't update Restricted Fields
     *
     * @param array $payload	: POST Payload
     *
     * @return void
     */
    private function restrictedFields(array $payload): void
    {
        $restricted = ['firstname', 'lastname', 'nationality', 'idnumber', 'dob'];
        $fields = '';
        foreach ($payload as $key => $value) {
            $fields .= in_array(strtolower($key), $restricted) ? "'$key', " : '';
        }
        if (strlen($fields) > 0) {
            $this->intercept("Changing Field $fields is restricted to your Employer", $this->response::FORBIDDEN);
        }
    }
}
?>
