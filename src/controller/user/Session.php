<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Controller\User
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Controller\User;

use Phiscal\Controller\Base;
use Phiscal\Model\Entity\User\User;
use Phiscal\Model\Entity\User\Token;
use Phiscal\Model\Entity\Org\Employee;
use Phiscal\Model\Validation\User\User as UserValidator;
/**
 * SessionController class
 *
 * @category  PHP
 * @package   Phiscal\Controller\User
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

class Session extends Base
{
    /**
     * Allow a user to signup to the system
     * PUBLIC
     * DB Requests :1
     *
     * @method POST
     */
    public function signup(): void
    {
        // verify HTTP request method
        $this->checkRequestMethod('Post');

        // validate user input
        $payload = (array) $this->request->getJsonRawBody();
        $this->validateInput($payload, new UserValidator(true));

        // create user record in database
        $model = new User($payload);
        $user = $model->signup();
        $this->checkDbRequest();
        $this->checkRecord($user, 'Signup Failed', $this->response::INTERNAL_SERVER_ERROR);

        // send emails only is config value is set to true
        $this->checkEmailConfig();

        $email = [
            'to' => ['email' => $user->email, 'recepient' => $user->fullname],
            'subject' => 'Verify Account',
            'message' => $this->mail->verifyemail($user->id, $user->token),
        ];
        // verify user email address
        $this->sendMail($email, $user);

        // make HTTP response
        $this->response
            ->setPayloadSuccess("Signup Successful! Confirmation Email sent to {$user->email}.")
            ->setStatusCode($this->response::CREATED)
            ->send();
    }

    /**
     * Email Verification
     * PUBLIC
     * DB Requests :1
     *
     * @param string $action	: Verification Type
     * @param string $uId	: User ID
     * @param string $token	: Verification Token
     *
     * @method GET
     */
    public function emailVerification(string $action, string $uId, string $token): void
    {
        // verify request method
        $this->checkRequestMethod('Get');

        // verify token from database
        $model = new User(['target' => $action, 'user_id' => $uId, 'token' => $token]);
        $result = $model->emailverification();
        $this->checkDbRequest();

        // JWT
        $jwt = $model->getToken();
        $token = new Token([
            'token_id' => $jwt->getClaims()->get('jti'),
            'token' => $jwt->getToken(),
            'user_id' => $uId,
            'type' => 'JWT',
            'useragent' => $this->request->getUserAgent(),
        ]);

        // action specific request
        switch ($action) {
            case 'revoke':
                if (!empty($result)) {
                    $file = "{$this->config->app->files}img/avatar/$result";
                    unlink($file);
                }
                // respond to resquest
                $this->response->setPayloadSuccess(true)->send();
                break;
            default:
                $token->add();
                if (!!$result) {
                    $this->response->setHeader('Module', $result);
                }
                // respond to resquest
                $this->response->setPayloadSuccess($jwt->getToken())->send();
                break;
        }
    }

    /**
     * Get Session User Data from JWT
     * PRIVATE
     * DB Requests :2
     *
     * @method GET
     * @throws Exception
     */
    public function verifyAuth(): void
    {
        // verify request method
        $this->checkRequestMethod('Get');

        try {
            $uId = $this->auth->uidByToken($this->request->getBearerToken());

            // make HTTP response
            $this->response->setPayloadSuccess(User::getSession($uId, $this->request->getTenant()))->send();
        } catch (\Exception $e) {
            $this->response
                ->reportErrors([$e->getMessage()], $e->getCode())
                ->setStatusCode($this->response::UNAUTHORIZED)
                ->send();
        }
    }

    /**
     * Generate JWT based on correct Login Details
     * PUBLIC
     * DB Requests :3
     *
     * @method POST
     * @throws Exception
     */
    public function login(): void
    {
        try {
            if (!$this->request->isPost()) {
                if ($this->auth->hasRememberMe()) {
                    $this->auth->loginWithRememberMe();
                }
            } else {
                $this->auth->check($this->request->getJsonRawBody());
            }
            // make HTTP response
            $this->response->setPayloadSuccess($this->session->get('JWT'))->send();
        } catch (\Exception $e) {
            $this->response
                ->reportErrors([$e->getMessage()], $e->getCode())
                ->setStatusCode($this->response::UNAUTHORIZED)
                ->send();
        }
    }

    /**
     * Signin using Password Reset Email
     * PUBLIC
     * DB Requests :3
     *
     * @param string $uId	: User ID
     * @param string $token	: Verification Token
     *
     * @method GET
     */
    public function emailSignin(string $uId, string $token): void
    {
        // verify request method
        $this->checkRequestMethod('Get');
        $model = Token::getData(['user_id' => $uId, 'tokentype_id' => 'PSD', 'token' => $token]);
        $this->checkRecord($model);

        // JWT
        $jwt = (new User(['user_id' => $uId]))->getToken();
        $token = new Token([
            'token_id' => $jwt->getClaims()->get('jti'),
            'token' => $jwt->getToken(),
            'user_id' => $uid,
            'type' => 'JWT',
            'useragent' => $this->request->getUserAgent(),
        ]);
        $token->add();
        // make HTTP response
        $this->response->setPayloadSuccess($jwt->getToken())->send();
    }

    /**
     * Resolve Email Reset Request
     *
     * @param string $uid	: User ID
     * @param string $token	: Verification Token
     *
     * @return void
     */
    public function emailUpdate(string $uid, string $token): void
    {
        // verify request method
        $this->checkRequestMethod('Get');

        // verify Token from Database
        $model = new User(['user_id' => $uid]);
        // make HTTP response
        $this->response->setPayloadSuccess($model->updateEmail($token))->send();
    }

    /**
     * Resquests Password Reset
     * PUBLIC
     * DB Requests :1
     *
     * @method POST
     */
    public function forgotPasswd(): void
    {
        // verify request method
        $this->checkRequestMethod('Post');

        // Send emails only is config value is set to true
        $this->checkEmailConfig();

        // execute database request
        $model = new User((array) $this->request->getJsonRawBody());
        $user = $model->forgotPasswd();
        $this->checkDbRequest();
        $this->checkRecord($user, 'Process Failed', $this->response::INTERNAL_SERVER_ERROR);

        $token = json_decode($user->token);
        $email = [
            'to' => ['email' => $user->email, 'recepient' => $user->fullname],
            'subject' => 'Reset Password',
            'message' => $this->mail->resetPassword($user->id, $token->token),
        ];

        // verify user email address
        $this->sendMail($email, $token);

        // make HTTP response
        $this->response
            ->setPayloadSuccess("Password Reset Email Sent! Login your mail account at {$user->email} to complete process.")
            ->send();
    }

    /**
     * Delete Token
     * PRIVATE
     * DB Requests :1
     *
     * @method DELETE
     */
    public function rmJWT(): void
    {
        // verify request method
        $this->checkRequestMethod('Delete');
        // make HTTP response
        $this->response->setPayloadSuccess($this->auth->deleteToken())->send();
    }

    /**
     * Reading Notifications
     * PRIVATE
     * DB Requests :1
     *
     * @method GET
     */
    public function viewedFeeds(): void
    {
        // verify request method
        $this->checkRequestMethod('Get');
        // execute database request
        $feeds = (new User())->readfeeds();
        $this->checkDbRequest();
        if ($feeds === false) {
            $this->intercept('Database Error', 500);
        }
        // make HTTP response
        $this->response->setPayloadSuccess($feeds)->send();
    }
}
?>
