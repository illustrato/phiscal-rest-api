<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Provider
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal\Provider;

use Phalcon\Crypt as PhCrypt;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class Crypt implements ServiceProviderInterface
{
    /**
     * @var string
     */
    protected $providerName = 'crypt';

    /**
     * @param DiInterface $di
     *
     * @return void
     */
    public function register(DiInterface $di): void
    {
        /** @var string $cryptSalt */
        $cryptSalt = $di->getShared('config')->app->salt;

        $di->set($this->providerName, function () use ($cryptSalt) {
            $crypt = new PhCrypt();
            $crypt->setKey($cryptSalt);

            return $crypt;
        });
    }
}
?>
