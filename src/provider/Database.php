<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Provider
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Provider;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

use function Phiscal\Core\envValue;

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
class Database implements ServiceProviderInterface
{
    /**
     * @var string
     */
    protected $providerName = 'db';

    public function register(DiInterface $di): void
    {
        $dbConfig = [
            'adapter' => envValue('PHISCAL_DB_ADAPTER'),
            'host' => envValue('PHISCAL_DB_HOST'),
            'user' => envValue('STRATO_DB_USER'),
            'password' => envValue('STRATO_DB_PASSWD'),
            'dbname' => envValue('PHISCAL_DB_NAME'),
            'charset' => envValue('PHISCAL_DB_CHARSET'),
        ];
        $di->setShared($this->providerName, function () use ($dbConfig) {
            $dbClass = 'Phalcon\Db\Adapter\Pdo\\' . $dbConfig['adapter'];
            unset($dbConfig['adapter']);

            return new $dbClass($dbConfig);
        });
    }
}
