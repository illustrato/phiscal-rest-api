<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscus\Provider
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Provider;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

use Phalcon\Events\Manager;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\Collection;
use Phalcon\Mvc\Router as PhRouter;

use Phiscal\Application;
use Phiscal\Middleware\CORS;
use Phiscal\Middleware\Request;

use Phiscal\Model\Entity\Index as Model;

/**
 * Router class
 *
 * @category  PHP
 * @package   Phiscal\Provider
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Router implements ServiceProviderInterface
{
    /**
     * @var string
     */
    protected $providerName = 'router';

    /**
     * @param DiInterface $di
     *
     * @return void
     */
    public function register(DiInterface $di): void
    {
        $di->set($this->providerName, PhRouter::class);
        /** @var Micro $application */
        $application = $di->getShared(Application::APPLICATION_PROVIDER);
        /** @var Manager $eventsManager */
        $eventsManager = new Manager();

        $this->attachRoutes($application);
        $this->attachMiddleware($application, $eventsManager);

        $application->setEventsManager($eventsManager);
    }

    /**
     * Attaches the middleware to the application
     *
     * @param Micro $application
     * @param Manager $eventsManager
     *
     * @return void
     */
    private function attachMiddleware(Micro $application, Manager $eventsManager): void
    {
        $middleware = $this->getMiddleware();

        /**
         * Get the events manager and attach the middleware to it
         */
        foreach ($middleware as $class => $function) {
            $eventsManager->attach('micro', new $class());
            $application->{$function}(new $class());
        }
    }

    /**
     * Attaches the routes to the application; lazy loaded
     *
     * @param Micro $application
     *
     * @return void
     */
    private function attachRoutes(Micro $application): void
    {
        $routes = $this->getRoutes();

        foreach ($routes as $prefix => $endpoint) {
            $collection = new Collection();
            $collection->setHandler($endpoint['handler'], true)->setPrefix("/$prefix");
            foreach ($endpoint['collection'] as $route) {
                $collection->{$route->method}("/{$route->uri}", $route->action);
                if ($route->preflight) {
                    $collection->options("/{$route->uri}", 'preflight');
                }
            }
            if (count($endpoint['collection']) > 0) {
                $application->mount($collection);
            }
        }

        // special cases
        $index = new Collection();
        $index->setHandler('Phiscal\Controller\Index', true)->mapVia('/test/{param}', 'test', ['GET', 'POST', 'PUT', 'DELETE']);
        $application->mount($index);

        $application->notFound(function () use ($application) {
            $code = $application->response::NOT_FOUND;
            $message = $application->response->getHttpCodeDescription($code);
            $application->response->setStatusCode($code, $message)->sendHeaders();
            $application->response->setPayloadError($application->request->getURI());
            $application->response->send();
        });
    }

    /**
     * Returns the array for the middleware with the action to attach
     *
     * @return array
     */
    private function getMiddleware(): array
    {
        return [
            CORS::class => 'before',
            Request::class => 'before',
        ];
    }

    /**
     * Returns the array for the routes
     *
     * @return array
     */
    private function getRoutes(): array
    {
        $resources = Model::getResources();
        $routes = [];

        foreach ($resources as $resource) {
            $routes[$resource->name] = ['handler' => $resource->handler, 'collection' => []];
        }
        $list = Model::getRoutes();
        foreach ($list as $route) {
            $routes[$route->resource]['collection'][] = $this->route(
                $route->method,
                $route->route,
                $route->action,
                boolval($route->preflight)
            );
        }
        return $routes;
    }

    /**
     * Create Route
     *
     * @return object
     */
    private function route(string $method, string $uri, string $action, bool $preflight = false): object
    {
        return (object) ['method' => $method, 'uri' => $uri, 'action' => $action, 'preflight' => $preflight];
    }
}
?>
