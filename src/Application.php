<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Phiscal;

use Phalcon\Di;
use Phalcon\Mvc\Micro;

use function Phiscal\Core\appPath;

class Application
{
    const APPLICATION_PROVIDER = 'application';

    /**
     * @var Micro
     */
    protected $application;

    /**
     * @var DiInterface
     */
    protected $di;

    /**
     * @param string $rootPath
     *
     * @throws Exception
     */
    public function __construct()
    {
        $this->di = new DI();
        $this->di->set('metrics', microtime(true));
        $this->setupApplication();
        $this->registerServices();
    }

    /**
     * Application
     *
     * @return Micro
     */
    public function getApplication(): Micro
    {
        return $this->application;
    }

    /**
     * Run Vökuró Application
     *
     * @return string
     * @throws Exception
     */
    public function run(): string
    {
        return (string) $this->application->handle($_SERVER['REQUEST_URI']);
    }

    /**
     * @return Micro
     */
    protected function setupApplication(): void
    {
        $this->application = new Micro($this->di);
        $this->di->setShared(self::APPLICATION_PROVIDER, $this->application); // register this class in the Di container using the name bootstrap
    }

    /**
     * @throws Exception
     */
    protected function registerServices(): void
    {
        $filename = appPath('core/providers.php');
        if (!file_exists($filename) || !is_readable($filename)) {
            throw new \Exception("File '$filename' does not exist or is not readable.");
        }

        $providers = require_once $filename;
        foreach ($providers as $providerClass) {
            /** @var ServiceProviderInterface $provider */
            $provider = new $providerClass();
            $provider->register($this->di);
        }
    }
}
?>
