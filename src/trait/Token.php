<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Phiscal\Traiting
 * @author    Gowan Cephus <peculiar@exclusivemail.co.za>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Phiscal\Traiting;

use Phalcon\Security\JWT\Signer\Hmac;
use Phalcon\Security\JWT\Builder;
use Phalcon\Security\JWT\Token\Parser;
use Phalcon\Security\JWT\Token\Token as Wrapper;

use Phalcon\Mvc\Micro;

use function Phiscal\Core\envValue;

/**
 * Trait Token
 * @author Gowan Cephus
 */
trait Token
{
    /**
     * Decode JWT string
     *
     * @param string $jwt Token
     *
     * @return Token
     */
    protected function getToken(string $jwt): Wrapper
    {
        return (new Parser())->parse($jwt);
    }

    /**
     * Returns token signing algorithm
     *
     * @return string
     */
    protected function getTokenAlgorithm(): string
    {
        return envValue('TOKEN_ALGORTHM', 'sha256');
    }

    /**
     * Returns the default audience for the tokens
     *
     * @return array
     */
    protected function getTokenAudience(): array
    {
        /** @var string $audience */
        $audience = envValue('TOKEN_AUDIENCE', '["https://www.phiscal.site", "android"]');
        return json_decode($audience);
    }

    /**
     * Returns the time the token is issued at
     *
     * @return int
     */
    protected function getTokenTimeIssuedAt(): int
    {
        return time();
    }

    /**
     * Returns the time drift i.e. token will be valid not before
     *
     * @return int
     */
    protected function getTokenTimeNotBefore(): int
    {
        return time() + envValue('TOKEN_NOT_BEFORE', -60);
    }

    /**
     * Returns the expiry time for the token
     *
     * @return int
     */
    protected function getTokenTimeExpiration(): int
    {
        return time() + envValue('TOKEN_EXPIRATION', 1800);
    }
}
?>
