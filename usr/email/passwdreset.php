<style>
*{margin:0;padding:0}
.danger{color:#800}
.hint{font-family:Arial;padding:0 2em;font-size: 9pt}
.hint b{font-size:8pt;}
body{font-family:arial}
div.break{padding: 5px 0}
</style>

<div class="wrap" style="font-family:Arial;min-height: 100%;color: #444;background: #fff;position: relative">
    <header class="title" style="border-bottom: 2px solid #444;margin-bottom: .6em">
        <ul class="streight-nav" style="display: table;width: 100%;">
            <li style="display: table-cell;width: 20%;vertical-align: middle;text-align: center"><span><img alt="Phiscal" src="https://files.phiscal.site/img/brand/phiscal/main.png" height="36" /></span></li>
            <li style="background-color:#fff;padding: 0 .6em;color: #444;display: table-cell;width: 20%;vertical-align: middle;text-align: center"><strong>Business</strong> . Information . System</li>
            <li style="display: table-cell;width: 20%;vertical-align: middle;text-align: center"><span>Reset Password</span></li>
        </ul>
    </header>
    <section>
        <div class="container">
            <p>
                You've received this mail upon requesting to reset password protecting your <b class="brownfox">Phiscal</b><sup>TM</sup> account.<br/>
                Click the "<b style='color:#444'>Reset</b>" button below to finalize process.
            </p>

<div class="break"></div>

<a class='btn' href='btnlink1' style='margin-left:2em;background-color: #444;color: #fff;text-decoration: none;width: 85px;line-height: 30px;display: block;border-radius: 40px;border: 1px solid #484329;transition: 1s;text-align: center;font-family: Arial, Sans-serif'>Reset</a>
            
<div class="break"></div>
            <p>
                <em>If you didn't request any passoword reset,  please <u>ignore mail</u> and get on with your life.</em>
            </p>
<div class="break"></div>

            <p style="font-family:Arial;padding:0 2em;font-size: 9pt"><b style="font-size:8pt">HINT: </b><br/>
                After reseting password, this mail becomes obsolete, so there's no point keeping it in your mailbox.
            </p>
            <img class="logo" style="float: right;width:  20%" src="https://files.phiscal.site/img/brand/phiscal/Illustrato.jpg" alt='Phiscal'/>
            <br/>
            <p>
                <b class="cap-line" style="font-size:16pt"><b style="float: left;width: 0.7em;font-size: 300%;line-height: 80%;margin-right: .1em">R</b><span style="margin-left: -.1em">egards</span></b><br/>
                <a href="http://www.illustrato.org" target="_blank">illustrato</a><br/>
                <b style="font-weight:100;font-size:9pt">
                    Port Elizabeth<br/>
                    <a href="http://www.mandela.ac.za" target="_blank">Nelson Mandela University</a>
                </b>
            </p>
        </div>
    </section>
</div>
<style>
    .title li span{background-color: #888;color: #fff;display: flex;font-size: 26px;padding: 0 .6rem}
    ul.streight-nav > li {display: table-row;}
    .brownfox{font-family: codepredators,Arial,sans-serif}
    .box-604{background-color: #C0B283;color:#fff;padding: 0 .3em}
    .container{width: 100%;margin: 0}
    p{padding: 0 .6em}
    .clearfix:after{content: "";display: table; clear:both}
    .align-center{text-align: center}
    a.btn:hover{background-color: #686351}
    img.logo{position: absolute;bottom: 5%;right: 5%;width:  25%}
    @media screen and (min-width: 480px){
        .container{width: 80%;margin: 0 auto}
        p{padding: 0 2em}     
    }
    @media screen and (min-width: 768px){
        .verbose{display:initial}
        ul.streight-nav > li {display: table-cell;}
        img.logo{width: 15%}
    }
</style>
