#!/bin/bash

read -p 'Is your MariaDB root user password protected?[y/N]: ' haspass
haspass=`echo $haspass | tr '[A-Z]' '[a-z]'` #transform value to lower case

while [ $haspass != "y" ] && [ $haspass != "n" ]; do
	echo option not understood!
	echo please use Y or N
	echo
	read -p 'Is your MariaDB root user password protected?[y/N]: ' haspass
done

if [ "$haspass" = "y" ]; then
	stty -echo
	printf "Enter MariaDB Root Password: "
	read  passvar
	echo
	mysql -uroot -p$passvar < usr/sql/admin.sql
	mysql -uroot -p$passvar < usr/sql/schema.sql
	mysql -uroot -p$passvar phiscal < usr/sql/data.sql
	mysql -uroot -p$passvar phiscal < usr/sql/triggers.sql
else
        mysql -uroot < usr/sql/admin.sql
        mysql -uroot < usr/sql/schema.sql
        mysql -uroot phiscal < usr/sql/data.sql
	mysql -uroot phiscal < usr/sql/triggers.sql
fi
