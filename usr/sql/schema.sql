-- MariaDB dump 10.19  Distrib 10.5.13-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: phiscal
-- ------------------------------------------------------
-- Server version	10.5.13-MariaDB-0ubuntu0.21.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `phiscal`
--

/*!40000 DROP DATABASE IF EXISTS `phiscal`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `phiscal` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `phiscal`;

--
-- Table structure for table `account_state`
--

DROP TABLE IF EXISTS `account_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_state` (
  `ACCST_ID` tinyint(4) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `HINT` varchar(35) DEFAULT NULL,
  `REPORT` varchar(95) DEFAULT NULL,
  PRIMARY KEY (`ACCST_ID`),
  UNIQUE KEY `STATE` (`STATE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `action`
--

DROP TABLE IF EXISTS `action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action` (
  `ACTION_ID` char(4) CHARACTER SET utf8 NOT NULL,
  `NAME` varchar(24) CHARACTER SET utf8 NOT NULL,
  `DESCRIPTION` varchar(36) DEFAULT NULL,
  `RESOURCE_ID` char(2) CHARACTER SET utf8 NOT NULL,
  `PRIVATE` tinyint(1) NOT NULL DEFAULT 1,
  `METHOD` enum('get','post','delete','put') NOT NULL DEFAULT 'get',
  `ROUTE` varchar(32) DEFAULT NULL,
  `PREFLIGHT` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ACTION_ID`),
  UNIQUE KEY `NAME` (`NAME`,`RESOURCE_ID`),
  UNIQUE KEY `ROUTE` (`ROUTE`,`METHOD`,`RESOURCE_ID`),
  KEY `RESOURCE_ID` (`RESOURCE_ID`),
  CONSTRAINT `action_ibfk_1` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `resource` (`RESOURCE_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `USER_ID` char(12) CHARACTER SET utf8 NOT NULL,
  `PROFILE_ID` char(1) CHARACTER SET utf8 NOT NULL DEFAULT 'A',
  `ORG_ID` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`USER_ID`,`ORG_ID`),
  KEY `PROFILE_ID` (`PROFILE_ID`),
  KEY `ORG_ID` (`ORG_ID`),
  CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `admin_ibfk_2` FOREIGN KEY (`PROFILE_ID`) REFERENCES `profile` (`PROFILE_ID`) ON UPDATE CASCADE,
  CONSTRAINT `admin_ibfk_3` FOREIGN KEY (`ORG_ID`) REFERENCES `organization` (`ORG_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `branch`
--

DROP TABLE IF EXISTS `branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branch` (
  `ORG_ID` bigint(20) unsigned NOT NULL,
  `BRANCH_NO` smallint(5) unsigned NOT NULL,
  `MAIN` tinyint(1) NOT NULL DEFAULT 0,
  `ADDRESSLINE1` varchar(45) NOT NULL,
  `ADDRESSLINE2` varchar(45) DEFAULT NULL,
  `CITY` varchar(35) DEFAULT NULL,
  `COUNTRY` char(2) CHARACTER SET utf8 NOT NULL,
  `GPS` text NOT NULL,
  `TELEPHONE` char(15) CHARACTER SET utf8 DEFAULT NULL,
  `PHOTO` varchar(5) DEFAULT NULL,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `REGION` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ORG_ID`,`BRANCH_NO`),
  UNIQUE KEY `TELEPHONE` (`TELEPHONE`),
  CONSTRAINT `branch_ibfk_1` FOREIGN KEY (`ORG_ID`) REFERENCES `organization` (`ORG_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `USER_ID` char(12) CHARACTER SET utf8 NOT NULL,
  `ORG_ID` bigint(20) unsigned NOT NULL,
  `PROFILE_ID` char(1) CHARACTER SET utf8 NOT NULL DEFAULT 'E',
  `BRANCH_NO` smallint(5) unsigned DEFAULT NULL,
  `ADDED` datetime NOT NULL DEFAULT current_timestamp(),
  `REFERRER` char(12) CHARACTER SET utf8 DEFAULT NULL,
  `EMAIL` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `TELEPHONE` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `TELEPHONE` (`TELEPHONE`),
  UNIQUE KEY `EMAIL` (`EMAIL`),
  KEY `TENANT` (`ORG_ID`),
  KEY `PROFILE_ID` (`PROFILE_ID`),
  KEY `TENANT_ID` (`ORG_ID`,`BRANCH_NO`),
  KEY `REFERRER` (`REFERRER`,`ORG_ID`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `employee_ibfk_2` FOREIGN KEY (`PROFILE_ID`) REFERENCES `profile` (`PROFILE_ID`) ON UPDATE CASCADE,
  CONSTRAINT `employee_ibfk_3` FOREIGN KEY (`ORG_ID`, `BRANCH_NO`) REFERENCES `branch` (`ORG_ID`, `BRANCH_NO`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `employee_ibfk_4` FOREIGN KEY (`ORG_ID`) REFERENCES `organization` (`ORG_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inquiry`
--

DROP TABLE IF EXISTS `inquiry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inquiry` (
  `QUEST_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(20) NOT NULL,
  `LAST_NAME` varchar(20) NOT NULL,
  `PHONE` varchar(15) DEFAULT NULL,
  `EMAIL` varchar(45) NOT NULL,
  `SUBJECT` varchar(90) NOT NULL,
  `CONTENT` text NOT NULL,
  `DATE` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`QUEST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `loginattempt`
--

DROP TABLE IF EXISTS `loginattempt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loginattempt` (
  `USER_ID` char(12) CHARACTER SET utf8 NOT NULL,
  `IP_ADDRESS` varchar(45) DEFAULT NULL,
  `USERAGENT` text DEFAULT NULL,
  `DATE` datetime NOT NULL DEFAULT current_timestamp(),
  `SUCCESS` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`USER_ID`,`DATE`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `loginattempt_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logo`
--

DROP TABLE IF EXISTS `logo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logo` (
  `ORG_ID` bigint(20) unsigned NOT NULL,
  `LOGO_NO` tinyint(3) unsigned NOT NULL,
  `EXT` varchar(5) NOT NULL,
  `MAIN` tinyint(1) NOT NULL DEFAULT 0,
  `DESCRIPTION` varchar(35) DEFAULT NULL,
  `WIDTH` smallint(5) unsigned DEFAULT NULL,
  `HEIGHT` smallint(5) unsigned DEFAULT NULL,
  `SIZE` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ORG_ID`,`LOGO_NO`),
  CONSTRAINT `logo_ibfk_1` FOREIGN KEY (`ORG_ID`) REFERENCES `organization` (`ORG_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module` (
  `NAME` varchar(20) NOT NULL,
  `DESCRIPTION` varchar(35) DEFAULT NULL,
  `MOD_ID` char(2) CHARACTER SET utf8 NOT NULL,
  `LOGO` char(3) DEFAULT NULL,
  `MICRO` tinyint(1) NOT NULL DEFAULT 0,
  `ACTIVE` tinyint(1) NOT NULL DEFAULT 0,
  `WEBCLIENT` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`MOD_ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `USER_ID` char(12) NOT NULL,
  `NOT_COUNT` smallint(5) unsigned NOT NULL,
  `TYPE` enum('alert','event','log') NOT NULL,
  `ICON` varchar(32) DEFAULT NULL,
  `MOD_ID` char(2) DEFAULT NULL,
  `MESSAGE` varchar(255) NOT NULL,
  `TIME` datetime NOT NULL DEFAULT current_timestamp(),
  `HAVE_READ` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`USER_ID`,`NOT_COUNT`),
  KEY `MOD_ID` (`MOD_ID`),
  CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `notification_ibfk_2` FOREIGN KEY (`MOD_ID`) REFERENCES `module` (`MOD_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `organization`
--

DROP TABLE IF EXISTS `organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization` (
  `ORG_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) NOT NULL,
  `REGNO` varchar(21) DEFAULT NULL,
  `WEBSITE` varchar(35) DEFAULT NULL,
  `MOTTO` varchar(95) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `STRUCT_ID` char(3) CHARACTER SET utf8 NOT NULL,
  `REGISTRA` varchar(85) DEFAULT NULL,
  `ADDED` datetime NOT NULL DEFAULT current_timestamp(),
  `MODIFIED` datetime DEFAULT NULL,
  `DBA` varchar(65) DEFAULT NULL,
  `TYPE` char(1) CHARACTER SET utf8 NOT NULL DEFAULT 'O',
  PRIMARY KEY (`ORG_ID`),
  UNIQUE KEY `ORG_ID` (`ORG_ID`,`TYPE`),
  UNIQUE KEY `REGNO` (`REGNO`,`REGISTRA`),
  KEY `STRUCT_ID` (`STRUCT_ID`),
  CONSTRAINT `organization_ibfk_1` FOREIGN KEY (`STRUCT_ID`) REFERENCES `structure` (`STRUCT_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `password_change`
--

DROP TABLE IF EXISTS `password_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_change` (
  `USER_ID` char(12) CHARACTER SET utf8 NOT NULL,
  `IPADDRESS` char(15) NOT NULL,
  `USERAGENT` text NOT NULL,
  `DATE` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`USER_ID`,`DATE`),
  CONSTRAINT `password_change_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paygateprovider`
--

DROP TABLE IF EXISTS `paygateprovider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paygateprovider` (
  `PROVIDER_ID` char(2) NOT NULL,
  `NAME` varchar(10) NOT NULL,
  PRIMARY KEY (`PROVIDER_ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paymentgateway`
--

DROP TABLE IF EXISTS `paymentgateway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentgateway` (
  `PROVIDER_ID` char(2) NOT NULL,
  `TENANT_ID` bigint(20) unsigned NOT NULL,
  `CLIENT_ID` varchar(35) DEFAULT NULL,
  `ACCESS_KEY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`TENANT_ID`),
  KEY `PROVIDER_ID` (`PROVIDER_ID`),
  CONSTRAINT `paymentgateway_ibfk_1` FOREIGN KEY (`PROVIDER_ID`) REFERENCES `paygateprovider` (`PROVIDER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `paymentgateway_ibfk_2` FOREIGN KEY (`TENANT_ID`) REFERENCES `tenant` (`TENANT_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `PROFILE_ID` char(1) CHARACTER SET utf8 NOT NULL,
  `ACTION_ID` char(4) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`PROFILE_ID`,`ACTION_ID`),
  KEY `ACTION_ID` (`ACTION_ID`),
  CONSTRAINT `permission_ibfk_1` FOREIGN KEY (`PROFILE_ID`) REFERENCES `profile` (`PROFILE_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_ibfk_2` FOREIGN KEY (`ACTION_ID`) REFERENCES `action` (`ACTION_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `PROFILE_ID` char(1) CHARACTER SET utf8 NOT NULL,
  `NAME` varchar(15) NOT NULL,
  `DESCRIPTION` varchar(35) DEFAULT NULL,
  `PRECEDENCE` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`PROFILE_ID`),
  UNIQUE KEY `NAME` (`NAME`),
  UNIQUE KEY `PRECEDENCE` (`PRECEDENCE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `registra`
--

DROP TABLE IF EXISTS `registra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registra` (
  `NAME` varchar(85) COLLATE utf8_unicode_ci NOT NULL,
  `alpha2Code` char(2) CHARACTER SET utf8 NOT NULL,
  `SHORTHAND` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEBSITE` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `NAME` (`NAME`),
  KEY `alpha2Code` (`alpha2Code`)
) ENGINE=CONNECT DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource` (
  `RESOURCE_ID` char(2) CHARACTER SET utf8 NOT NULL,
  `NAME` varchar(16) NOT NULL,
  `DESCRIPTION` varchar(35) DEFAULT NULL,
  `HANDLER` varchar(36) NOT NULL,
  PRIMARY KEY (`RESOURCE_ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structure`
--

DROP TABLE IF EXISTS `structure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structure` (
  `STRUCT_ID` char(3) NOT NULL,
  `NAME` varchar(25) NOT NULL,
  `DESCRIPTION` varchar(160) DEFAULT NULL,
  `SUFFIX` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`STRUCT_ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription` (
  `TENANT_ID` bigint(20) unsigned NOT NULL,
  `MOD_ID` char(2) NOT NULL,
  `DATE` datetime NOT NULL DEFAULT current_timestamp(),
  `MONTHS` tinyint(3) unsigned DEFAULT NULL,
  `ACTIVE` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`TENANT_ID`,`MOD_ID`,`DATE`),
  KEY `MOD_ID` (`MOD_ID`),
  CONSTRAINT `subscription_ibfk_1` FOREIGN KEY (`MOD_ID`) REFERENCES `module` (`MOD_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `subscription_ibfk_2` FOREIGN KEY (`TENANT_ID`) REFERENCES `tenant` (`TENANT_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tech`
--

DROP TABLE IF EXISTS `tech`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tech` (
  `USER_ID` char(12) CHARACTER SET utf8 NOT NULL,
  `PROFILE_ID` char(1) CHARACTER SET utf8 NOT NULL DEFAULT 'T',
  PRIMARY KEY (`USER_ID`),
  KEY `PROFILE_ID` (`PROFILE_ID`),
  CONSTRAINT `tech_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tech_ibfk_2` FOREIGN KEY (`PROFILE_ID`) REFERENCES `profile` (`PROFILE_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tenant`
--

DROP TABLE IF EXISTS `tenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tenant` (
  `TENANT_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `USER_ID` char(12) DEFAULT NULL,
  `ORG_ID` bigint(20) unsigned DEFAULT NULL,
  `TYPE` char(1) NOT NULL,
  PRIMARY KEY (`TENANT_ID`),
  UNIQUE KEY `ENTITY_ID` (`TENANT_ID`,`TYPE`),
  UNIQUE KEY `USER_ID` (`USER_ID`),
  UNIQUE KEY `TENANT_ID` (`ORG_ID`),
  KEY `ORG_ID` (`ORG_ID`,`TYPE`),
  KEY `USER_ID_2` (`USER_ID`,`TYPE`),
  CONSTRAINT `tenant_ibfk_1` FOREIGN KEY (`ORG_ID`, `TYPE`) REFERENCES `organization` (`ORG_ID`, `TYPE`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tenant_ibfk_2` FOREIGN KEY (`USER_ID`, `TYPE`) REFERENCES `user` (`USER_ID`, `TYPE`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `TOKEN_ID` char(22) CHARACTER SET utf8 NOT NULL,
  `TOKEN` text NOT NULL,
  `TOKENTYPE_ID` char(3) CHARACTER SET utf8 NOT NULL,
  `USER_ID` char(12) CHARACTER SET utf8 DEFAULT NULL,
  `TIME` datetime NOT NULL DEFAULT current_timestamp(),
  `USERAGENT` text DEFAULT NULL,
  PRIMARY KEY (`TOKEN_ID`),
  UNIQUE KEY `USER_ID` (`USER_ID`,`TOKENTYPE_ID`),
  KEY `USER` (`USER_ID`),
  KEY `TYPE` (`TOKENTYPE_ID`),
  CONSTRAINT `token_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `token_ibfk_2` FOREIGN KEY (`TOKENTYPE_ID`) REFERENCES `tokendesc` (`TOKENTYPE_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tokendesc`
--

DROP TABLE IF EXISTS `tokendesc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokendesc` (
  `TOKENTYPE_ID` char(3) CHARACTER SET utf8 NOT NULL,
  `NAME` varchar(25) NOT NULL,
  `DESCRIPTION` varchar(56) DEFAULT NULL,
  PRIMARY KEY (`TOKENTYPE_ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `USER_ID` char(12) CHARACTER SET utf8 NOT NULL,
  `USERNAME` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `FIRSTNAME` varchar(20) DEFAULT NULL,
  `LASTNAME` varchar(20) DEFAULT NULL,
  `IDNUMBER` varchar(32) DEFAULT NULL,
  `EMAIL` varchar(254) CHARACTER SET utf8 NOT NULL,
  `PHONE` varchar(15) DEFAULT NULL,
  `ADDRESSLINE1` varchar(45) DEFAULT NULL,
  `ADDRESSLINE2` varchar(45) DEFAULT NULL,
  `GPS` text DEFAULT NULL,
  `PASSWORD` char(60) CHARACTER SET utf8 DEFAULT NULL,
  `AVATAR` varchar(5) DEFAULT NULL,
  `ACTIVE` tinyint(1) NOT NULL DEFAULT 0,
  `DOB` date DEFAULT NULL,
  `TITLE` enum('Mr.','Ms.','Mrs','Dr.','Prof.','Rev.','Sir') DEFAULT NULL,
  `NATIONALITY` char(2) CHARACTER SET utf8 DEFAULT NULL,
  `CITY` char(35) DEFAULT NULL,
  `COUNTRY` char(2) CHARACTER SET utf8 DEFAULT NULL,
  `MODIFIED` datetime DEFAULT current_timestamp(),
  `ALIAS` varchar(20) DEFAULT NULL,
  `CREATED` datetime NOT NULL DEFAULT current_timestamp(),
  `REGION` varchar(45) DEFAULT NULL,
  `TYPE` char(1) CHARACTER SET utf8 NOT NULL DEFAULT 'U',
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `EMAIL` (`EMAIL`),
  UNIQUE KEY `USER_ID` (`USER_ID`,`TYPE`),
  UNIQUE KEY `USERNAME` (`USERNAME`),
  UNIQUE KEY `PHONE` (`PHONE`),
  KEY `ACTIVE` (`ACTIVE`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`ACTIVE`) REFERENCES `account_state` (`ACCST_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `v_activeUser`
--

DROP TABLE IF EXISTS `v_activeUser`;
/*!50001 DROP VIEW IF EXISTS `v_activeUser`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_activeUser` (
  `id` tinyint NOT NULL,
  `username` tinyint NOT NULL,
  `fullname` tinyint NOT NULL,
  `firstname` tinyint NOT NULL,
  `lastname` tinyint NOT NULL,
  `idnumber` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `phone` tinyint NOT NULL,
  `avatar` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_dopamine`
--

DROP TABLE IF EXISTS `v_dopamine`;
/*!50001 DROP VIEW IF EXISTS `v_dopamine`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_dopamine` (
  `feed` tinyint NOT NULL,
  `TIME` tinyint NOT NULL,
  `USER_ID` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_module`
--

DROP TABLE IF EXISTS `v_module`;
/*!50001 DROP VIEW IF EXISTS `v_module`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_module` (
  `mod_id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `webclient` tinyint NOT NULL,
  `logo` tinyint NOT NULL,
  `days` tinyint NOT NULL,
  `active` tinyint NOT NULL,
  `tenant_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_org_compact`
--

DROP TABLE IF EXISTS `v_org_compact`;
/*!50001 DROP VIEW IF EXISTS `v_org_compact`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_org_compact` (
  `organization` tinyint NOT NULL,
  `ORG_ID` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_org_depth`
--

DROP TABLE IF EXISTS `v_org_depth`;
/*!50001 DROP VIEW IF EXISTS `v_org_depth`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_org_depth` (
  `admin` tinyint NOT NULL,
  `tenant_id` tinyint NOT NULL,
  `org_id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `struct_id` tinyint NOT NULL,
  `dba` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `motto` tinyint NOT NULL,
  `regno` tinyint NOT NULL,
  `structure` tinyint NOT NULL,
  `registra` tinyint NOT NULL,
  `website` tinyint NOT NULL,
  `logo` tinyint NOT NULL,
  `ext` tinyint NOT NULL,
  `width` tinyint NOT NULL,
  `height` tinyint NOT NULL,
  `mainLogo` tinyint NOT NULL,
  `size` tinyint NOT NULL,
  `logoDesc` tinyint NOT NULL,
  `branch` tinyint NOT NULL,
  `photo` tinyint NOT NULL,
  `mainBranch` tinyint NOT NULL,
  `addressline1` tinyint NOT NULL,
  `addressline2` tinyint NOT NULL,
  `city` tinyint NOT NULL,
  `region` tinyint NOT NULL,
  `country` tinyint NOT NULL,
  `gps` tinyint NOT NULL,
  `telephone` tinyint NOT NULL,
  `branchDesc` tinyint NOT NULL,
  `staff` tinyint NOT NULL,
  `user_id` tinyint NOT NULL,
  `fullname` tinyint NOT NULL,
  `firstname` tinyint NOT NULL,
  `lastname` tinyint NOT NULL,
  `age` tinyint NOT NULL,
  `dob` tinyint NOT NULL,
  `idnumber` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `phone` tinyint NOT NULL,
  `nationality` tinyint NOT NULL,
  `avatar` tinyint NOT NULL,
  `active` tinyint NOT NULL,
  `organization` tinyint NOT NULL,
  `workEmail` tinyint NOT NULL,
  `workTelephone` tinyint NOT NULL,
  `empBranch` tinyint NOT NULL,
  `branchAddress` tinyint NOT NULL,
  `branchCity` tinyint NOT NULL,
  `branchRegion` tinyint NOT NULL,
  `branchCountry` tinyint NOT NULL,
  `state` tinyint NOT NULL,
  `report` tinyint NOT NULL,
  `gatewayName` tinyint NOT NULL,
  `gatewayProvider` tinyint NOT NULL,
  `gatewayClient` tinyint NOT NULL,
  `gatewayKey` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_profilePermission`
--

DROP TABLE IF EXISTS `v_profilePermission`;
/*!50001 DROP VIEW IF EXISTS `v_profilePermission`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_profilePermission` (
  `id` tinyint NOT NULL,
  `profile` tinyint NOT NULL,
  `resource` tinyint NOT NULL,
  `action` tinyint NOT NULL,
  `description` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_resourceAction`
--

DROP TABLE IF EXISTS `v_resourceAction`;
/*!50001 DROP VIEW IF EXISTS `v_resourceAction`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_resourceAction` (
  `resource` tinyint NOT NULL,
  `method` tinyint NOT NULL,
  `route` tinyint NOT NULL,
  `action` tinyint NOT NULL,
  `preflight` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `private` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_session`
--

DROP TABLE IF EXISTS `v_session`;
/*!50001 DROP VIEW IF EXISTS `v_session`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_session` (
  `user` tinyint NOT NULL,
  `USER_ID` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_staff`
--

DROP TABLE IF EXISTS `v_staff`;
/*!50001 DROP VIEW IF EXISTS `v_staff`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_staff` (
  `user_id` tinyint NOT NULL,
  `fullname` tinyint NOT NULL,
  `firstname` tinyint NOT NULL,
  `lastname` tinyint NOT NULL,
  `age` tinyint NOT NULL,
  `dob` tinyint NOT NULL,
  `idnumber` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `phone` tinyint NOT NULL,
  `nationality` tinyint NOT NULL,
  `avatar` tinyint NOT NULL,
  `active` tinyint NOT NULL,
  `organization` tinyint NOT NULL,
  `workEmail` tinyint NOT NULL,
  `workTelephone` tinyint NOT NULL,
  `empBranch` tinyint NOT NULL,
  `branchAddress` tinyint NOT NULL,
  `branchCity` tinyint NOT NULL,
  `branchRegion` tinyint NOT NULL,
  `branchCountry` tinyint NOT NULL,
  `state` tinyint NOT NULL,
  `report` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_tenant`
--

DROP TABLE IF EXISTS `v_tenant`;
/*!50001 DROP VIEW IF EXISTS `v_tenant`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_tenant` (
  `tenant_id` tinyint NOT NULL,
  `type` tinyint NOT NULL,
  `business` tinyint NOT NULL,
  `modules` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_user_tenant`
--

DROP TABLE IF EXISTS `v_user_tenant`;
/*!50001 DROP VIEW IF EXISTS `v_user_tenant`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_user_tenant` (
  `TENANT_ID` tinyint NOT NULL,
  `USER_ID` tinyint NOT NULL,
  `RANK` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'phiscal'
--
/*!50003 DROP FUNCTION IF EXISTS `auth` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `auth`(tenantId	BIGINT UNSIGNED,
	client	CHAR(12) CHARSET UTF8
) RETURNS tinyint(1)
BEGIN
	DECLARE target CHAR(1) DEFAULT (SELECT TYPE FROM tenant WHERE TENANT_ID = tenantId);
	DECLARE uid CHAR(12) CHARSET UTF8;
	DECLARE decider BOOLEAN DEFAULT FALSE;

	CASE target
	WHEN 'U' THEN
		BEGIN
			SELECT USER_ID FROM tenant WHERE TENANT_ID = tenantId INTO uid;
			SELECT uid = client INTO decider;
		END;
	WHEN 'O' THEN
		SELECT
			(SELECT COUNT(*) FROM admin WHERE ORG_ID = tenant(tenantId) AND USER_ID = client)
			||
			(SELECT COUNT(*) FROM employee WHERE ORG_ID = tenant(tenantId) AND USER_ID = client)
		INTO decider;
	ELSE
		RETURN decider;
	END CASE;

	RETURN decider;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `caste` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `caste`(string	VARCHAR(255),
	target	CHAR(2) CHARSET UTF8
) RETURNS char(1) CHARSET utf8
BEGIN
	DECLARE item CHAR(1) CHARSET UTF8 DEFAULT 'G';
	SET string = TRIM(string);
	CASE target
	WHEN 'ZA' THEN
		BEGIN
			IF LENGTH(string) < 13 THEN RETURN item; END IF;
			RETURN IF(SUBSTR(string, 7, 4) >= 5000, 'A', 'B');
		END;
	WHEN '--' THEN
		BEGIN
			IF LENGTH(string) < 12 THEN RETURN item; END IF;
			RETURN SUBSTR(string, 6, 1);
		END;
	ELSE RETURN item;
	END CASE;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `checkcredential` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `checkcredential`(target	CHAR(2),
	string	VARCHAR(255),
	uid	CHAR(11)
) RETURNS tinyint(1)
BEGIN
	DECLARE found BOOLEAN;

	CASE target
	WHEN 'em' THEN SELECT COUNT(USER_ID) FROM `user` WHERE `EMAIL` = string AND USER_ID <> uid INTO found;
	WHEN 'ph' THEN SELECT COUNT(USER_ID) FROM `user` WHERE `PHONE` = string AND USER_ID <> uid INTO found;
	WHEN 'un' THEN SELECT COUNT(USER_ID) FROM `user` WHERE `USERNAME` = string AND USER_ID <> uid INTO found;
	END CASE;

	RETURN found;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `dob` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `dob`(idno	VARCHAR(32),
	nation	CHAR(2),
	birthday DATE
) RETURNS date
BEGIN
	SET idno = TRIM(idno);


	CASE nation
	WHEN 'ZA' THEN
		IF idno REGEXP '^[0-9]{13}$' THEN
			SET birthday = STR_TO_DATE(SUBSTRING(idno,1,6),'%y%m%d');
		END IF;
	ELSE
	BEGIN
	END;
	END CASE;

	RETURN birthday;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `face` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `face`(subject	BIGINT UNSIGNED
) RETURNS varchar(255) CHARSET utf8mb4
BEGIN
	DECLARE target VARCHAR(6) DEFAULT (SELECT type FROM tenant WHERE TENANT_ID = subject);
	DECLARE id VARCHAR(90);
	DECLARE file VARCHAR(255);

	CASE target
		WHEN 'U' THEN
		BEGIN
			SELECT USER_ID FROM tenant WHERE TENANT_ID = subject INTO id;
			SELECT CONCAT('img/avatar/', id, '.', AVATAR) FROM user WHERE USER_ID = id INTO file;
		END;
		WHEN 'O' THEN
		BEGIN
			SELECT ORG_ID FROM tenant WHERE TENANT_ID = subject INTO id;
			SELECT CONCAT('img/logo/', id, '/', LOGO_NO,'.',EXT) FROM logo WHERE ORG_ID = id AND MAIN INTO file;
		END;
	END CASE;

	RETURN file;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `naam` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `naam`(txt VARCHAR(20)) RETURNS varchar(20) CHARSET utf8mb4
RETURN (SELECT CONCAT(UPPER(SUBSTRING(txt,1,1)),SUBSTRING(txt,2,LENGTH(txt)-1))) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `readfeeds` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `readfeeds`(client	CHAR(12) CHARSET UTF8
) RETURNS text CHARSET utf8mb4
BEGIN
	DECLARE whispers TEXT;

	UPDATE notification SET HAVE_READ = TRUE WHERE USER_ID = client;
	SELECT JSON_ARRAYAGG(feed ORDER BY `TIME` DESC) FROM v_dopamine WHERE USER_ID = client INTO whispers;

	RETURN whispers;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `remembertoken` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `remembertoken`(uId VARCHAR(12) CHARSET UTF8,
	agent TEXT
) RETURNS text CHARSET utf8mb4
BEGIN
	DECLARE minlate DATETIME DEFAULT DATE_ADD(NOW(), INTERVAL 1 MINUTE);
	DECLARE tId CHAR(19) CHARSET UTF8 DEFAULT CONCAT(uId,UNIX_TIMESTAMP(minlate));
	DECLARE umail VARCHAR(254) CHARSET UTF8 DEFAULT (SELECT EMAIL FROM user WHERE USER_ID = uId);
	DECLARE passwd CHAR(60) CHARSET UTF8 DEFAULT (SELECT PASSWORD FROM user WHERE USER_ID = uId);
	DECLARE tkn TEXT DEFAULT MD5(CONCAT(umail,passwd,agent));
	DECLARE ttype CHAR(3) CHARSET UTF8 DEFAULT 'RMT';
	DECLARE found BOOLEAN DEFAULT (SELECT COUNT(*) FROM token WHERE USER_ID = uId AND TOKENTYPE_ID = ttype);

	IF found THEN
		UPDATE token SET TIME = minlate WHERE USER_ID = uId AND TOKENTYPE_ID = ttype;
	ELSE
		INSERT INTO token VALUES(tId,tkn,ttype,uId,minlate,agent);
	END IF;

	RETURN tkn;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `signinfail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `signinfail`(uId CHAR(12) CHARSET UTF8,
	ip VARCHAR(45),
	agent VARCHAR(255)
) RETURNS int(11)
    DETERMINISTIC
BEGIN
	DECLARE attempts INT DEFAULT (SELECT COUNT(*) FROM loginattempt WHERE `USER_ID` = uId AND TIMESTAMPDIFF(MINUTE, `DATE`, NOW()) < 60);
	INSERT INTO loginattempt(USER_ID, IP_ADDRESS, USERAGENT, SUCCESS) VALUES(uId, ip, agent, FALSE);
	RETURN (attempts + 1);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `tenant` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `tenant`(tenantId	BIGINT UNSIGNED
) RETURNS varchar(255) CHARSET utf8mb4
BEGIN
	DECLARE tenantType CHAR(1) DEFAULT (SELECT TYPE FROM tenant WHERE TENANT_ID = tenantId);
	DECLARE primaryKey VARCHAR(255);

	IF tenantId < 1  || tenantType IS \N THEN
		RETURN 0;
	END IF;

	CASE tenantType
		WHEN 'O' THEN
			SELECT ORG_ID FROM tenant WHERE TENANT_ID = tenantId INTO primaryKey;
		WHEN 'U' THEN
			SELECT USER_ID FROM tenant WHERE TENANT_ID = tenantId INTO primaryKey;
	END CASE;

	RETURN primaryKey;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `tenantmodules` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `tenantmodules`(tenantId	BIGINT UNSIGNED
) RETURNS text CHARSET utf8mb4
BEGIN
	DECLARE list TEXT DEFAULT (
		SELECT JSON_ARRAYAGG(
			JSON_OBJECT(
				"mod_id", mod_id,
				"name", name,
				"description", description,
				"webclient", webclient,
				"logo", logo,
				"days", days,
				"active", active
			)
		)
		FROM v_module
		WHERE tenant_id = tenantId
	);
	RETURN IF(list IS \N, '[]',list);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `tenants` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `tenants`(uId	CHAR(12) CHARSET UTF8
) RETURNS varchar(255) CHARSET utf8mb4
BEGIN
	RETURN IFNULL((SELECT JSON_ARRAYAGG(TENANT_ID) FROM v_user_tenant WHERE `USER_ID` = uId),'[]');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `trademark` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `trademark`(target BIGINT UNSIGNED
) RETURNS varchar(128) CHARSET utf8mb4
BEGIN
	DECLARE uid CHAR(12) CHARSET UTF8;
	DECLARE org BIGINT UNSIGNED;
	DECLARE label VARCHAR(128);
	DECLARE class VARCHAR(6) DEFAULT(SELECT TYPE FROM tenant WHERE TENANT_ID = target);

	CASE class
	WHEN 'U' THEN
	BEGIN
		SELECT USER_ID FROM tenant WHERE TENANT_ID = target INTO uid;
		SELECT IF(ALIAS IS \N, CONCAT(IF(TITLE IS \N,'',CONCAT(TITLE,' ')),naam(FIRSTNAME),' ',naam(LASTNAME)), ALIAS)
			FROM user
			WHERE USER_ID = uid
			INTO label;
	END;
	WHEN 'O' THEN
	BEGIN
		SELECT ORG_ID FROM tenant WHERE TENANT_ID = target INTO org;
		SELECT CONCAT(o.NAME, ' ',IF(SUFFIX IS NULL,'',CONCAT( SUFFIX, '.' )))
			FROM organization o
			INNER JOIN structure s USING (STRUCT_ID)
			WHERE ORG_ID = org
			INTO label;
	END;
	ELSE BEGIN END;
	END CASE;

	RETURN TRIM(label);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `userprofile` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `userprofile`(uId VARCHAR(12) CHARSET UTF8,
	tenantId BIGINT UNSIGNED
) RETURNS varchar(64) CHARSET utf8mb4
BEGIN
	DECLARE x VARCHAR(64) DEFAULT (SELECT JSON_OBJECT('name', NAME, 'desc', DESCRIPTION) FROM profile WHERE PROFILE_ID = 'Z');
	DECLARE y VARCHAR(64) DEFAULT (SELECT JSON_OBJECT('name', NAME, 'desc', DESCRIPTION) FROM profile WHERE PROFILE_ID = 'T');
	DECLARE super BOOLEAN DEFAULT (SELECT COUNT(USER_ID) FROM tech WHERE USER_ID = uId);
	DECLARE tType CHAR(1) CHARSET UTF8;

	IF tenantId IS \N OR tenantId = 0 THEN
		RETURN IF(super, y, x);
	END IF;
	SELECT TYPE FROM tenant WHERE TENANT_ID = tenantId INTO tType;
	CASE tType
		WHEN 'U' THEN
		BEGIN
			SELECT COUNT(*) FROM tenant WHERE TENANT_ID = tenantId AND USER_ID = uId INTO super;
			SELECT JSON_OBJECT('name', NAME, 'desc', DESCRIPTION) FROM profile WHERE PROFILE_ID = 'I' INTO y;
			RETURN IF(super, y, x);
		END;
		WHEN 'O' THEN
		BEGIN
			SELECT COUNT(*) FROM admin WHERE ORG_ID = tenant(tenantId) AND USER_ID = uId INTO super;
			SELECT JSON_OBJECT('name', NAME, 'desc', DESCRIPTION) FROM profile WHERE PROFILE_ID = 'A' INTO y;
			IF super THEN RETURN y; END IF;

			SELECT COUNT(*) FROM employee WHERE ORG_ID = tenant(tenantId) AND USER_ID = uId INTO super;
			SELECT JSON_OBJECT('name', NAME, 'desc', DESCRIPTION) FROM profile WHERE PROFILE_ID = 'E' INTO y;
			IF super THEN RETURN y; END IF;
		END;
		ELSE RETURN x;
	END CASE;

	RETURN x;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `userprofiles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `userprofiles`(uId VARCHAR(12) CHARSET UTF8
) RETURNS text CHARSET utf8mb4
BEGIN
	DECLARE array TEXT DEFAULT '';
	DECLARE done, matched BOOLEAN DEFAULT FALSE;

	DECLARE temp_rank VARCHAR(15);
	DECLARE temp_stage TINYINT;

	DECLARE cursor_rank CURSOR FOR SELECT NAME FROM profile WHERE PROFILE_ID <> 'Z' ORDER BY PRECEDENCE DESC;
	DECLARE cursor_stage CURSOR FOR SELECT PRECEDENCE FROM profile WHERE PROFILE_ID <> 'Z' ORDER BY PRECEDENCE DESC;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN cursor_rank;
	OPEN cursor_stage;

	read_loop: LOOP
		FETCH cursor_rank INTO temp_rank;
		FETCH cursor_stage INTO temp_stage;
		IF done THEN
			LEAVE read_loop;
		END IF;

		CASE temp_stage
			WHEN 4 THEN SELECT COUNT(USER_ID) FROM tech WHERE USER_ID = uId INTO matched;
			WHEN 3 THEN SELECT COUNT(USER_ID) FROM admin WHERE USER_ID = uId INTO matched;
			WHEN 2 THEN SELECT COUNT(USER_ID) FROM employee WHERE USER_ID = uId INTO matched;
			WHEN 1 THEN SELECT COUNT(USER_ID) FROM tenant WHERE USER_ID = uId INTO matched;
		END CASE;

		IF matched THEN
			SET array = CONCAT(array,',"',temp_rank,'"');
		END IF;
	END LOOP;

	CLOSE cursor_rank;
	CLOSE cursor_stage;

	IF LENGTH(array) = 0 THEN
		SELECT CONCAT(',"',NAME,'"') FROM profile WHERE PROFILE_ID = 'Z' INTO array;
	END IF;

	RETURN CONCAT('[',SUBSTRING(array,2),']');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `verify` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `verify`(uId		CHAR(12) CHARSET UTF8,
	tenantId	BIGINT UNSIGNED
) RETURNS text CHARSET utf8mb4
BEGIN
	DECLARE session TEXT DEFAULT (SELECT user FROM v_session WHERE USER_ID = uId);
	DECLARE business TEXT DEFAULT (SELECT business FROM v_tenant WHERE tenant_id = tenantId AND auth(tenantId, uId));
	DECLARE modules TEXT DEFAULT (SELECT modules FROM v_tenant WHERE tenant_id = tenantId AND auth(tenantId, uId));
	DECLARE whispers TEXT DEFAULT (SELECT JSON_ARRAYAGG(feed ORDER BY TIME DESC) FROM v_dopamine  WHERE USER_ID = uId);
	DECLARE unread SMALLINT UNSIGNED DEFAULT(SELECT COUNT(*) FROM notification  WHERE NOT `HAVE_READ` AND USER_ID = uId);

	DECLARE feeds TEXT DEFAULT CONCAT('"unread":', unread, ',"records":', IF(whispers IS \N, '[]', whispers));

	SET session = IF(session IS \N, FALSE, session);
	SET business = IF(business IS \N, FALSE, business);
	SET modules = IF(modules IS \N, '[]', modules);

	RETURN CONCAT('{"user":', session, ', "tenant":', business, ', "rank":', userprofile(uid,tenantId), ', "modules":', modules,', "feed":{', feeds, '}}');

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `auth` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `auth`(
	IN uId		VARCHAR(12) CHARSET UTF8,
	IN ip		VARCHAR(45),
	IN agent	TEXT,
	IN remember	BOOLEAN,
	IN RMT		TEXT,
	OUT code 	INT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE status BOOLEAN DEFAULT (SELECT ACTIVE FROM user WHERE USER_ID = uId);
	DECLARE umail VARCHAR(254) CHARSET UTF8 DEFAULT (SELECT EMAIL FROM user WHERE USER_ID = uId);
	DECLARE passwd CHAR(60) CHARSET UTF8 DEFAULT (SELECT PASSWORD FROM user WHERE USER_ID = uId);
	DECLARE tkn TEXT DEFAULT MD5(CONCAT(umail,passwd,agent));
	DECLARE stamp DATETIME;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	START TRANSACTION;
		CASE status
		WHEN 1 THEN
			BEGIN
				IF remember THEN
					SELECT TIME FROM token WHERE USER_ID = uId AND TOKEN = tkn INTO stamp;
					IF stamp IS \N || RMT <> tkn THEN
						SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Token Verification Failed', MYSQL_ERRNO = 3000;
					END IF;
					IF ((UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(stamp)) / (86400 * 8)) >= 8 THEN
						SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Token Expired', MYSQL_ERRNO = 3001;
					END IF;
				ELSE
					DELETE FROM loginattempt WHERE USER_ID = uId;
					INSERT INTO loginattempt(USER_ID,IP_ADDRESS,USERAGENT) VALUES(uId,ip,agent);
					DELETE FROM token WHERE USER_ID = uid AND TOKENTYPE_ID = 'JWT';
					SET info = tkn;
				END IF;

			END;
		ELSE
			BEGIN
			END;
		END CASE;
	COMMIT;

	SET code = status - 1;
	SELECT HINT FROM account_state WHERE ACCST_ID  = status INTO info;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `crud_avatar` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `crud_avatar`(
	IN target	CHAR(12) CHARSET UTF8,
	IN tenantId	BIGINT UNSIGNED,
	IN pic		VARCHAR(5),
	IN client	CHAR(12) CHARSET UTF8,
	OUT code 	INT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE org BIGINT UNSIGNED DEFAULT tenant(tenantId);
	DECLARE admin_name VARCHAR(41) DEFAULT(SELECT fullname FROM v_activeUser WHERE id = client);
	DECLARE isEmployed BOOLEAN;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	START TRANSACTION;

	CASE target
	WHEN 'self' THEN
		BEGIN
			SELECT AVATAR FROM user WHERE USER_ID = client INTO info;
			UPDATE user SET AVATAR = pic WHERE USER_ID = client;
		END;
	ELSE
		BEGIN
			IF NOT auth(tenantId, client) THEN
				SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "You're not authorized to perform action", MYSQL_ERRNO = 3000;
			END IF;
			SELECT COUNT(*) FROM employee WHERE USER_ID = target AND ORG_ID = org INTO isEmployed;
			IF NOT isEmployed THEN
				SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "This is No Employee", MYSQL_ERRNO = 3001;
			END IF;
			UPDATE user SET AVATAR = pic WHERE USER_ID = target;
			INSERT INTO notification(USER_ID, TYPE, ICON, MESSAGE) VALUES(target, 'event', 'fa fa-user text-success', CONCAT(admin_name ,IF(pic IS \N || pic = '',' removed',' uploaded'),' your profile picture.'));
			SELECT * FROM v_staff  WHERE organization = org;
		END;
	END CASE;

	COMMIT;
	SET code = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `crud_emp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `crud_emp`(
	IN uId		CHAR(12) CHARSET UTF8,
	IN fname	VARCHAR(20),
	IN lname	VARCHAR(20),
	IN nation	CHAR(2),
	IN idno		CHAR(13),
	IN birthday	DATE,
	IN umail	VARCHAR(45),
	IN wmail	VARCHAR(45),
	IN tel		VARCHAR(15),
	IN base		SMALLINT UNSIGNED,
	IN tenantId	BIGINT UNSIGNED,
	IN pic		VARCHAR(5),
	IN client	CHAR(12) CHARSET UTF8,
	IN skipmail	BOOLEAN,
	OUT code 	INT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE org BIGINT UNSIGNED DEFAULT tenant(tenantId);
	DECLARE admin_name VARCHAR(41) DEFAULT(SELECT fullname FROM v_activeUser WHERE id = client);
	DECLARE admin_mail VARCHAR(45) DEFAULT(SELECT email FROM v_activeUser WHERE id = client);
	DECLARE org_name VARCHAR(45) DEFAULT(SELECT NAME FROM organization WHERE ORG_ID = org);
	DECLARE utoken TEXT DEFAULT MD5(CONCAT(umail,nation));
	DECLARE active BOOLEAN DEFAULT (SELECT ACTIVE FROM user WHERE USER_ID = uId);
	DECLARE TID CHAR(22) CHARSET UTF8;
	DECLARE isEmployed BOOLEAN;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	START TRANSACTION;

	IF NOT auth(tenantId, client) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "You're not authorized to perform action", MYSQL_ERRNO = 3000;
	END IF;

	IF uid IS \N THEN
	BEGIN
		INSERT INTO user(FIRSTNAME, LASTNAME, NATIONALITY, IDNUMBER, DOB, EMAIL, ACTIVE, AVATAR) VALUES(fname, lname, nation, idno, birthday, umail, skipmail, pic);
		SET uid = @USER_ID;
		INSERT INTO employee(USER_ID, ORG_ID, REFERRER, EMAIL, TELEPHONE) VALUES(uId, org, client, wmail, tel);

		IF NOT skipmail THEN
			SELECT CONCAT(uid,UNIX_TIMESTAMP()) INTO TID;
			INSERT INTO token(TOKEN_ID, USER_ID, TOKEN, TOKENTYPE_ID) VALUES(TID, uId, utoken, 'REG');
		END IF;
	END;
	ELSE
	BEGIN
		SELECT COUNT(*) FROM employee WHERE USER_ID = uId AND ORG_ID = org INTO isEmployed;

		IF NOT isEmployed THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "This is No Employee", MYSQL_ERRNO = 3001;
		END IF;

		UPDATE user
		SET
			FIRSTNAME = fname,
			LASTNAME = lname,
			NATIONALITY = nation,
			IDNUMBER = idno,
			DOB = birthday,
			EMAIL = umail
		WHERE
			USER_ID = uid;

		UPDATE employee
		SET
			EMAIL = wmail,
			TELEPHONE = tel
		WHERE
			USER_ID = uid;

		IF active THEN
			INSERT INTO notification(USER_ID,TYPE,ICON,MESSAGE) VALUES(uId,'event','fa fa-user text-success',CONCAT(admin_name ,' updated your profile details.'));
		END IF;
	END;
	END IF;
	IF base > 0 THEN
		UPDATE employee SET BRANCH_NO = base WHERE USER_ID = uid AND ORG_ID = org;
	END IF;

	COMMIT;
	SET code = 0;
	SELECT JSON_OBJECT('business', org_name, 'admin_name', admin_name, 'admin_mail', admin_mail, 'token', utoken) INTO info;
	SELECT * FROM v_staff  WHERE user_id = uId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `crud_freelance` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `crud_freelance`(
	IN name		VARCHAR(20),
	IN modId	CHAR(2) CHARSET UTF8,
	IN processor	CHAR(2) CHARSET UTF8,
	IN paygateId	VARCHAR(35),
	IN access 	VARCHAR(255),
	IN client 	CHAR(12),
	OUT code 	INT,
	OUT info 	TEXT
)
BEGIN
	DECLARE tenantId BIGINT UNSIGNED;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	DECLARE EXIT HANDLER FOR 1062
	BEGIN
		SELECT TENANT_ID FROM tenant WHERE USER_ID = client INTO tenantId;
		IF IFNULL(processor, '') <> '' THEN
			call paygate(tenantId, processor, paygateId, access, client, code, info);
		END IF;
		IF code <> 0 THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = info, MYSQL_ERRNO = code;
		END IF;
		COMMIT;
		SET info = JSON_OBJECT('fresh', false, 'tenant_id', tenantId, 'report', CONCAT(name, ' information updated'));
		SET code = 0;
	END;
	START TRANSACTION;
		UPDATE user SET ALIAS = name WHERE USER_ID = client;
		INSERT INTO tenant(USER_ID, TYPE) VALUES(client, 'U');
		SET tenantId = LAST_INSERT_ID();
		IF modId IS NOT \N THEN
			INSERT INTO subscription VALUES(tenantId, modId, NOW(), 1, 1);
		END IF;
		IF IFNULL(processor, '') <> '' THEN
			call paygate(tenantId, processor, paygateId, access, client, code, info);
		END IF;
		IF code <> 0 THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = info, MYSQL_ERRNO = code;
		END IF;
	COMMIT;
	SET info = JSON_OBJECT('fresh', true, 'tenant_id', tenantId, 'report', CONCAT("You're now a business with name '", name, "'"));
	SET code = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `crud_org` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `crud_org`(
	IN tenantId	BIGINT UNSIGNED,
	IN name		VARCHAR(45),
	IN struct	VARCHAR(21),
	IN regno	VARCHAR(21),
	IN commission	VARCHAR(85),
	IN web		VARCHAR(35),
	IN dba		VARCHAR(65),
	IN intel	VARCHAR(255),
	IN slogan	VARCHAR(255),
	IN img		VARCHAR(5),
	IN modId	CHAR(2) CHARSET UTF8,
	IN client 	CHAR(12),
	OUT code 	INT,
	OUT info 	TEXT
)
BEGIN
	DECLARE tempId VARCHAR(255) DEFAULT tenant(tenantId);
	DECLARE orgId BIGINT UNSIGNED DEFAULT IF(tempId REGEXP '^[0-9]+$', tempId, 0);
	DECLARE hasLogo BOOLEAN DEFAULT FALSE;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	START TRANSACTION;
		CASE orgId
			WHEN 0 THEN
				BEGIN
					INSERT INTO organization(NAME, STRUCT_ID, REGNO, REGISTRA, WEBSITE, MOTTO) VALUES(name, struct, regno, commission, web, slogan);
					SET orgId = LAST_INSERT_ID();
					INSERT INTO admin VALUES(client, 'A', orgId);
					INSERT INTO tenant(ORG_ID,TYPE) VALUES(orgId,'O');
					SET tenantId = LAST_INSERT_ID();

					IF modId IS NOT \N THEN
						INSERT INTO subscription VALUES(tenantId, modId, NOW(), 1, TRUE);
					END IF;
				END;
			ELSE
				BEGIN
					UPDATE organization
					SET
						NAME = name,
						STRUCT_ID = struct,
						REGNO = regno,
						REGISTRA = commission,
						WEBSITE = web,
						DBA = dba,
						DESCRIPTION = intel,
						MOTTO = slogan
					WHERE
						ORG_ID = orgId;
					SELECT COUNT(*) FROM logo WHERE ORG_ID = orgId AND MAIN INTO hasLogo;
				END;
		END CASE;

		IF img IS NOT \N THEN
			IF hasLogo THEN
				UPDATE logo SET EXT = img WHERE ORG_ID = orgId AND MAIN;
			ELSE
				INSERT INTO logo(ORG_ID, EXT, MAIN) VALUES(tenantId, img, TRUE);
			END IF;
		END IF;
	COMMIT;
	SET code = 0;
	SELECT organization FROM v_org_compact WHERE ORG_ID = orgId INTO info;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `emailreset` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `emailreset`(
	IN uId		CHAR(12) CHARSET UTF8,
	IN tkn		TEXT,
	OUT code 	SMALLINT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE umail VARCHAR(254) DEFAULT (SELECT USERAGENT FROM token WHERE USER_ID = uId AND TOKENTYPE_ID = 'EML' AND TOKEN = tkn);
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	START TRANSACTION;

	IF umail IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'token not found', MYSQL_ERRNO = 3000;
	END IF;

	UPDATE user SET EMAIL = umail WHERE USER_ID = uId;
	DELETE FROM token WHERE USER_ID = uId AND TOKENTYPE_ID = 'EML';

	COMMIT;
	SET info = 'Your email was successfully updated';
	SET code = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `emailverification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `emailverification`(
	IN action	VARCHAR(8),
	IN uId		CHAR(12) CHARSET UTF8,
	IN utoken	TEXT,
	OUT code 	SMALLINT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE TOKEN_TYPE CHAR(3) CHARSET UTF8 DEFAULT 'REG';
	DECLARE FOUND BOOLEAN DEFAULT (SELECT COUNT(*) FROM token WHERE TOKENTYPE_ID = TOKEN_TYPE  AND TOKEN = utoken);
	DECLARE fullname VARCHAR(41) DEFAULT(SELECT CONCAT(naam(FIRSTNAME), ' ', naam(LASTNAME)) FROM user WHERE USER_ID = uId);
	DECLARE REF CHAR(12) CHARSET UTF8 DEFAULT(SELECT REFERRER FROM employee WHERE USER_ID = uId);
	DECLARE org VARCHAR(45);

	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;

	IF NOT FOUND THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'token not found', MYSQL_ERRNO = 3000;
	END IF;

	START TRANSACTION;

		CASE action
			WHEN 'activate' THEN
			BEGIN
				SELECT USERAGENT FROM token WHERE USER_ID = uId AND TOKENTYPE_ID = TOKEN_TYPE INTO info;
				UPDATE user SET ACTIVE = TRUE WHERE USER_ID = uId;
				IF REF IS NOT \N THEN
					INSERT INTO notification(USER_ID, TYPE, ICON, MESSAGE) VALUES(REF, 'event', 'fa fa-user text-success', CONCAT(fullname, ' activated their user account.'));
				END IF;
			END;
			WHEN 'decline' THEN
			BEGIN
				SELECT NAME FROM organization INNER JOIN employee USING (ORG_ID) WHERE USER_ID = uId INTO org;
				DELETE FROM employee WHERE USER_ID = uId;
				UPDATE user SET ACTIVE = TRUE WHERE USER_ID = uId;
				INSERT INTO notification(USER_ID, TYPE, ICON, MESSAGE) VALUES(REF, 'event', 'fa fa-ban text-danger', CONCAT(fullname, ' rejected being an employee for ', org, '.'));
			END;
			WHEN 'revoke' THEN
			BEGIN
				SELECT CONCAT(USER_ID, '.', AVATAR) FROM user WHERE USER_ID = uId INTO info;
				DELETE FROM user WHERE USER_ID = uId;
				IF REF IS NOT \N THEN
					INSERT INTO notification(USER_ID, TYPE, ICON, MESSAGE) VALUES(REF, 'event', 'fa fa-ban text-danger', CONCAT(fullname, ' rejected having a Phiscal Account.'));
				END IF;
			END;
		END CASE;
		DELETE FROM token WHERE USER_ID = uId AND TOKENTYPE_ID = TOKEN_TYPE;

	COMMIT;
	SET code = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `forgotpasswd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `forgotpasswd`(
	IN umail	VARCHAR(45),
	IN agent	TEXT,
	OUT code 	SMALLINT,
	OUT info 	VARCHAR(255)

)
BEGIN
	DECLARE TTYPE 		CHAR(3) CHARSET UTF8 DEFAULT 'PSD';
	DECLARE uId 		CHAR(12) CHARSET UTF8 DEFAULT (SELECT USER_ID FROM user WHERE EMAIL = umail);
	DECLARE hasPasswd	BOOLEAN DEFAULT (SELECT PASSWORD IS NOT \N FROM user WHERE USER_ID = uId);
	DECLARE status 		BOOLEAN DEFAULT (SELECT ACTIVE FROM user WHERE EMAIL = umail);
	DECLARE TID 		CHAR(22) CHARSET UTF8 DEFAULT CONCAT(uId,UNIX_TIMESTAMP());
	DECLARE utoken 		TEXT DEFAULT MD5(TID);
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;

	IF uId IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'There is no account associated to this email', MYSQL_ERRNO = 3000;
	END IF;
	IF NOT hasPasswd THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "This User Account isn't Password Protected", MYSQL_ERRNO = 3001;
	END IF;
	IF status < 1 THEN
		SELECT HINT FROM account_state WHERE ID = status INTO info;
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = info, MYSQL_ERRNO = 3002;
	END IF;

	START TRANSACTION;
		DELETE FROM token WHERE USER_ID = uId AND TOKENTYPE_ID = TTYPE;
		INSERT INTO token(TOKEN_ID,USER_ID,TOKEN,TOKENTYPE_ID,USERAGENT) VALUES(TID,uId,utoken,TTYPE,agent);
		SELECT JSON_OBJECT('tid', TID,'token', utoken) INTO info;
	COMMIT;

	SET code = 0;
	SELECT * FROM v_activeUser WHERE ID = uId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `greatreset` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `greatreset`(
	OUT code 	SMALLINT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	START TRANSACTION;
		DELETE FROM user WHERE EMAIL <> 'guest';
		DELETE FROM notification;
		DELETE FROM inquiry;
		DELETE FROM organization;
		ALTER TABLE organization AUTO_INCREMENT = 1;
		ALTER TABLE inquiry AUTO_INCREMENT = 1;
		ALTER TABLE tenant AUTO_INCREMENT = 1;
	COMMIT;
	SET code = 0;
	SET info = 'success';
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `mainswitch` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `mainswitch`(
	IN tenantId	BIGINT UNSIGNED,
	IN target	VARCHAR(15),
	IN id		TINYINT UNSIGNED,
	IN action	BOOLEAN,
	IN client	CHAR(12) CHARSET UTF8,
	OUT code 	SMALLINT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE org BIGINT UNSIGNED DEFAULT tenant(tenantId);
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;

	IF NOT auth(tenantId, client) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "You're not authorized to perform action", MYSQL_ERRNO = 3000;
	END IF;

	START TRANSACTION;
	CASE target
		WHEN 'branch' THEN
			UPDATE branch SET MAIN = FALSE WHERE ORG_ID = org;
			UPDATE branch SET MAIN = action WHERE ORG_ID = org AND BRANCH_NO = id;
			SELECT BRANCH_NO branch, main, photo, addressline1, addressline2, city, country, gps, telephone, description FROM branch WHERE ORG_ID = org;
		BEGIN
		END;
		WHEN 'logo' THEN
			UPDATE logo SET MAIN = FALSE WHERE ORG_ID = org;
			UPDATE logo SET MAIN = action WHERE ORG_ID = org AND LOGO_NO = id;
			SELECT LOGO_NO id, CONCAT(LOGO_NO,'.',EXT) file, size, main, description, width, height, ext FROM logo WHERE ORG_ID = org;
		BEGIN
		END;
	END CASE;
	COMMIT;
	SET info = 'success';
	SET code = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `newtoken` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `newtoken`(
	IN tId		CHAR(22) CHARSET UTF8,
	IN content	TEXT,
	IN class	CHAR(3) CHARSET UTF8,
	IN uId		CHAR(12) CHARSET UTF8,
	IN agent	TEXT,
	OUT code 	SMALLINT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	DELETE FROM token WHERE USER_ID = uId AND TOKENTYPE_ID = class;
	INSERT INTO token(TOKEN_ID,TOKEN,TOKENTYPE_ID,USER_ID,USERAGENT) VALUE(tId,content,class,uId,agent);

	SET info = 'success';
	SET code = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `notify` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `notify`(
	IN client	CHAR(12) CHARSET UTF8,
	IN alert	CHAR(2),
	IN caste	VARCHAR(5),
	IN img		VARCHAR(32),
	IN content	TEXT,
	IN delim	CHAR(1),
	OUT code 	INT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE feeds TINYINT UNSIGNED DEFAULT (SELECT COUNT(*) FROM notification WHERE USER_ID = client AND TYPE = caste);

	DECLARE messages TINYINT UNSIGNED DEFAULT eq.arrLength(delim, content);
	DECLARE i TINYINT UNSIGNED DEFAULT 0;
	DECLARE q TEXT DEFAULT 'INSERT INTO notification(USER_ID, TYPE, ICON, MOD_ID, MESSAGE) VALUES';

	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;

	IF messages < 1 THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = '0 error messages', MYSQL_ERRNO = 3000; END IF;
	START TRANSACTION;
		WHILE(i < messages) DO
			SET q = CONCAT(q, '("', client, '", "', caste, '", "', img, '", ', IFNULL(CONCAT("'", alert, "'"), 'NULL'), ', "', eq.split(content, delim, i), '")', IF(i + 1 < messages, ', ' , ''));
			SET i = i + 1;
		END WHILE;

		PREPARE stmt FROM q;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;

		IF feeds > 9 THEN
			DELETE FROM notification WHERE USER_ID = client AND TYPE = caste ORDER BY TIME LIMIT i;
		END IF;
	COMMIT;
	SET info = CONCAT(messages, ' notification', IF(i = 1, "", "s"), ' added');
	SET code = 0;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `passwdreset` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `passwdreset`(
	IN uId		CHAR(12) CHARSET UTF8,
	IN authentic	BOOLEAN,
	IN tkn		TEXT,
	IN passwd	CHAR(60) CHARSET UTF8,
	IN ip		CHAR(15),
	IN agent	TEXT,
	OUT code 	SMALLINT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE userFound	BOOLEAN DEFAULT (SELECT COUNT(USER_ID) FROM user WHERE USER_ID = uId);
	DECLARE hasToken	BOOLEAN DEFAULT (SELECT COUNT(*) FROM token WHERE userFound && TOKENTYPE_ID = 'PSD');
	DECLARE tokenValid	BOOLEAN DEFAULT (SELECT COUNT(*) FROM token WHERE hasToken && TOKEN = tkn);
	DECLARE hasPasswd	BOOLEAN DEFAULT (SELECT PASSWORD IS NOT \N FROM user WHERE USER_ID = uId);
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;

	IF NOT userFound THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'User Not Found', MYSQL_ERRNO = 3000;
	END IF;
	IF hasToken && NOT tokenValid THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Token', MYSQL_ERRNO = 3000;
	END IF;
	IF NOT hasToken && hasPasswd && NOT authentic THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Please provide Correct Password', MYSQL_ERRNO = 3000;
	END IF;

	START TRANSACTION;
		UPDATE user SET PASSWORD = passwd WHERE USER_ID = uId;
		INSERT INTO password_change(USER_ID,IPADDRESS,USERAGENT) VALUES(uId,ip,agent);
		DELETE FROM token WHERE USER_ID = uid AND TOKENTYPE_ID = 'PSD';
	COMMIT;

	SET info = 'Password successfully Changed';
	SET code = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `paygate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `paygate`(
	IN tenantId	BIGINT UNSIGNED,
	IN processor	CHAR(2) CHARSET UTF8,
	IN clientId	VARCHAR(35),
	IN access 	VARCHAR(255),
	IN client	CHAR(12) CHARSET UTF8,
	OUT code 	INT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	DECLARE EXIT HANDLER FOR 1062
	BEGIN
		UPDATE paymentgateway SET PROVIDER_ID = processor, CLIENT_ID = clientId, ACCESS_KEY = access WHERE TENANT_ID = tenantId;
		SET code = 0;
	END;

	IF NOT auth(tenantId, client) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "You're not authorized to view tenant", MYSQL_ERRNO = 3000;
	END IF;

	IF processor <> '?' THEN
		INSERT INTO paymentgateway VALUES(processor, tenantId, clientId, access);
	ELSE
		DELETE FROM paymentgateway WHERE TENANT_ID = tenantId;
	END IF;
	SET code = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `rm_org` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `rm_org`(
	IN tenantId	BIGINT UNSIGNED,
	IN confirmation	VARCHAR(45),
	IN client	CHAR(12) CHARSET UTF8,
	OUT code 	SMALLINT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE orgId BIGINT UNSIGNED DEFAULT tenant(tenantId);
	DECLARE orgName VARCHAR(45) DEFAULT (SELECT NAME FROM organization WHERE ORG_ID = orgId);
	DECLARE authorized BOOLEAN DEFAULT auth(tenantId, client);

	DECLARE empId CHAR(12) CHARSET UTF8;
	DECLARE done BOOLEAN DEFAULT FALSE;
	DECLARE cursor_staff CURSOR FOR SELECT e.USER_ID FROM employee e INNER JOIN user u USING(USER_ID) WHERE ORG_ID = orgId AND ACTIVE;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;

	IF NOT authorized THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "You're not authorized to perform action", MYSQL_ERRNO = 3000;
	END IF;

	IF LOWER(confirmation) <> LOWER(orgName) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "Confirmation Failed! Check Your Spelling.", MYSQL_ERRNO = 3001;
	END IF;

	START TRANSACTION;



		OPEN cursor_staff;
		read_loop: LOOP
			FETCH cursor_staff INTO empId;
			IF done THEN
				LEAVE read_loop;
			END IF;
			INSERT INTO notification(USER_ID, TYPE, ICON, MESSAGE) VALUES(empId, 'event', 'fa fa-building text-danger', CONCAT(trademark(tenantId) , ' has been deleted from Phiscal by its Administration.'));
		END LOOP;
		CLOSE cursor_staff;



		DELETE FROM organization WHERE ORG_ID = orgId;

	COMMIT;
	SET info = orgId;
	SET code = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `signup` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `signup`(
	IN fname	VARCHAR(20),
	IN lname	VARCHAR(20),
	IN idno		CHAR(13),
	IN nation	CHAR(2),
	IN umail	VARCHAR(45),
	IN passwd	VARCHAR(60),
	IN agent	TEXT,
	IN skipmail	BOOLEAN,
	OUT code 	INT,
	OUT info 	VARCHAR(255)
)
BEGIN
	DECLARE TID CHAR(22) CHARSET UTF8;
	DECLARE utoken TEXT DEFAULT MD5(CONCAT(umail,passwd));
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;

	START TRANSACTION;
		INSERT INTO user(FIRSTNAME,LASTNAME,NATIONALITY,IDNUMBER,EMAIL,PASSWORD,ACTIVE) VALUES(fname,lname,nation,idno,umail,passwd,skipmail);
		IF NOT skipmail THEN
			SELECT CONCAT(@USER_ID,UNIX_TIMESTAMP()) INTO TID;
			INSERT INTO token(TOKEN_ID,USER_ID,TOKEN,TOKENTYPE_ID,USERAGENT) VALUES(TID,@USER_ID,utoken,'REG',agent);
		END IF;
	COMMIT;

	SET code = 0;
	SET info = utoken;
	SELECT * FROM v_session WHERE USER_ID = @USER_ID;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `subscribe` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `subscribe`(
	IN tenantId 	BIGINT UNSIGNED,
	IN modId	CHAR(2) CHARSET UTF8,
	IN client	CHAR(12) CHARSET UTF8,
	OUT code 	INT,
	OUT info 	TEXT
)
BEGIN
	DECLARE latest DATETIME DEFAULT (SELECT MAX(DATE) FROM subscription WHERE TENANT_ID = tenantId AND MOD_ID = modId);
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
	END;
	IF NOT auth(tenantId,client) THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "you're not authorized for operation", MYSQL_ERRNO = 30000;
	END IF;

	IF latest IS NULL || DATEDIFF(NOW(),latest) > 30 THEN
		INSERT INTO subscription VALUES(tenantId, modId, NOW() ,1 , TRUE);
	ELSE
		UPDATE subscription SET ACTIVE = NOT ACTIVE WHERE DATE = latest;
	END IF;

	SET info = latest;
	SET code = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `phiscal`
--

USE `phiscal`;

--
-- Final view structure for view `v_activeUser`
--

/*!50001 DROP TABLE IF EXISTS `v_activeUser`*/;
/*!50001 DROP VIEW IF EXISTS `v_activeUser`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_activeUser` AS select `user`.`USER_ID` AS `id`,`user`.`USERNAME` AS `username`,concat(`naam`(`user`.`FIRSTNAME`),' ',`naam`(`user`.`LASTNAME`)) AS `fullname`,`naam`(`user`.`FIRSTNAME`) AS `firstname`,`naam`(`user`.`LASTNAME`) AS `lastname`,`user`.`IDNUMBER` AS `idnumber`,`user`.`EMAIL` AS `email`,`user`.`PHONE` AS `phone`,`user`.`AVATAR` AS `avatar` from `user` where `user`.`ACTIVE` <> 0 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_dopamine`
--

/*!50001 DROP TABLE IF EXISTS `v_dopamine`*/;
/*!50001 DROP VIEW IF EXISTS `v_dopamine`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_dopamine` AS select json_object('id',`n`.`NOT_COUNT`,'type',`n`.`TYPE`,'icon',`n`.`ICON`,'message',`n`.`MESSAGE`,'time',`n`.`TIME`,'read',`n`.`HAVE_READ`,'trademark',concat(`m`.`MOD_ID`,'.png'),'module',`m`.`NAME`) AS `feed`,`n`.`TIME` AS `TIME`,`n`.`USER_ID` AS `USER_ID` from (`notification` `n` left join `module` `m` on(`n`.`MOD_ID` = `m`.`MOD_ID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_module`
--

/*!50001 DROP TABLE IF EXISTS `v_module`*/;
/*!50001 DROP VIEW IF EXISTS `v_module`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_module` AS select `m`.`MOD_ID` AS `mod_id`,`m`.`NAME` AS `name`,`m`.`DESCRIPTION` AS `description`,`m`.`WEBCLIENT` AS `webclient`,`m`.`LOGO` AS `logo`,to_days(current_timestamp()) - to_days(`s`.`DATE`) AS `days`,`s`.`ACTIVE` AS `active`,`s`.`TENANT_ID` AS `tenant_id` from (`module` `m` join `subscription` `s` on(`m`.`MOD_ID` = `s`.`MOD_ID`)) order by `s`.`DATE` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_org_compact`
--

/*!50001 DROP TABLE IF EXISTS `v_org_compact`*/;
/*!50001 DROP VIEW IF EXISTS `v_org_compact`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_org_compact` AS select json_object('admin',`a`.`USER_ID`,'tenant',`t`.`TENANT_ID`,'id',`o`.`ORG_ID`,'name',`trademark`(`t`.`TENANT_ID`),'regno',`o`.`REGNO`,'website',`o`.`WEBSITE`,'description',`o`.`DESCRIPTION`,'motto',`o`.`MOTTO`,'registra',`o`.`REGISTRA`,'logo',concat(`l`.`LOGO_NO`,'.',`l`.`EXT`)) AS `organization`,`o`.`ORG_ID` AS `ORG_ID` from ((((`admin` `a` join `tenant` `t` on(`a`.`ORG_ID` = `t`.`ORG_ID`)) join `organization` `o` on(`a`.`ORG_ID` = `o`.`ORG_ID`)) left join `structure` `s` on(`o`.`STRUCT_ID` = `s`.`STRUCT_ID`)) left join `logo` `l` on(`l`.`ORG_ID` = `o`.`ORG_ID` and `l`.`MAIN` <> 0)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_org_depth`
--

/*!50001 DROP TABLE IF EXISTS `v_org_depth`*/;
/*!50001 DROP VIEW IF EXISTS `v_org_depth`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_org_depth` AS select `a`.`USER_ID` AS `admin`,`t`.`TENANT_ID` AS `tenant_id`,`o`.`ORG_ID` AS `org_id`,`o`.`NAME` AS `name`,`o`.`STRUCT_ID` AS `struct_id`,`o`.`DBA` AS `dba`,`o`.`DESCRIPTION` AS `description`,`o`.`MOTTO` AS `motto`,`o`.`REGNO` AS `regno`,`s`.`NAME` AS `structure`,`o`.`REGISTRA` AS `registra`,`o`.`WEBSITE` AS `website`,`l`.`LOGO_NO` AS `logo`,`l`.`EXT` AS `ext`,`l`.`WIDTH` AS `width`,`l`.`HEIGHT` AS `height`,`l`.`MAIN` AS `mainLogo`,`l`.`SIZE` AS `size`,`l`.`DESCRIPTION` AS `logoDesc`,`b`.`BRANCH_NO` AS `branch`,`b`.`PHOTO` AS `photo`,`b`.`MAIN` AS `mainBranch`,`b`.`ADDRESSLINE1` AS `addressline1`,`b`.`ADDRESSLINE2` AS `addressline2`,`b`.`CITY` AS `city`,`b`.`REGION` AS `region`,`b`.`COUNTRY` AS `country`,`b`.`GPS` AS `gps`,`b`.`TELEPHONE` AS `telephone`,`b`.`DESCRIPTION` AS `branchDesc`,(select count(0) from `employee` where `employee`.`ORG_ID` = `o`.`ORG_ID` and `employee`.`BRANCH_NO` = `b`.`BRANCH_NO`) AS `staff`,`e`.`user_id` AS `user_id`,`e`.`fullname` AS `fullname`,`e`.`firstname` AS `firstname`,`e`.`lastname` AS `lastname`,`e`.`age` AS `age`,`e`.`dob` AS `dob`,`e`.`idnumber` AS `idnumber`,`e`.`email` AS `email`,`e`.`phone` AS `phone`,`e`.`nationality` AS `nationality`,`e`.`avatar` AS `avatar`,`e`.`active` AS `active`,`e`.`organization` AS `organization`,`e`.`workEmail` AS `workEmail`,`e`.`workTelephone` AS `workTelephone`,`e`.`empBranch` AS `empBranch`,`e`.`branchAddress` AS `branchAddress`,`e`.`branchCity` AS `branchCity`,`e`.`branchRegion` AS `branchRegion`,`e`.`branchCountry` AS `branchCountry`,`e`.`state` AS `state`,`e`.`report` AS `report`,`p`.`NAME` AS `gatewayName`,`g`.`PROVIDER_ID` AS `gatewayProvider`,`g`.`CLIENT_ID` AS `gatewayClient`,`g`.`ACCESS_KEY` AS `gatewayKey` from ((((((((`admin` `a` join `tenant` `t` on(`a`.`ORG_ID` = `t`.`ORG_ID`)) left join `paymentgateway` `g` on(`t`.`TENANT_ID` = `g`.`TENANT_ID`)) left join `paygateprovider` `p` on(`g`.`PROVIDER_ID` = `p`.`PROVIDER_ID`)) join `organization` `o` on(`a`.`ORG_ID` = `o`.`ORG_ID`)) join `structure` `s` on(`o`.`STRUCT_ID` = `s`.`STRUCT_ID`)) left join `logo` `l` on(`a`.`ORG_ID` = `l`.`ORG_ID`)) left join `branch` `b` on(`a`.`ORG_ID` = `b`.`ORG_ID`)) left join `v_staff` `e` on(`b`.`ORG_ID` = `e`.`organization`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_profilePermission`
--

/*!50001 DROP TABLE IF EXISTS `v_profilePermission`*/;
/*!50001 DROP VIEW IF EXISTS `v_profilePermission`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_profilePermission` AS select `p`.`PROFILE_ID` AS `id`,`p`.`NAME` AS `profile`,`r`.`NAME` AS `resource`,`a`.`ROUTE` AS `action`,`a`.`DESCRIPTION` AS `description` from (((`profile` `p` join `permission` `m` on(`p`.`PROFILE_ID` = `m`.`PROFILE_ID`)) join `action` `a` on(`m`.`ACTION_ID` = `a`.`ACTION_ID`)) join `resource` `r` on(`a`.`RESOURCE_ID` = `r`.`RESOURCE_ID`)) order by `p`.`NAME`,`r`.`NAME`,`a`.`ROUTE` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_resourceAction`
--

/*!50001 DROP TABLE IF EXISTS `v_resourceAction`*/;
/*!50001 DROP VIEW IF EXISTS `v_resourceAction`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_resourceAction` AS select `r`.`NAME` AS `resource`,`a`.`METHOD` AS `method`,`a`.`ROUTE` AS `route`,`a`.`NAME` AS `action`,`a`.`PREFLIGHT` AS `preflight`,`a`.`DESCRIPTION` AS `description`,`a`.`PRIVATE` AS `private` from (`action` `a` join `resource` `r` on(`a`.`RESOURCE_ID` = `r`.`RESOURCE_ID`)) order by `r`.`NAME`,`a`.`ROUTE` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_session`
--

/*!50001 DROP TABLE IF EXISTS `v_session`*/;
/*!50001 DROP VIEW IF EXISTS `v_session`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_session` AS select json_object('id',`u`.`USER_ID`,'username',`u`.`USERNAME`,'fullname',concat(`naam`(`u`.`FIRSTNAME`),' ',`naam`(`u`.`LASTNAME`)),'firstname',`naam`(`u`.`FIRSTNAME`),'lastname',`naam`(`u`.`LASTNAME`),'idnumber',`u`.`IDNUMBER`,'email',`u`.`EMAIL`,'phone',`u`.`PHONE`,'avatar',`u`.`AVATAR`,'active',`u`.`ACTIVE`,'password',`u`.`PASSWORD` is null,'tenants',`tenants`(`u`.`USER_ID`),'employee',`u`.`USER_ID` in (select `employee`.`USER_ID` from `employee`)) AS `user`,`u`.`USER_ID` AS `USER_ID` from `user` `u` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_staff`
--

/*!50001 DROP TABLE IF EXISTS `v_staff`*/;
/*!50001 DROP VIEW IF EXISTS `v_staff`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_staff` AS select `u`.`USER_ID` AS `user_id`,concat(`naam`(`u`.`FIRSTNAME`),' ',`naam`(`u`.`LASTNAME`),' ',if(`u`.`DOB` is null,'',concat('(',timestampdiff(YEAR,`u`.`DOB`,curdate()),')'))) AS `fullname`,`naam`(`u`.`FIRSTNAME`) AS `firstname`,`naam`(`u`.`LASTNAME`) AS `lastname`,timestampdiff(YEAR,`u`.`DOB`,curdate()) AS `age`,`u`.`DOB` AS `dob`,`u`.`IDNUMBER` AS `idnumber`,`u`.`EMAIL` AS `email`,`u`.`PHONE` AS `phone`,`u`.`NATIONALITY` AS `nationality`,`u`.`AVATAR` AS `avatar`,`u`.`ACTIVE` AS `active`,`e`.`ORG_ID` AS `organization`,`e`.`EMAIL` AS `workEmail`,`e`.`TELEPHONE` AS `workTelephone`,`e`.`BRANCH_NO` AS `empBranch`,`b`.`ADDRESSLINE1` AS `branchAddress`,`b`.`CITY` AS `branchCity`,`b`.`REGION` AS `branchRegion`,`b`.`COUNTRY` AS `branchCountry`,`s`.`STATE` AS `state`,`s`.`REPORT` AS `report` from (((`account_state` `s` join `user` `u` on(`u`.`ACTIVE` = `s`.`ACCST_ID`)) join `employee` `e` on(`u`.`USER_ID` = `e`.`USER_ID`)) left join `branch` `b` on(`e`.`ORG_ID` = `b`.`ORG_ID` and `e`.`BRANCH_NO` = `b`.`BRANCH_NO`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_tenant`
--

/*!50001 DROP TABLE IF EXISTS `v_tenant`*/;
/*!50001 DROP VIEW IF EXISTS `v_tenant`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_tenant` AS select `t`.`TENANT_ID` AS `tenant_id`,`t`.`TYPE` AS `type`,json_object('tenant_id',`t`.`TENANT_ID`,'user_id',`t`.`USER_ID`,'type',`t`.`TYPE`,'org_id',`t`.`ORG_ID`,'website',(select `organization`.`WEBSITE` from `organization` where `organization`.`ORG_ID` = `t`.`ORG_ID`),'trademark',`trademark`(`t`.`TENANT_ID`),'face',`face`(`t`.`TENANT_ID`)) AS `business`,`tenantmodules`(`t`.`TENANT_ID`) AS `modules` from `tenant` `t` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_user_tenant`
--

/*!50001 DROP TABLE IF EXISTS `v_user_tenant`*/;
/*!50001 DROP VIEW IF EXISTS `v_user_tenant`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_user_tenant` AS (select `tenant`.`TENANT_ID` AS `TENANT_ID`,`tenant`.`USER_ID` AS `USER_ID`,'A' AS `RANK` from `tenant`) union (select `tenant`.`TENANT_ID` AS `TENANT_ID`,`a`.`USER_ID` AS `USER_ID`,'A' AS `RANK` from (`admin` `a` join `tenant` on(`a`.`ORG_ID` = `tenant`.`ORG_ID`))) union (select `tenant`.`TENANT_ID` AS `TENANT_ID`,`e`.`USER_ID` AS `USER_ID`,'E' AS `RANK` from (`employee` `e` join `tenant` on(`e`.`ORG_ID` = `tenant`.`ORG_ID`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-11 14:30:57
