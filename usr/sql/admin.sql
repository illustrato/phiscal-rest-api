-- --defaults-group-suffix=Root
-- -t
--
DROP USER IF EXISTS strato;
CREATE USER strato IDENTIFIED BY 'SAf3&sound';
GRANT  FILE ON *.* TO strato;
GRANT ALL PRIVILEGES ON phiscal.* TO strato;
GRANT EXECUTE, SELECT ON mysql.* TO strato;

DROP DATABASE IF EXISTS earth;
CREATE DATABASE earth;
GRANT EXECUTE, SELECT ON earth.* TO strato;

FLUSH PRIVILEGES;

USE earth;
CREATE TABLE country(alpha2Code CHAR(2) CHARSET UTF8 PRIMARY KEY, name VARCHAR(52), region VARCHAR(8), capital VARCHAR(19), callingCodes VARCHAR(6)) ENGINE=CONNECT TABLE_TYPE=JSON FILE_NAME='/var/lib/mysql/json/country.json';
