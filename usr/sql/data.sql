-- MariaDB dump 10.19  Distrib 10.5.13-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: phiscal
-- ------------------------------------------------------
-- Server version	10.5.13-MariaDB-0ubuntu0.21.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `account_state`
--

LOCK TABLES `account_state` WRITE;
/*!40000 ALTER TABLE `account_state` DISABLE KEYS */;
INSERT INTO `account_state` VALUES (-3,'Banned','Account Banned','Phiscal Banned this Account'),(-2,'Suspended','Account Suspended','Phiscal Suspended this Account'),(-1,'Deactivated','You Deactivated Account','Account has been Deactivated by User'),(0,'Inactive','Please Verify Email','Account has not yet been activated via Verification Email'),(1,'Active',NULL,NULL);
/*!40000 ALTER TABLE `account_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `action`
--

LOCK TABLES `action` WRITE;
/*!40000 ALTER TABLE `action` DISABLE KEYS */;
INSERT INTO `action` VALUES ('AMDT','getData','Get Module Data','AM',0,'get','data/{id}',0),('AMEL','elaborate','Module Description','AM',0,'get','elaborate/{name}',0),('AMIX','index','Index Action','AM',0,'get','index',0),('AMLS','getList','Get Subscription List of Entity','AM',1,'get','ls/{target}',1),('AMSB','subscribe','Subscribe Module to Entity','AM',1,'post','subscribe',1),('ASDT','rmJWT','Delete User Tokens','AS',1,'delete','deletetoken',1),('ASEU','emailUpdate','Update Email Address','AS',1,'get','updateemail/{uid}/{token}',1),('ASEV','emailVerification','User Account Email Verification','AS',0,'get','email/{src}/{uId}/{token}',0),('ASFG','forgotPasswd','Request Password Reset','AS',0,'post','forgotpasswd',1),('ASLI','login','Login User Account','AS',0,'post','auth',1),('ASPR','emailSignin','Reset Password Emaill Login','AS',0,'get','signin/{uid}/{token}',0),('ASRN','viewedFeeds','Update notifications as All Read','AS',1,'get','readfeeds',1),('ASSU','signup','User Account sign up','AS',0,'post','register',1),('ASVA','verifyAuth','Verify Bearer Token','AS',0,'get','verify',1),('BRCU','crud','Create or Update Organization Branch','BR',1,'put','crud/{id:[0-9]+}',1),('BRPH','updatePhoto','Upload or Delete Branch Photo','BR',1,'put','photo/{id:[0-9]+}',1),('BRRM','delete','Delete Organization Branch','BR',1,'delete','{id:[0-9]+}',1),('EMCP','crudPhoto','Update Employee Photo','EM',1,'put','photo/{uId}',1),('EMCU','crud','Create new Employee','EM',1,'put','crud/{id}',1),('EMRM','rm','Delete Employee','EM',1,'delete','{id}',1),('IXCU','makeInquiry','Contact Us','IX',0,'post','inquiry',1),('IXIL','getInquiries','Inquiries List','IX',0,'get','inquiries',0),('IXLC','getCountries','List all Countries','IX',0,'get','nations',0),('IXTC','getTnC','Terms of Service','IX',0,'get','tnc',1),('LGCD','crud','Upload or Delete Organization Logo','LG',1,'put','crud/{id:[0-9]+}',1),('OGCD','crud','Create or Update organization Record','OG',1,'post','crud',1),('OGLS','getList','Get list of Business Structures','OG',0,'get','ls/{target}',0),('OGMS','mainSwitch','Switch Headquarters','OG',1,'put','switch/{target}/{id:[0-9]+}',1),('OGRD','getAdminInfo','Organizational Private Data','OG',1,'get','privatedata',1),('OGRM','delete','Remove Organization from Phiscal','OG',1,'delete','this/{confirmation}',1),('OGUD','getPublicInfo','Organizational Public Information','OG',0,'get','publicdata',1),('PAGR','greatreset','Great Reset','PA',1,'delete','reset',1),('PYAG','addGateway','Add Payment Gateway','PY',1,'post','addgateway',1),('PYGD','getDetails','Get Payment Details','PY',1,'get','details/{target}',1),('PYLS','getList','Generic List','PY',0,'get','ls/{proxy}',0),('UA0P','getProfiles','List Profiles','UA',1,'get','profiles',0),('UA0S','search','Find Users','UA',1,'get','search/{params}',0),('UACH','check','Verify User Record','UA',0,'get','check/{target}/{value}',0),('UADT','getData','Get User Data','UA',1,'get','data/{target}',1),('UAFL','freelanceAgent','Freelance Agent CRUD Operations','UA',1,'post','freelance',1),('UAIX','index','Index Action','UA',1,'get','index',0),('UAPR','resetPasswd','Password Reset','UA',1,'put','passwdreset',1),('UARM','rm','Delete User Record','UA',0,'delete','{email}',1),('UATL','getTitles','Get User Titles','UA',0,'get','titles',1),('UATN','tenantList','List Admin Tenants','UA',1,'get','tenants',1),('UAUA','crudAvatar','Update User Avatar','UA',1,'post','avatar',1),('UAUP','update','Update User Data','UA',1,'post','update',1);
/*!40000 ALTER TABLE `action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `branch`
--

LOCK TABLES `branch` WRITE;
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `inquiry`
--

LOCK TABLES `inquiry` WRITE;
/*!40000 ALTER TABLE `inquiry` DISABLE KEYS */;
/*!40000 ALTER TABLE `inquiry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `loginattempt`
--

LOCK TABLES `loginattempt` WRITE;
/*!40000 ALTER TABLE `loginattempt` DISABLE KEYS */;
INSERT INTO `loginattempt` VALUES ('0','192.168.43.66','Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0','2022-02-05 00:46:07',0);
/*!40000 ALTER TABLE `loginattempt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `logo`
--

LOCK TABLES `logo` WRITE;
/*!40000 ALTER TABLE `logo` DISABLE KEYS */;
/*!40000 ALTER TABLE `logo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` VALUES ('Box 604','Equipment Managent System','B1','png',0,0,NULL),('Homing','Poperty Listing','H1','jpg',0,0,NULL),('Nishe','E-Commerce','N1','jpg',0,1,'https://www.nishe.store'),('Net Pain','Payroll System','P1','jpg',0,0,NULL),('Spa Zilla','Mobile Car Wash','S1','jpg',0,0,NULL);
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `organization`
--

LOCK TABLES `organization` WRITE;
/*!40000 ALTER TABLE `organization` DISABLE KEYS */;
/*!40000 ALTER TABLE `organization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `password_change`
--

LOCK TABLES `password_change` WRITE;
/*!40000 ALTER TABLE `password_change` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_change` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `paygateprovider`
--

LOCK TABLES `paygateprovider` WRITE;
/*!40000 ALTER TABLE `paygateprovider` DISABLE KEYS */;
INSERT INTO `paygateprovider` VALUES ('PF','PayFast'),('PP','PayPal'),('SP','Stripe');
/*!40000 ALTER TABLE `paygateprovider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `paymentgateway`
--

LOCK TABLES `paymentgateway` WRITE;
/*!40000 ALTER TABLE `paymentgateway` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymentgateway` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES ('A','AMSB'),('A','BRCU'),('A','BRPH'),('A','BRRM'),('A','EMCP'),('A','EMCU'),('A','EMRM'),('A','LGCD'),('A','OGCD'),('A','OGMS'),('A','OGRD'),('A','OGRM'),('A','UA0P'),('A','UAIX'),('I','AMSB'),('T','PAGR'),('T','UA0P');
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES ('A','Administrator','System Admin',3),('E','Employee','Organization Employee',2),('I','Individual','Freelance Agent',1),('T','Tech','Phiscal Programmer',4),('Z','Default','General User',0);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `resource`
--

LOCK TABLES `resource` WRITE;
/*!40000 ALTER TABLE `resource` DISABLE KEYS */;
INSERT INTO `resource` VALUES ('AM','module','Application Module','Phiscal\\Controller\\Tenant\\Module'),('AS','session','Account Session','Phiscal\\Controller\\User\\Session'),('BR','branch','Organization Branch','Phiscal\\Controller\\Org\\Branch'),('EM','staff','Staff Resource','Phiscal\\Controller\\Org\\Employee'),('IX','index','Index Resource','Phiscal\\Controller\\Index'),('LG','logo','Organization Logo','Phiscal\\Controller\\Org\\Logo'),('OG','org','Organization Resource','Phiscal\\Controller\\Org\\Organization'),('PA','privacc','Privileged Access','Phiscal\\Controller\\Index'),('PY','payment','Payments Resource','Phiscal\\Controller\\Tenant\\Payment'),('UA','user','User Account','Phiscal\\Controller\\User\\User');
/*!40000 ALTER TABLE `resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `structure`
--

LOCK TABLES `structure` WRITE;
/*!40000 ALTER TABLE `structure` DISABLE KEYS */;
INSERT INTO `structure` VALUES ('CCP','Close Corporation','Private Company where all shareholders are also directors.','CC'),('COO','Cooperative','Business owned by and operated for the benefit of those using its services.','Coop'),('INC','Corporaton','Business acts in its own legal capacity, being a separate entity with shareholders.','Co'),('LTD','Public Company','Limited Liability Company traded publicly.','Ltd'),('LTP','Limited Partnership','Partnership where there is only one partner with unlimited liability - the General Partner.','LP'),('NGO','Non-Profit Organization','Tax-Free Humanitarian organization.',NULL),('PTN','Partnership','Business owned by two or more people who share responsibilities and profits. Legally, the Owners and the Business are the same entity.',NULL),('PTY','Private Company','Limited Liability Company before IPO.','(Pty) Ltd'),('SOL','Sole Trader','Business structure designed for one person. Legally, the Owner and the Business are the same entity.',NULL);
/*!40000 ALTER TABLE `structure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `subscription`
--

LOCK TABLES `subscription` WRITE;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tech`
--

LOCK TABLES `tech` WRITE;
/*!40000 ALTER TABLE `tech` DISABLE KEYS */;
/*!40000 ALTER TABLE `tech` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tenant`
--

LOCK TABLES `tenant` WRITE;
/*!40000 ALTER TABLE `tenant` DISABLE KEYS */;
/*!40000 ALTER TABLE `tenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tokendesc`
--

LOCK TABLES `tokendesc` WRITE;
/*!40000 ALTER TABLE `tokendesc` DISABLE KEYS */;
INSERT INTO `tokendesc` VALUES ('EML','Email Reset','Used in verifying New Email Address'),('JWT','JSON Web Token','Used in Authentication'),('PSD','Password Reset Token','Used when user has Forgotten Password'),('REG','Account Activation','Used in verifying New Account'),('RMT','Remember Me Token','Used to Login without Credentials');
/*!40000 ALTER TABLE `tokendesc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('0',NULL,NULL,NULL,NULL,'guest',NULL,NULL,NULL,NULL,'secret',NULL,1,NULL,NULL,NULL,NULL,NULL,'2021-11-25 21:32:32',NULL,'2021-11-26 07:32:00',NULL,'U');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-18 11:50:42
