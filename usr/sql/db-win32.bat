@echo off
set /p hasPass=" Do you have MariaDB root Password?[y/N] "
::set pass=
IF /I "%hasPass%"=="Y" (

	set /p pass=" Enter MariaDB root Password: "
	ECHO ON
	mysql -uroot -p%pass% < usr\sql\admin.sql
	mysql -uroot -p%pass% < usr\sql\schema.sql
	mysql -uroot -p%pass% phiscal < usr\sql\data.sql
	mysql -uroot -p%pass% phiscal < usr\sql\triggers.sql

) ELSE (
	ECHO ON
	mysql -uroot < usr\sql\admin.sql
	mysql -uroot < usr\sql\schema.sql
	mysql -uroot phiscal < usr\sql\data.sql
	mysql -uroot phiscal < usr\sql\triggers.sql
)
