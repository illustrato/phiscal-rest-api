Businesses use this module to maintain happy creditors, by keeping to date with their **payments**.
Funny enough, creditors of any business technically include its own employees, as their remuneration is due each month.
This application also handles **salaries & wages**, on top of a host of other payments, such as your business **invoices**, **suppliers**, **operating expenses**, etc.

**Net Pain** is a great tool to manage business expenditure, and drafting an informed budget. It includes a database of previous payments and budgets for the future, making it a recipe for comprehensive extrapolation tools.
