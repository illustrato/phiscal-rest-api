<?php

return [
    \Phiscal\Provider\Acl::class,
    \Phiscal\Provider\Config::class,
    \Phiscal\Provider\Crypt::class,
    \Phiscal\Provider\Database::class,
    \Phiscal\Provider\Response::class,
    \Phiscal\Provider\Router::class,
    \Phiscal\Provider\Session::class,
    \Phiscal\Provider\Url::class,

    \Phiscal\Provider\Providers::class,
];
?>
