<?php
use Phalcon\Loader;

use function Phiscal\Core\appPath;

require_once __DIR__ . '/Helper.php';

// Register Auto Loader
$loader = new Loader();
$loader->registerNamespaces([
    'Phiscal\Model' => appPath('/src/model'),
    'Phiscal\Model\Entity' => appPath('/src/model/entity'),

    'Phiscal\Model\Entity\Org' => appPath('/src/model/entity/org'),
    'Phiscal\Model\Entity\Tenant' => appPath('/src/model/entity/tenant'),
    'Phiscal\Model\Entity\User' => appPath('/src/model/entity/user'),

    'Phiscal\Model\Validator' => appPath('/src/model/validator'),
    'Phiscal\Model\Validator\IdNo' => appPath('/src/model/validator/idno'),
    'Phiscal\Model\Validation' => appPath('/src/model/validation'),

    'Phiscal\Model\Validation\Etc' => appPath('/src/model/validation/etc'),
    'Phiscal\Model\Validation\Org' => appPath('/src/model/validation/org'),
    'Phiscal\Model\Validation\Tenant' => appPath('/src/model/validation/tenant'),
    'Phiscal\Model\Validation\User' => appPath('/src/model/validation/user'),

    'Phiscal\Controller' => appPath('/src/controller'),

    'Phiscal\Controller\Org' => appPath('/src/controller/org'),
    'Phiscal\Controller\Tenant' => appPath('/src/controller/tenant'),
    'Phiscal\Controller\User' => appPath('/src/controller/user'),

    'Phiscal\Provider' => appPath('/src/provider'),
    'Phiscal\Middleware' => appPath('/src/middleware'),
    'Phiscal\Traiting' => appPath('/src/trait'),

    'Phiscal\Plugin' => appPath('/src/plugin'),
    'Phiscal\Plugin\Identity' => appPath('/src/plugin/identity'),
    'Phiscal\Plugin\Http' => appPath('/src/plugin/http'),
]);
$loader->registerClasses(['Phiscal\Application' => appPath('/src/Application.php')]);
$loader->registerFiles([appPath('/core/Helper.php')]);
$loader->register();

/**
 * Composer Autoloader
 */
require_once appPath('/vendor/autoload.php');
?>
